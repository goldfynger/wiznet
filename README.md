TODO:

* ~~Review DNS and write DNS parser~~
* ~~In SNTP check for next datagram~~
* ~~Take and Release socket functions: need to review, add critical sections~~
* ~~Comment on WaitPool func~~
* Thread-safe printf
* ~~Close socket in end of SNTP (DNS too)~~
* ~~Change W5100_DEBUG and W5100_DEBUG_DATA to WIZ_DEBUG and WIZ_DEBUG_DATA~~
* ~~Add DNS cache realization~~
* ~~Add DNS support to SNTP realization~~
* ~~Add mutex access when reading two-bytes parameters.~~
* ~~Check answer count and flags in DNS response~~
* Add WIZ_NET_Init in which need reset all network functions (clear IR's, flush memory, close sockets) (all except memory sizes)
* Removing tasks and malloc results.
* ~~Review code for common error list~~
* ? Replace disconnected flag from HTTP socket to TCP socket ?
* ~~Need add read, write and size change functions for W5500~~
* ~~In W5500, need to check for atomic read~~
* Rewrite short lived port system.
* Second callbacks