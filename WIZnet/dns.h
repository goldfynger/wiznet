#ifndef __DNS_H
#define __DNS_H


#include "net.h"
#include "net_def.h"
#include "net_os.h"


#define DNS_TRY_COUNT       3
#define DNS_POLL_TIMEOUT    3000


typedef struct
{    
    const char              *HostName;
    
    NET_IPAddressTypeDef    IPAddress;
    
    uint32_t                TimeToLive;
}
DNS_RecordTypeDef;

typedef struct
{
    NET_HandleTypeDef   *NetHandle;
    
    NET_OS_MutexHandle  *CacheMutex;
    
    uint16_t            TransactionId;
    
    uint8_t             CacheSize;
    
    DNS_RecordTypeDef   Cache[];
}
DNS_HandleTypeDef;


DNS_HandleTypeDef   * DNS_AllocateHandle(NET_HandleTypeDef *hNet, uint8_t cacheSize);
NET_ErrorTypeDef    DNS_Resolve         (DNS_HandleTypeDef *hDns, const char *hostName, NET_IPAddressTypeDef *hostAddress);
void                DNS_SecondCallback  (DNS_HandleTypeDef *hDns);


#endif /* __DNS_H */
