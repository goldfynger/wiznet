#ifndef __HTTP_SRV_H
#define __HTTP_SRV_H


#include <stdint.h>

#include "net.h"
#include "tcp.h"


typedef struct
{
    const uint8_t   *Content;
    
    const char      *Url;
    
    const char      *ContentType;
    
    uint16_t        ContentLength;
    
    uint8_t         UrlLength;
    
    uint8_t         ContentTypeLength;
}
HTTP_SRV_UrlEntryTypeDef;

typedef struct
{
    TCP_SocketHandleTypeDef     TcpSocket;
    
    HTTP_SRV_UrlEntryTypeDef    *CurrentUrlEntry;
    
    uint16_t                    SentLength;
    
    bool                        SentDisconnect;
}
HTTP_SRV_SocketHandleTypeDef;

typedef struct
{
    NET_HandleTypeDef               *NetHandle;
    
    HTTP_SRV_SocketHandleTypeDef    *HttpSockets;
    
    HTTP_SRV_UrlEntryTypeDef        *UrlEntries;
    
    uint8_t                         SocketsCount;
    
    uint8_t                         UrlEntriesCount;
    
    uint8_t                         UrlEntriesMaxCount;
    
    bool                            ListeningInProcess;
}
HTTP_SRV_HandleTypeDef;


NET_ErrorTypeDef HTTP_SRV_AllocHandle    (HTTP_SRV_HandleTypeDef *hHttp, NET_PortTypeDef localPort);
NET_ErrorTypeDef HTTP_SRV_FreeHandle     (HTTP_SRV_HandleTypeDef *hHttp);
NET_ErrorTypeDef HTTP_SRV_AddUrlEntry    (HTTP_SRV_HandleTypeDef *hHttp, const uint8_t *content, uint16_t contentLength, const char *url, uint8_t urlLength, const char *contentType, uint8_t contentTypeLength);
NET_ErrorTypeDef HTTP_SRV_Run            (HTTP_SRV_HandleTypeDef *hHttp);


#endif /* __HTTP_SRV_H */
