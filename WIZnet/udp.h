#ifndef __UDP_H
#define __UDP_H


#include <stdbool.h>
#include <stdint.h>

#include "FreeRTOS.h"
#include "task.h"

#include "net.h"
#include "net_def.h"
#include "net_os.h"
#include "wizchip.h"


#define UDP_FLUSH_BUFFER_LEN    16

#define UDP_POLL_INTERVAL       20


typedef enum
{
    UDP_STATE_UNKNOWN           = 0,
    UDP_STATE_CLOSED            = 1,
    UDP_STATE_OPENED            = 2,
    UDP_STATE_IN_PROCESS        = 3,
}
UDP_StateTypeDef;

typedef enum
{
    UDP_FLAG_NONE               = 0x00,
    UDP_FLAG_RECEIVED           = 0x04,
    UDP_FLAG_TIMEOUT            = 0x08,
    UDP_FLAG_SENT               = 0x10,
    UDP_FLAG_ALL                = 0xFF,
}
UDP_FlagTypeDef;


typedef struct
{
    WIZ_SocketHandleTypeDef     Socket;
    
    NET_PortTypeDef             LocalPort;
}
UDP_SocketHandleTypeDef;

typedef struct
{
    UDP_SocketHandleTypeDef     *UdpSocket;    
    
    uint16_t                    FullLength;    
    uint16_t                    ReceivedLength;
    
    NET_EndPointTypeDef         RemotePoint;
}
UDP_DatagramTypeDef;


NET_ErrorTypeDef UDP_OpenSocket             (UDP_SocketHandleTypeDef *hUdp, NET_HandleTypeDef *hNet);
NET_ErrorTypeDef UDP_PollForOpenSocket      (UDP_SocketHandleTypeDef *hUdp, uint16_t *pTimeout, NET_HandleTypeDef *hNet);

NET_ErrorTypeDef UDP_SendLength             (UDP_SocketHandleTypeDef *hUdp, uint16_t *pLength);
NET_ErrorTypeDef UDP_SendData               (UDP_SocketHandleTypeDef *hUdp, uint8_t *pBuffer, uint16_t bufferLength, NET_EndPointTypeDef *pRemotePoint);
NET_ErrorTypeDef UDP_PollForSendData        (UDP_SocketHandleTypeDef *hUdp, uint8_t *pBuffer, uint16_t bufferLength, NET_EndPointTypeDef *pRemotePoint, uint16_t *pTimeout);

NET_ErrorTypeDef UDP_ReceiveLength          (UDP_SocketHandleTypeDef *hUdp, uint16_t *pLength);
NET_ErrorTypeDef UDP_ReceiveDatagram        (UDP_SocketHandleTypeDef *hUdp, UDP_DatagramTypeDef *pDatagram);
NET_ErrorTypeDef UDP_PollForReceiveDatagram (UDP_SocketHandleTypeDef *hUdp, UDP_DatagramTypeDef *pDatagram, uint16_t *pTimeout);
NET_ErrorTypeDef UDP_ReceiveData            (UDP_DatagramTypeDef *pDatagram, uint8_t *pBuffer, uint16_t bufferLength, uint16_t *pReceivedLength);
NET_ErrorTypeDef UDP_FlushDatagram          (UDP_DatagramTypeDef *pDatagram);

NET_ErrorTypeDef UDP_CloseSocket            (UDP_SocketHandleTypeDef *hUdp);

NET_ErrorTypeDef UDP_GetState               (UDP_SocketHandleTypeDef *hUdp, UDP_StateTypeDef *pState);
NET_ErrorTypeDef UDP_GetFlags               (UDP_SocketHandleTypeDef *hUdp, UDP_FlagTypeDef *pFlags, UDP_FlagTypeDef flagsMask);
NET_ErrorTypeDef UDP_PollForFlags           (UDP_SocketHandleTypeDef *hUdp, UDP_FlagTypeDef *pFlags, UDP_FlagTypeDef flagsMask, uint16_t *pTimeout);


#endif /* __UDP_H */
