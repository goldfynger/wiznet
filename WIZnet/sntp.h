#ifndef __SNTP_H
#define __SNTP_H


#include "dns.h"
#include "net.h"


#define SNTP_TRY_COUNT      3
#define SNTP_POLL_TIMEOUT   3000


typedef enum
{
    SNTP_OFFSET_MSK = +3,
}
SNTP_TimeOffsetTypeDef;


typedef struct
{
    uint16_t    Year;       // 1900..2036
    uint8_t     Month;      // 1..12
    uint8_t     Day;        // 1..31
    uint8_t     Hours;      // 0..23
    uint8_t     Minutes;    // 0..59   
    uint8_t     Seconds;    // 0..59
}
SNTP_DateTimeTypeDef;

typedef struct
{
    NET_HandleTypeDef   *NetHandle;    
    DNS_HandleTypeDef   *DnsHandle;
}
SNTP_HandleTypeDef;


NET_ErrorTypeDef SNTP_DetermineDateTime(SNTP_HandleTypeDef *hSntp, SNTP_TimeOffsetTypeDef timeOffset, SNTP_DateTimeTypeDef *pDateTime);


#endif /* __SNTP_H */
