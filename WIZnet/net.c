#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "dhcp.h"
#include "dns.h"
#include "main.h"
#include "net_os.h"
#include "sntp.h"
#include "wizchip.h"

#include "net.h"


#define NET_MIN_SHORT_LIVED_PORT 49152
#define NET_MAX_SHORT_LIVED_PORT 65534


//static NET_ParamsTypeDef _params;

static NET_PortTypeDef _shortLivedPort;


// Initial init of network parameters.
NET_ErrorTypeDef NET_Init(NET_HandleTypeDef *hNet)
{
    NET_ErrorTypeDef error = NET_ERR_OK;
    
    
    NET_OS_LOCK(&(hNet->__ShortLivedPortLocker))
    {
        hNet->ShortLivedPort.Value = NET_MIN_SHORT_LIVED_PORT;
    }
    
    
    return error;
}

// Get free local port of dynamic zone.
NET_PortTypeDef NET_GetFreePort(NET_HandleTypeDef *hNet, NET_PortModeTypeDef portMode)
{
    NET_PortTypeDef port = { .Value = 0 };
    
    
    NET_OS_LOCK(&(hNet->__ShortLivedPortLocker))
    {
        do
        {
            port = hNet->ShortLivedPort;
        }
        while (WIZ_IsPortBusy(&(hNet->WizHandle), port, portMode));
        
        
        if (port.Value == NET_MAX_SHORT_LIVED_PORT)
        {
            _shortLivedPort.Value = NET_MIN_SHORT_LIVED_PORT;
        }
        else
        {
            _shortLivedPort.Value++;
        }
    }
    
    
    return port;
}

// Wait for poll interval. Return left time of timeout.
uint16_t NET_WaitPollInterval(NET_PollInfoTypeDef *pPollInfo)
{
    uint16_t leftTime = 0;
    uint32_t delayTime = pPollInfo->Interval;
    
    if (pPollInfo->Timeout != NET_POLL_INFINITE)
    {
        uint32_t beginTime = pPollInfo->BeginTime;
        
        uint32_t nowTime = NET_OS_Ticks();
        if (beginTime > nowTime) // Overflow control.
        {
            uint32_t subtracted = nowTime + 1;
            beginTime -= subtracted;
            nowTime -= subtracted;
        }
        
        uint32_t timeoutTime = beginTime + pPollInfo->Timeout;
        if (beginTime > timeoutTime) // Overflow control.
        {
            uint32_t subtracted = timeoutTime + 1;
            beginTime -= subtracted;
            nowTime -= subtracted;
            timeoutTime -= subtracted;
        }
        
        if (nowTime >= timeoutTime)
        {
            return leftTime;
        }
        else if (delayTime >= (timeoutTime - nowTime))
        {
            delayTime = timeoutTime - nowTime;
        }
        else
        {
            leftTime = timeoutTime - nowTime;
        }
    }
    else
    {
        leftTime = NET_POLL_INFINITE;
    }
    
    
    NET_OS_Delay(delayTime);
    
    
    return leftTime; 
}

void NET_SecondCallback(void)
{
    NET_OS_CRITICAL()
    {    
        //if (_params.DHCPLeftTime > 0)
        {
            //_params.DHCPLeftTime--;
        }
    }
    
    
    //DNS_SecondCallback();
}

NET_IPAddressTypeDef NET_GetIPAddress(NET_HandleTypeDef *hNet)
{
    NET_IPAddressTypeDef ipAddress;
    
    NET_OS_LOCK(&(hNet->Params.__Locker))
    {
        ipAddress = hNet->Params.IPAddress;
    }
    
    return ipAddress;
}

NET_IPAddressTypeDef NET_GetSubnetMask(NET_HandleTypeDef *hNet)
{
    NET_IPAddressTypeDef subnetMask;
    
    NET_OS_LOCK(&(hNet->Params.__Locker))
    {
        subnetMask = hNet->Params.SubnetMask;
    }
    
    return subnetMask;
}

NET_IPAddressTypeDef NET_GetDefaultGateway(NET_HandleTypeDef *hNet)
{
    NET_IPAddressTypeDef defaultGateway;
    
    NET_OS_LOCK(&(hNet->Params.__Locker))
    {
        defaultGateway = hNet->Params.DefaultGateway;
    }
    
    return defaultGateway;
}

NET_IPAddressTypeDef NET_GetDNSServerAddress(NET_HandleTypeDef *hNet)
{
    NET_IPAddressTypeDef serverAddress;
    
    NET_OS_LOCK(&(hNet->Params.__Locker))
    {
        serverAddress = hNet->Params.DNSServerAddress;
    }
    
    return serverAddress;
}

NET_IPAddressTypeDef NET_GetDHCPServerAddress(NET_HandleTypeDef *hNet)
{
    NET_IPAddressTypeDef serverAddress;
    
    NET_OS_LOCK(&(hNet->Params.__Locker))
    {
        serverAddress = hNet->Params.DHCPServerAddress;
    }
    
    return serverAddress;
}

uint32_t NET_GetDHCPLeaseTime(NET_HandleTypeDef *hNet)
{
    uint32_t time;
    
    NET_OS_LOCK(&(hNet->Params.__Locker))
    {
        time = hNet->Params.DHCPLeaseTime;
    }
    
    return time;
}

uint32_t NET_GetDHCPLeftTime(NET_HandleTypeDef *hNet)
{
    uint32_t time;
    
    NET_OS_LOCK(&(hNet->Params.__Locker))
    {
        time = hNet->Params.DHCPLeftTime;
    }
    
    return time;
}

NET_IPAddressTypeDef NET_GetSNTPServerAddress(NET_HandleTypeDef *hNet)
{
    NET_IPAddressTypeDef serverAddress;
    
    NET_OS_LOCK(&(hNet->Params.__Locker))
    {
        serverAddress = hNet->Params.SNTPServerAddress;
    }
    
    return serverAddress;
}

char * NET_GetSNTPServerName(NET_HandleTypeDef *hNet)
{
    char *pServerName;
    
    NET_OS_LOCK(&(hNet->Params.__Locker))
    {
        pServerName = hNet->Params.SNTPServerName;
    }
    
    return pServerName;
}

char * NET_GetHostName(NET_HandleTypeDef *hNet)
{
    char *pHostName;
    
    NET_OS_LOCK(&(hNet->Params.__Locker))
    {
        pHostName = hNet->Params.HostName;
    }
    
    return pHostName;
}

uint8_t * NET_GetMacAddress(NET_HandleTypeDef *hNet)
{
    uint8_t *pMacAddress;
    
    NET_OS_LOCK(&(hNet->Params.__Locker))
    {
        pMacAddress = hNet->Params.MacAddress;
    }
    
    return pMacAddress;
}

NET_FlagTypeDef NET_GetFlags(NET_HandleTypeDef *hNet)
{
    NET_FlagTypeDef flags;
    
    NET_OS_LOCK(&(hNet->Params.__Locker))
    {
        flags = hNet->Params.Flags;
    }
    
    return flags;
}

NET_ErrorTypeDef NET_SetIPAddress(NET_HandleTypeDef *hNet, NET_IPAddressTypeDef ipAddress)
{
    NET_ErrorTypeDef error;

    NET_OS_LOCK(&(hNet->Params.__Locker))
    {
        hNet->Params.IPAddress = ipAddress;
            
        error = WIZ_SetIPAddress(&(hNet->WizHandle), ipAddress);
    }

    return error;
}

NET_ErrorTypeDef NET_SetSubnetMask(NET_HandleTypeDef *hNet, NET_IPAddressTypeDef subnetMask)
{
    NET_ErrorTypeDef error;
    
    NET_OS_LOCK(&(hNet->Params.__Locker))
    {
        hNet->Params.SubnetMask = subnetMask;

        error = WIZ_SetSubnetMask(&(hNet->WizHandle), subnetMask);
    }
    
    return error;
}

NET_ErrorTypeDef NET_SetDefaultGateway(NET_HandleTypeDef *hNet, NET_IPAddressTypeDef defaultGateway)
{
    NET_ErrorTypeDef error;
    
    NET_OS_LOCK(&(hNet->Params.__Locker))
    {
        hNet->Params.DefaultGateway = defaultGateway;
        
        error = WIZ_SetDefaultGateway(&(hNet->WizHandle), defaultGateway);
    }
    return error;
}

void NET_SetDNSServerAddress(NET_HandleTypeDef *hNet, NET_IPAddressTypeDef serverAddress)
{
    NET_OS_LOCK(&(hNet->Params.__Locker))
    {
        hNet->Params.DNSServerAddress = serverAddress;
    }
}

void NET_SetDHCPServerAddress(NET_HandleTypeDef *hNet, NET_IPAddressTypeDef serverAddress)
{
    NET_OS_LOCK(&(hNet->Params.__Locker))
    {
        hNet->Params.DHCPServerAddress = serverAddress;
    }
}

void NET_SetDHCPLeaseTime(NET_HandleTypeDef *hNet, uint32_t time)
{
    NET_OS_LOCK(&(hNet->Params.__Locker))
    {
        hNet->Params.DHCPLeaseTime = time;
    }
}

void NET_SetDHCPLeftTime(NET_HandleTypeDef *hNet, uint32_t time)
{
    NET_OS_LOCK(&(hNet->Params.__Locker))
    {
        hNet->Params.DHCPLeftTime = time;
    }
}

void NET_SetSNTPServerAddress(NET_HandleTypeDef *hNet, NET_IPAddressTypeDef serverAddress)
{
    NET_OS_LOCK(&(hNet->Params.__Locker))
    {
        hNet->Params.SNTPServerAddress = serverAddress;
    }
}

void NET_SetSNTPServerName(NET_HandleTypeDef *hNet, char *pServerName)
{
    NET_OS_LOCK(&(hNet->Params.__Locker))
    {
        hNet->Params.SNTPServerName = pServerName;
    }
}

void NET_SetHostName(NET_HandleTypeDef *hNet, char *pHostName)
{
    NET_OS_LOCK(&(hNet->Params.__Locker))
    {
        hNet->Params.HostName = pHostName;
    }
}

NET_ErrorTypeDef NET_SetMacAddress(NET_HandleTypeDef *hNet, uint8_t *pMacAddress)
{
    NET_ErrorTypeDef error;
    
    NET_OS_LOCK(&(hNet->Params.__Locker))
    {
        memcpy(hNet->Params.MacAddress, pMacAddress, 6);
        
        error = WIZ_SetMacAddress(&(hNet->WizHandle), hNet->Params.MacAddress);
    }
    return error;
}

void NET_SetFlags(NET_HandleTypeDef *hNet, NET_FlagTypeDef flags)
{
    NET_OS_LOCK(&(hNet->Params.__Locker))
    {
        hNet->Params.Flags = flags;
    }
}
