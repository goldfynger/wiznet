#ifndef __NARODMON_H
#define __NARODMON_H


#include <math.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include <time.h>

#include "FreeRTOS.h"
#include "queue.h"
#include "semphr.h"

#include "dns.h"
#include "net.h"
#include "net_def.h"
#include "net_os.h"
#include "tcp.h"


//#define NM_CONNECT_TRY_COUNT    ((uint8_t)3)
#define NM_POLL_TIMEOUT         ((uint16_t)3000)

#define NM_MIN_SENDING_INTERVAL ((time_t)300)           // 5 minutes.

#define NM_DEVICE_NAME_MAX_LEN  ((uint8_t)19)           // Max length of device name without termintaion zero.

#define NM_SENSOR_MAC_MAX_LEN   ((uint8_t)19)           // Max length of sensor identifier without termintaion zero.
#define NM_SENSOR_NAME_MAX_LEN  ((uint8_t)19)           // Max length of sensor name without termintaion zero.


typedef struct
{
    NET_HandleTypeDef   *NetHandle;    
    DNS_HandleTypeDef   *DnsHandle;
    
    char                Name[NM_DEVICE_NAME_MAX_LEN + 1];
    
    float               Latitude;    
    float               Longitude;    
    float               Elevation;
    
    time_t              Interval;
    
    SemaphoreHandle_t   MutexHandle;
    
    QueueHandle_t       QueueHandle;
    
    char                *RequestBuffer;
    
    char                *ResponseBuffer;
}
NM_HandleTypeDef;

typedef struct
{
    NM_HandleTypeDef    *NmHandle;
    
    char                Mac[NM_SENSOR_MAC_MAX_LEN + 1];
    char                Name[NM_SENSOR_NAME_MAX_LEN + 1];
}
NM_SensorHandleTypeDef;

typedef struct
{
    NM_SensorHandleTypeDef  *SensorHandle;
    
    float                   Value;
    time_t                  Time;
}
NM_SensorValueTypeDef;


NM_HandleTypeDef        * NM_AllocateHandle         (NET_HandleTypeDef *hNet, DNS_HandleTypeDef *hDns, uint8_t msgQueueLen);
void                    NM_FreeHandle               (NM_HandleTypeDef *hNm);
NM_SensorHandleTypeDef  * NM_AllocateSensorHandle   (NM_HandleTypeDef *hNm, char *pMac, uint8_t len);
void                    NM_FreeSensorHandle         (NM_SensorHandleTypeDef *hSensor);
NET_ErrorTypeDef        NM_SetName                  (NM_HandleTypeDef *hNm, char *pName, uint8_t len);
NET_ErrorTypeDef        NM_SetPosition              (NM_HandleTypeDef *hNm, float lat, float lng, float ele);
NET_ErrorTypeDef        NM_SetInterval              (NM_HandleTypeDef *hNm, time_t interval);
NET_ErrorTypeDef        NM_SetSensorMac             (NM_SensorHandleTypeDef *hSensor, char *pMac, uint8_t len);
NET_ErrorTypeDef        NM_SetSensorName            (NM_SensorHandleTypeDef *hSensor, char *pName, uint8_t len);
NET_ErrorTypeDef        NM_AddSensorValue           (NM_SensorHandleTypeDef *hSensor, float value, time_t time);
NET_ErrorTypeDef        NM_Run                      (NM_HandleTypeDef *hNm);


#endif /* __NARODMON_H */
