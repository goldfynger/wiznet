#ifndef __W5100_H
#define __W5100_H


#include "wizchip.h"


#define W5100_SOCKET_COUNT 4


void W5100_GetChipApi(WIZ_ChipApiTypeDef **hApi);


#endif /* __W5100_H */
