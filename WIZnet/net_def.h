#ifndef __NET_DEF_H
#define __NET_DEF_H


#include <stdbool.h>
#include <stdint.h>


#define NET_DEBUG

#ifdef NET_DEBUG
//#define NET_OS_DEBUG
#define WIZ_DEBUG
//#define WIZ_DEBUG_DATA
#define DHCP_DEBUG
#define DNS_DEBUG
#define SNTP_DEBUG
#define HTTP_SRV_DEBUG
//#define HTTP_SRV_DEBUG_DATA
#define NM_DEBUG
#define NM_DEBUG_DATA
#endif

#define NET_POLL_INFINITE            ((uint16_t)0xFFFF)


typedef enum
{
    NET_ERR_OK                      = 0x0,
    
    __WIZ_ERR_OFFSET                = 0,
    
    WIZ_ERR_OK                      = (0x0 << __WIZ_ERR_OFFSET),
    WIZ_ERR_HAL                     = (0x1 << __WIZ_ERR_OFFSET),
    WIZ_ERR_MEM_ALLOCATION          = (0x2 << __WIZ_ERR_OFFSET),
    WIZ_ERR_NULLPTR                 = (0x3 << __WIZ_ERR_OFFSET),
    WIZ_ERR_NO_SOCK_AVAILABLE       = (0x4 << __WIZ_ERR_OFFSET),
    WIZ_ERR_POLL_TIMEOUT            = (0x5 << __WIZ_ERR_OFFSET),
    WIZ_ERR_TX_FULL                 = (0x6 << __WIZ_ERR_OFFSET),
    WIZ_ERR_RX_EMPTY                = (0x7 << __WIZ_ERR_OFFSET),
    WIZ_ERR_SOCKET_OPENING          = (0x8 << __WIZ_ERR_OFFSET),
    WIZ_ERR_SOCKET_LISTENING        = (0x9 << __WIZ_ERR_OFFSET),
    WIZ_ERR_SEND_TIMEOUT            = (0xA << __WIZ_ERR_OFFSET),
    WIZ_ERR_WRONGSIZE               = (0xB << __WIZ_ERR_OFFSET),
    WIZ_ERR_WRONGSOCKET             = (0xC << __WIZ_ERR_OFFSET),
    WIZ_ERR_WRONGOPERATION          = (0xD << __WIZ_ERR_OFFSET),
    WIZ_ERR_WRONGSTATE              = (0xE << __WIZ_ERR_OFFSET),
    
    __WIZ_ERR_MASK                  = (0xF << __WIZ_ERR_OFFSET),
    
    
    TCP_ERR_OK                      = WIZ_ERR_OK,
    TCP_ERR_NO_SOCK_AVAILABLE       = WIZ_ERR_NO_SOCK_AVAILABLE,
    TCP_ERR_RX_EMPTY                = WIZ_ERR_RX_EMPTY,
    
    
    UDP_ERR_OK                      = WIZ_ERR_OK,
    UDP_ERR_POLL_TIMEOUT            = WIZ_ERR_POLL_TIMEOUT,
    UDP_ERR_RX_EMPTY                = WIZ_ERR_RX_EMPTY,
    UDP_ERR_SEND_TIMEOUT            = WIZ_ERR_SEND_TIMEOUT,
    UDP_ERR_WRONGSTATE              = WIZ_ERR_WRONGSTATE,
    
    
    __DHCP_ERR_OFFSET               = 4 + __WIZ_ERR_OFFSET,
    
    DHCP_ERR_OK                     = (0x0 << __DHCP_ERR_OFFSET),
    DHCP_ERR_LOW_LEVEL              = (0x1 << __DHCP_ERR_OFFSET),
    DHCP_ERR_MEM_ALLOCATION         = (0x2 << __DHCP_ERR_OFFSET),
    DHCP_ERR_NULLPTR                = (0x3 << __DHCP_ERR_OFFSET),
    DHCP_ERR_WRONG_STATE            = (0x4 << __DHCP_ERR_OFFSET),
    DHCP_ERR_NO_ANSWER              = (0x5 << __DHCP_ERR_OFFSET),
    DHCP_ERR_SERVER_UNREACHABLE     = (0x6 << __DHCP_ERR_OFFSET),
    DHCP_ERR_BAD_RESPONSE           = (0x7 << __DHCP_ERR_OFFSET),
    
    __DHCP_ERR_MASK                 = (0xF << __DHCP_ERR_OFFSET),
    
    
    __DNS_ERR_OFFSET                = 4 + __DHCP_ERR_OFFSET,
    
    DNS_ERR_OK                      = (0x0 << __DNS_ERR_OFFSET),
    DNS_ERR_LOW_LEVEL               = (0x1 << __DNS_ERR_OFFSET),
    DNS_ERR_MEM_ALLOCATION          = (0x2 << __DNS_ERR_OFFSET),
    DNS_ERR_NULLPTR                 = (0x3 << __DNS_ERR_OFFSET),
    DNS_ERR_WRONG_HOSTNAME          = (0x4 << __DNS_ERR_OFFSET),
    DNS_ERR_CANNOT_RESOLVE          = (0x5 << __DNS_ERR_OFFSET),
    DNS_ERR_BAD_RESPONSE            = (0x6 << __DNS_ERR_OFFSET),
    
    __DNS_ERR_MASK                  = (0xF << __DNS_ERR_OFFSET),
    
    
    __SNTP_ERR_OFFSET               = 4 + __DNS_ERR_OFFSET,
    
    SNTP_ERR_OK                     = (0x0 << __SNTP_ERR_OFFSET),
    SNTP_ERR_LOW_LEVEL              = (0x1 << __SNTP_ERR_OFFSET),
    SNTP_ERR_MEM_ALLOCATION         = (0x2 << __SNTP_ERR_OFFSET),
    SNTP_ERR_NULLPTR                = (0x3 << __SNTP_ERR_OFFSET),
    SNTP_ERR_CANNOT_DETERMINE       = (0x4 << __SNTP_ERR_OFFSET),
    SNTP_ERR_SOURCE_NOT_DEFINED     = (0x5 << __SNTP_ERR_OFFSET),
    
    __SNTP_ERR_MASK                 = (0xF << __SNTP_ERR_OFFSET),
    
    
    __HTTP_SRV_ERR_OFFSET           = 4 + __SNTP_ERR_OFFSET,
    
    HTTP_SRV_ERR_OK                 = (0x0 << __HTTP_SRV_ERR_OFFSET),
    HTTP_SRV_ERR_LOW_LEVEL          = (0x1 << __HTTP_SRV_ERR_OFFSET),
    HTTP_SRV_ERR_MEM_ALLOCATION     = (0x2 << __HTTP_SRV_ERR_OFFSET),
    HTTP_SRV_ERR_NULLPTR            = (0x3 << __HTTP_SRV_ERR_OFFSET),
    HTTP_SRV_ERR_URL_MAX_EXCEEDED   = (0x4 << __HTTP_SRV_ERR_OFFSET),
    HTTP_SRV_ERR_BAD_REQUEST        = (0x5 << __HTTP_SRV_ERR_OFFSET),
    HTTP_SRV_ERR_ZEROLENGTH         = (0x6 << __HTTP_SRV_ERR_OFFSET),
    
    __HTTP_SRV_ERR_MASK             = (0xF << __HTTP_SRV_ERR_OFFSET),
    
    
    __NM_ERR_OFFSET                 = 4 + __HTTP_SRV_ERR_OFFSET,
    
    NM_ERR_OK                       = (0x0 << __NM_ERR_OFFSET),
    NM_ERR_LOW_LEVEL                = (0x1 << __NM_ERR_OFFSET),
    NM_ERR_MEM_ALLOCATION           = (0x2 << __NM_ERR_OFFSET),
    NM_ERR_NULLPTR                  = (0x3 << __NM_ERR_OFFSET),
    NM_ERR_RTOS_FAILURE             = (0x4 << __NM_ERR_OFFSET),
    NM_ERR_WRONG_STRING_LENGTH      = (0x5 << __NM_ERR_OFFSET),
    NM_ERR_WRONG_SENDING_INTERVAL   = (0x6 << __NM_ERR_OFFSET),
    NM_ERR_MSG_QUEUE_FULL           = (0x7 << __NM_ERR_OFFSET),
    
    __NM_SRV_ERR_MASK               = (0xF << __NM_ERR_OFFSET),
}
NET_ErrorTypeDef;

// NETSRV flags.
typedef enum
{
    NET_FLAG_NONE                    = 0x0000,
    
    NET_FLAG_IS_RUN                  = 0x0001, // Indicates that network is run.
    
    NET_FLAG_USE_DHCP_FOR_IP         = 0x0010, // Use DHCP for determine IP settings: IP address, subnet mask, default gateway.
    NET_FLAG_USE_DHCP_FOR_DNS        = 0x0020, // Use DHCP for determine DNS settings: DNS server address.
    NET_FLAG_USE_DHCP_FOR_SNTP       = 0x0040, // Use DHCP for determine SNTP settings: SNTP server address.
    NET_FLAG_USE_DHCP_RESERVED       = 0x0080, // Reserved.    
    NET_FLAG_USE_DHCP_FOR_ALL        = 0x00F0, // All DHCP flags.
    
    NET_FLAG_SNTP_NAME_PRIORITY      = 0x0100, // If specified SNTP name and address both, defines priority.
    
    NET_FLAG_ALL                     = 0xFFFF,
}
NET_FlagTypeDef;

typedef enum
{
    NET_PORTMODE_NONE               = 0x00,
    NET_PORTMODE_TCP                = 0x01,
    NET_PORTMODE_UDP                = 0x02,    
}
NET_PortModeTypeDef;


// Must be little-endian always.
typedef union
{
    uint16_t Value;
    
    struct
    {
        uint8_t Value;
    }
    Byte0;
    
    struct
    {
        uint8_t __Unused0;
        uint8_t Value;
    }
    Byte1;
}
NET_Union16TypeDef;

// Must be little-endian always.
typedef union
{
    uint32_t Value;
    
    struct
    {
        uint8_t Value;
    }
    Byte0;
    
    struct
    {
        uint8_t __Unused0;
        uint8_t Value;
    }
    Byte1;
    
    struct
    {
        uint8_t __Unused0;
        uint8_t __Unused1;
        uint8_t Value;
    }
    Byte2;
    
    struct
    {
        uint8_t __Unused0;
        uint8_t __Unused1;
        uint8_t __Unused2;
        uint8_t Value;
    }
    Byte3;
}
NET_Union32TypeDef;

// Must be little-endian always.
typedef NET_Union32TypeDef NET_IPAddressTypeDef;

// Must be little-endian always.
typedef NET_Union16TypeDef NET_PortTypeDef;


// Endpoint.
typedef struct
{    
    NET_IPAddressTypeDef IPAddress;
    
    NET_PortTypeDef Port;
}
NET_EndPointTypeDef;

// Contains poll info.
typedef struct
{
    uint16_t Interval;

    uint16_t Timeout;
    
    uint32_t BeginTime;
}
NET_PollInfoTypeDef;


static inline uint16_t htons(uint16_t s)
{
    return ((s >> 8) & 0xFF) | ((s << 8) & 0xFF00);
}

static inline uint16_t ntohs(uint16_t s)
{
    return htons(s);
}

static inline uint32_t htonl(uint32_t l)
{
    return ((l >> 24) & 0xFF) | ((l >> 8) & 0xFF00) | ((l << 8) & 0xFF0000) | ((l << 24) & 0xFF000000);
}

static inline uint32_t ntohl(uint32_t l)
{
    return htonl(l);
}


NET_IPAddressTypeDef NET_CreateIPAddress(uint8_t byte3, uint8_t byte2, uint8_t byte1, uint8_t byte0);


#endif /* __NET_DEF_H */
