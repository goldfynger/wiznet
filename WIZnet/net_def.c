#include "net_def.h"


NET_IPAddressTypeDef NET_CreateIPAddress(uint8_t byte3, uint8_t byte2, uint8_t byte1, uint8_t byte0)
{
    NET_IPAddressTypeDef ipAddress;
    
    ipAddress.Byte3.Value = byte3;
    ipAddress.Byte2.Value = byte2;
    ipAddress.Byte1.Value = byte1;
    ipAddress.Byte0.Value = byte0;
    
    return ipAddress;
}
