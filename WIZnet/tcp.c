#include <stdbool.h>
#include <stdint.h>

#include "net_def.h"
#include "net_os.h"
#include "wizchip.h"

#include "tcp.h"


// Open TCP socket if free socket is available and begin connection process.
NET_ErrorTypeDef TCP_ConnectSocket(TCP_SocketHandleTypeDef *hTcp, NET_HandleTypeDef *hNet)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    hTcp->Socket.WizHandle = &hNet->WizHandle;
    
    
    if ((error = WIZ_TakeSocket(&(hTcp->Socket))) != WIZ_ERR_OK) return error;
    
    
    if ((error = hTcp->Socket.WizHandle->ChipApi->WIZ_CHIP_API_TCPOpen(&(hTcp->Socket), hTcp->LocalPort)) != WIZ_ERR_OK) return error;
    
    if ((error = hTcp->Socket.WizHandle->ChipApi->WIZ_CHIP_API_TCPConnect(&(hTcp->Socket), &(hTcp->RemotePoint))) != WIZ_ERR_OK) return error;
    
    
    return error;
}

// Open and listen TCP socket if free socket is available.
NET_ErrorTypeDef TCP_ListenSocket(TCP_SocketHandleTypeDef *hTcp, NET_HandleTypeDef *hNet)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    hTcp->Socket.WizHandle = &hNet->WizHandle;
    
    
    if ((error = WIZ_TakeSocket(&(hTcp->Socket))) != WIZ_ERR_OK) return error;
    
    
    if ((error = hTcp->Socket.WizHandle->ChipApi->WIZ_CHIP_API_TCPOpen(&(hTcp->Socket), hTcp->LocalPort)) != WIZ_ERR_OK) return error;
    
    if ((error = hTcp->Socket.WizHandle->ChipApi->WIZ_CHIP_API_TCPListen(&(hTcp->Socket))) != WIZ_ERR_OK) return error;
    
    
    return error;
}

// Get free size for new TCP data for send in wizchip buffer.
NET_ErrorTypeDef TCP_SendLength(TCP_SocketHandleTypeDef *hTcp, uint16_t *pLength)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    if ((error = hTcp->Socket.WizHandle->ChipApi->WIZ_CHIP_API_SendLength(&(hTcp->Socket), pLength)) != WIZ_ERR_OK) return error;
    
    
    return error;
}

// Send TCP data if free size in wizchip buffer is less or equal to buffer lenfth.
NET_ErrorTypeDef TCP_SendData(TCP_SocketHandleTypeDef *hTcp, uint8_t *pBuffer, uint16_t bufferLength)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    if ((error = hTcp->Socket.WizHandle->ChipApi->WIZ_CHIP_API_SendData(&(hTcp->Socket), pBuffer, bufferLength)) != WIZ_ERR_OK) return error;
    
    
    return error;
}

// Get length of received TCP data in wizchip buffer.
NET_ErrorTypeDef TCP_ReceiveLength(TCP_SocketHandleTypeDef *hTcp, uint16_t *pLength)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    if ((error = hTcp->Socket.WizHandle->ChipApi->WIZ_CHIP_API_ReceiveLength(&(hTcp->Socket), pLength)) != WIZ_ERR_OK) return error;
    
    
    return error;
}

// Recieve TCP data from wizchip buffer.
NET_ErrorTypeDef TCP_ReceiveData(TCP_SocketHandleTypeDef *hTcp, uint8_t *pBuffer, uint16_t bufferLength, uint16_t *pReceivedLength)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    if ((error = hTcp->Socket.WizHandle->ChipApi->WIZ_CHIP_API_ReceiveData(&(hTcp->Socket), pBuffer, bufferLength, pReceivedLength)) != WIZ_ERR_OK) return error;
    
    
    return error;
}

// Begin TCP disconnection process.
NET_ErrorTypeDef TCP_DisconnectSocket(TCP_SocketHandleTypeDef *hTcp)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    if ((error = hTcp->Socket.WizHandle->ChipApi->WIZ_CHIP_API_TCPDisconnect(&(hTcp->Socket))) != WIZ_ERR_OK) return error;
    
    
    return error;
}

// Close TCP socket.
NET_ErrorTypeDef TCP_CloseSocket(TCP_SocketHandleTypeDef *hTcp)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    if ((error = hTcp->Socket.WizHandle->ChipApi->WIZ_CHIP_API_CloseSocket(&(hTcp->Socket))) != WIZ_ERR_OK) return error;
    
    
    if ((error = WIZ_ReleaseSocket(&(hTcp->Socket))) != WIZ_ERR_OK) return error;
    
    
    return error;
}

// Get TCP state of wizchip.
NET_ErrorTypeDef TCP_GetState(TCP_SocketHandleTypeDef *hTcp, TCP_StateTypeDef *pState)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    WIZ_SocketStateTypeDef rawState = WIZ_SOCKET_STATE_NONE;
    
    if ((error = hTcp->Socket.WizHandle->ChipApi->WIZ_CHIP_API_GetSocketState(&(hTcp->Socket), &rawState)) != WIZ_ERR_OK) return error;
    
    switch (rawState)
    {
        case WIZ_SOCKET_STATE_CLOSED: *pState = TCP_STATE_CLOSED; break;
        
        case WIZ_SOCKET_STATE_INIT: *pState = TCP_STATE_OPENED; break;
        
        case WIZ_SOCKET_STATE_LISTEN: *pState = TCP_STATE_LISTEN; break;
        
        case WIZ_SOCKET_STATE_SYNSENT:
        case WIZ_SOCKET_STATE_SYNRECV: *pState = TCP_STATE_CONNECTION; break;
        
        case WIZ_SOCKET_STATE_ESTABLISHED: *pState = TCP_STATE_CONNECTED; break;
        
        case WIZ_SOCKET_STATE_CLOSE_WAIT: *pState = TCP_STATE_CLOSE_WAIT; break;
        
        case WIZ_SOCKET_STATE_FIN_WAIT:
        case WIZ_SOCKET_STATE_CLOSING:
        case WIZ_SOCKET_STATE_TIME_WAIT:
        case WIZ_SOCKET_STATE_LAST_ACK: *pState = TCP_STATE_CLOSING; break;
        
        case WIZ_SOCKET_STATE_ARP_TCP: *pState = (hTcp->Socket.WizHandle->ChipApi->ChipType == WIZ_CHIP_5100) ? TCP_STATE_IN_PROCESS : TCP_STATE_UNKNOWN; break;
        
        default:  *pState = TCP_STATE_UNKNOWN; break;
    }
    
    
    return error;
}

// Get TCP flags of wizchip.
NET_ErrorTypeDef TCP_GetFlags(TCP_SocketHandleTypeDef *hTcp, TCP_FlagTypeDef *pFlags, TCP_FlagTypeDef flagsMask)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    if ((error = hTcp->Socket.WizHandle->ChipApi->WIZ_CHIP_API_GetSocketFlags(&(hTcp->Socket), (WIZ_SocketFlagTypeDef *)pFlags, (WIZ_SocketFlagTypeDef)flagsMask)) != WIZ_ERR_OK) return error;
    
    
    return error;
}

// Poll for one or several of specified TCP flags.
NET_ErrorTypeDef TCP_PollForFlags(TCP_SocketHandleTypeDef *hTcp, TCP_FlagTypeDef *pFlags, TCP_FlagTypeDef flagsMask, uint16_t *pTimeout)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    NET_PollInfoTypeDef pollInfo = { .Interval = TCP_POLL_INTERVAL, .Timeout = *pTimeout, .BeginTime = NET_OS_Ticks() };    
        
    while (true)
    {
        if ((error = TCP_GetFlags(hTcp, pFlags, flagsMask)) != WIZ_ERR_OK) return error;
        
        if (*pFlags & flagsMask) return WIZ_ERR_OK;
        
        if (!(*pTimeout)) return WIZ_ERR_POLL_TIMEOUT;
        
        *pTimeout = NET_WaitPollInterval(&pollInfo);
    }
}

// Gets destintaion end point.
NET_ErrorTypeDef TCP_GetDestination(TCP_SocketHandleTypeDef *hTcp, NET_EndPointTypeDef *pDestination)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    if ((error = hTcp->Socket.WizHandle->ChipApi->WIZ_CHIP_API_GetDestination(&(hTcp->Socket), pDestination)) != WIZ_ERR_OK) return error;
    
    
    return error;
}
