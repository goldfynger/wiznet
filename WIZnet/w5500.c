#include <stdint.h>
#include <string.h>

#include "net_def.h"
#include "net_os.h"
#include "wizchip.h"

#include "w5500.h"


typedef enum
{
    __W5500_OPERATION_MODE_OFFSET   = 0,
    
    W5500_SPI_OPERATION_MODE_VDM    = 0x00,
    W5500_SPI_OPERATION_MODE_FDM_1  = 0x01,
    W5500_SPI_OPERATION_MODE_FDM_2  = 0x02,
    W5500_SPI_OPERATION_MODE_FDM_4  = 0x03,
}
W5500_SpiOperationModeTypeDef;

typedef enum
{
    __W5500_ACCESS_MODE_OFFSET      = 2,
    
    W5500_ACCESS_MODE_READ          = 0,
    W5500_ACCESS_MODE_WRITE         = 1,
}
W5500_AccessModeTypeDef;

typedef enum
{
    __W5500_BLOCK_SELECT_OFFSET     = 3,
    
    __W5500_BLOCK_SOCKET_REGISTER   = 1,
    __W5500_BLOCK_SOCKET_TX_BUFFER  = 2,
    __W5500_BLOCK_SOCKET_RX_BUFFER  = 3,
    
    __W5500_BLOCK_SOCKET_OFFSET     = 4,
    
    W5500_BLOCK_COMMON_REGISTER     = 0x00,
    
    __W5500_BLOCK_SOCKET_0_OFFSET   = __W5500_BLOCK_SOCKET_OFFSET * 0,
    
    W5500_BLOCK_SOCKET_REGISTER_0   = (__W5500_BLOCK_SOCKET_REGISTER + __W5500_BLOCK_SOCKET_0_OFFSET),
    W5500_BLOCK_TX_BUFFER_0         = (__W5500_BLOCK_SOCKET_TX_BUFFER + __W5500_BLOCK_SOCKET_0_OFFSET),
    W5500_BLOCK_RX_BUFFER_0         = (__W5500_BLOCK_SOCKET_RX_BUFFER + __W5500_BLOCK_SOCKET_0_OFFSET),
    
    __W5500_BLOCK_SOCKET_1_OFFSET   = __W5500_BLOCK_SOCKET_OFFSET * 1,
    
    W5500_BLOCK_SOCKET_REGISTER_1   = (__W5500_BLOCK_SOCKET_REGISTER + __W5500_BLOCK_SOCKET_1_OFFSET),
    W5500_BLOCK_TX_BUFFER_1         = (__W5500_BLOCK_SOCKET_TX_BUFFER + __W5500_BLOCK_SOCKET_1_OFFSET),
    W5500_BLOCK_RX_BUFFER_1         = (__W5500_BLOCK_SOCKET_RX_BUFFER + __W5500_BLOCK_SOCKET_1_OFFSET),
    
    __W5500_BLOCK_SOCKET_2_OFFSET   = __W5500_BLOCK_SOCKET_OFFSET * 2,
    
    W5500_BLOCK_SOCKET_REGISTER_2   = (__W5500_BLOCK_SOCKET_REGISTER + __W5500_BLOCK_SOCKET_2_OFFSET),
    W5500_BLOCK_TX_BUFFER_2         = (__W5500_BLOCK_SOCKET_TX_BUFFER + __W5500_BLOCK_SOCKET_2_OFFSET),
    W5500_BLOCK_RX_BUFFER_2         = (__W5500_BLOCK_SOCKET_RX_BUFFER + __W5500_BLOCK_SOCKET_2_OFFSET),
    
    __W5500_BLOCK_SOCKET_3_OFFSET   = __W5500_BLOCK_SOCKET_OFFSET * 3,
    
    W5500_BLOCK_SOCKET_REGISTER_3   = (__W5500_BLOCK_SOCKET_REGISTER + __W5500_BLOCK_SOCKET_3_OFFSET),
    W5500_BLOCK_TX_BUFFER_3         = (__W5500_BLOCK_SOCKET_TX_BUFFER + __W5500_BLOCK_SOCKET_3_OFFSET),
    W5500_BLOCK_RX_BUFFER_3         = (__W5500_BLOCK_SOCKET_RX_BUFFER + __W5500_BLOCK_SOCKET_3_OFFSET),
    
    __W5500_BLOCK_SOCKET_4_OFFSET   = __W5500_BLOCK_SOCKET_OFFSET * 4,
    
    W5500_BLOCK_SOCKET_REGISTER_4   = (__W5500_BLOCK_SOCKET_REGISTER + __W5500_BLOCK_SOCKET_4_OFFSET),
    W5500_BLOCK_TX_BUFFER_4         = (__W5500_BLOCK_SOCKET_TX_BUFFER + __W5500_BLOCK_SOCKET_4_OFFSET),
    W5500_BLOCK_RX_BUFFER_4         = (__W5500_BLOCK_SOCKET_RX_BUFFER + __W5500_BLOCK_SOCKET_4_OFFSET),
    
    __W5500_BLOCK_SOCKET_5_OFFSET   = __W5500_BLOCK_SOCKET_OFFSET * 5,
    
    W5500_BLOCK_SOCKET_REGISTER_5   = (__W5500_BLOCK_SOCKET_REGISTER + __W5500_BLOCK_SOCKET_5_OFFSET),
    W5500_BLOCK_TX_BUFFER_5         = (__W5500_BLOCK_SOCKET_TX_BUFFER + __W5500_BLOCK_SOCKET_5_OFFSET),
    W5500_BLOCK_RX_BUFFER_5         = (__W5500_BLOCK_SOCKET_RX_BUFFER + __W5500_BLOCK_SOCKET_5_OFFSET),
    
    __W5500_BLOCK_SOCKET_6_OFFSET   = __W5500_BLOCK_SOCKET_OFFSET * 6,
    
    W5500_BLOCK_SOCKET_REGISTER_6   = (__W5500_BLOCK_SOCKET_REGISTER + __W5500_BLOCK_SOCKET_6_OFFSET),
    W5500_BLOCK_TX_BUFFER_6         = (__W5500_BLOCK_SOCKET_TX_BUFFER + __W5500_BLOCK_SOCKET_6_OFFSET),
    W5500_BLOCK_RX_BUFFER_6         = (__W5500_BLOCK_SOCKET_RX_BUFFER + __W5500_BLOCK_SOCKET_6_OFFSET),
    
    __W5500_BLOCK_SOCKET_7_OFFSET   = __W5500_BLOCK_SOCKET_OFFSET * 7,
    
    W5500_BLOCK_SOCKET_REGISTER_7   = (__W5500_BLOCK_SOCKET_REGISTER + __W5500_BLOCK_SOCKET_7_OFFSET),
    W5500_BLOCK_TX_BUFFER_7         = (__W5500_BLOCK_SOCKET_TX_BUFFER + __W5500_BLOCK_SOCKET_7_OFFSET),
    W5500_BLOCK_RX_BUFFER_7         = (__W5500_BLOCK_SOCKET_RX_BUFFER + __W5500_BLOCK_SOCKET_7_OFFSET),
}
W5500_BlockTypeDef;

enum __W5500_COMMON_REGISTERS
{
    // Mode register address.
    W5500_MR            = 0x0000,
    
    // Gateway IP register address.
    W5500_GAR           = 0x0001,
    W5500_GAR0          = 0x0001,
    W5500_GAR1          = 0x0002,
    W5500_GAR2          = 0x0003,
    W5500_GAR3          = 0x0004,
    
    // Subnet mask register address.
    W5500_SUBR          = 0x0005,
    W5500_SUBR0         = 0x0005,
    W5500_SUBR1         = 0x0006,
    W5500_SUBR2         = 0x0007,
    W5500_SUBR3         = 0x0008,
    
    // Source MAC register address.
    W5500_SHAR          = 0x0009,
    W5500_SHAR0         = 0x0009,
    W5500_SHAR1         = 0x000A,
    W5500_SHAR2         = 0x000B,
    W5500_SHAR3         = 0x000C,
    W5500_SHAR4         = 0x000D,
    W5500_SHAR5         = 0x000E,
    
    // Source IP register address.
    W5500_SIPR          = 0x000F,
    W5500_SIPR0         = 0x000F,
    W5500_SIPR1         = 0x0010,
    W5500_SIPR2         = 0x0011,
    W5500_SIPR3         = 0x0012,
    
    // Interrupt low level timer register address.
    W5500_INTLEVEL      = 0x0013,
    W5500_INTLEVEL0     = 0x0013,
    W5500_INTLEVEL1     = 0x0014,
    
    // Interrupt register.
    W5500_IR            = 0x0015,
    
    // Interrupt mask register address.
    W5500_IMR           = 0x0016,
    
    // Socket interrupt register address.
    W5500_SIR           = 0x0017,
    
    // Socket interrupt mask register address.
    W5500_SIMR          = 0x0018,
    
    // Timeout register address.
    W5500_RTR           = 0x0019,
    W5500_RTR0          = 0x0019,
    W5500_RTR1          = 0x001A,
    
    // Retry count register address.
    W5500_RCR           = 0x001B,
    
    // PPP LCP request timer register address.
    W5500_PTIMER        = 0x001C,
    
    // PPP LCP magic number register address.
    W5500_PMAGIC        = 0x001D,
    
    // PPP destination MAC register address.
    W5500_PHAR          = 0x001E,
    W5500_PHAR0         = 0x001E,
    W5500_PHAR1         = 0x001F,
    W5500_PHAR2         = 0x0020,
    W5500_PHAR3         = 0x0021,
    W5500_PHAR4         = 0x0022,
    W5500_PHAR5         = 0x0023,
    
    // PPP session identification register address.
    W5500_PSID          = 0x0024,
    W5500_PSID0         = 0x0024,
    W5500_PSID1         = 0x0025,
    
    // PPP maximum segment size register address.
    W5500_PMRU          = 0x0026,
    W5500_PMRU0         = 0x0026,
    W5500_PMRU1         = 0x0027,
    
    // Unreachable IP register address.
    W5500_UIPR          = 0x0028,
    W5500_UIPR0         = 0x0028,
    W5500_UIPR1         = 0x0029,
    W5500_UIPR2         = 0x002A,
    W5500_UIPR3         = 0x002B,
    
    // Unreachable port register address.
    W5500_UPORTR        = 0x002C,
    W5500_UPORTR0       = 0x002C,
    W5500_UPORTR1       = 0x002D,
    
    // PHY configuration register address.
    W5500_PHYCFGR       = 0x002E,
    
    // Chip version register address.
    W5500_VERSIONR      = 0x0039,
    
    // Make enum int16_t.
    W5500_COMMON_REGISTER_MAX = 0x7FFF,
};

enum __W5500_SOCKET_REGISTERS
{
    // Socket mode register address.
    W5500_S_MR          = 0x0000,
    
    // Socket command register address.
    W5500_S_CR          = 0x0001,
    
    // Socket interrupt register address.
    W5500_S_IR          = 0x0002,
    
    // Socket status register address.
    W5500_S_SR          = 0x0003,
    
    // Source port register address.
    W5500_S_PORT        = 0x0004,
    W5500_S_PORT0       = 0x0004,
    W5500_S_PORT1       = 0x0005,
    
    // Destination MAC register address.
    W5500_S_DHAR        = 0x0006,
    W5500_S_DHAR0       = 0x0006,
    W5500_S_DHAR1       = 0x0007,
    W5500_S_DHAR2       = 0x0008,
    W5500_S_DHAR3       = 0x0009,
    W5500_S_DHAR4       = 0x000A,
    W5500_S_DHAR5       = 0x000B,
    
    // Destination IP register address.
    W5500_S_DIPR        = 0x000C,
    W5500_S_DIPR0       = 0x000C,
    W5500_S_DIPR1       = 0x000D,
    W5500_S_DIPR2       = 0x000E,
    W5500_S_DIPR3       = 0x000F,
    
    // Destination port register address.
    W5500_S_DPORT       = 0x0010,
    W5500_S_DPORT0      = 0x0010,
    W5500_S_DPORT1      = 0x0011,
    
    // Socket maximum segment size register address.
    W5500_S_MSSR        = 0x0012,
    W5500_S_MSSR0       = 0x0012,
    W5500_S_MSSR1       = 0x0013,
    
    // IP type of service (TOS) register address.
    W5500_S_TOS         = 0x0015,
    
    // IP time to live (TTL) register address.
    W5500_S_TTL         = 0x0016,
    
    // Receive buffer size register address.
    W5500_S_RXBUFF_SIZE = 0x001E,
    
    // Transmit buffer size register address.
    W5500_S_TXBUFF_SIZE = 0x001F,
    
    // Transmit free memory size register address.
    W5500_S_TX_FSR      = 0x0020,
    W5500_S_TX_FSR0     = 0x0020,
    W5500_S_TX_FSR1     = 0x0021,
    
    // Transmit memory read pointer register address.
    W5500_S_TX_RD       = 0x0022,
    W5500_S_TX_RD0      = 0x0022,
    W5500_S_TX_RD1      = 0x0023,
    
    // Transmit memory write pointer register address.
    W5500_S_TX_WR       = 0x0024,
    W5500_S_TX_WR0      = 0x0024,
    W5500_S_TX_WR1      = 0x0025,
    
    // Received data size register address.
    W5500_S_RX_RSR      = 0x0026,
    W5500_S_RX_RSR0     = 0x0026,
    W5500_S_RX_RSR1     = 0x0027,
    
    // Receive memory read pointer register address.
    W5500_S_RX_RD       = 0x0028,
    W5500_S_RX_RD0      = 0x0028,
    W5500_S_RX_RD1      = 0x0029,
    
    // Receive memory write pointer register address.
    W5500_S_RX_WR       = 0x002A,
    W5500_S_RX_WR0      = 0x002A,
    W5500_S_RX_WR1      = 0x002B,
    
    // Socket interrupt mask register address.
    W5500_S_IMR         = 0x002C,
    
    // Socket fragment offset in IP header register address.
    W5500_S_FRAG        = 0x002E,
    W5500_S_FRAG0       = 0x002E,
    W5500_S_FRAG1       = 0x002D,
    
    // Keep alive timer register address.
    W5500_S_KPALVTR     = 0x002F,
    
    // Make enum int16_t.
    W5500_SOCKET_REGISTER_MAX = 0x7FFF,
};

enum __W5500_MR_VALUES
{
    W5500_MR_FARP           = (1 << 1), // Force ARP.
    W5500_MR_PPPoE          = (1 << 3), // Enable PPPoE.
    W5500_MR_PB             = (1 << 4), // Ping block.
    W5500_MR_WOL            = (1 << 5), // Wake on LAN.
    W5500_MR_RST            = (1 << 7), // Reset.
};

enum __W5500_PHYCFGR_VALUES
{
    W5500_PHYCFGR_LNK       = (1 << 0), // Link status.
    W5500_PHYCFGR_SPD       = (1 << 1), // Speed status.
    W5500_PHYCFGR_DPX       = (1 << 2), // Duplex status.
    W5500_PHYCFGR_10HD      = (0 << 3), // Operation mode configuration.
    W5500_PHYCFGR_10FD      = (1 << 3),
    W5500_PHYCFGR_100HD     = (2 << 3),
    W5500_PHYCFGR_100FD     = (3 << 3),
    W5500_PHYCFGR_100HD_AN  = (4 << 3),
    W5500_PHYCFGR_PWDWN     = (6 << 3),
    W5500_PHYCFGR_ALL       = (7 << 3),
    W5500_PHYCFGR_OPMD      = (1 << 6), // Configure PHY operation mode.
    W5500_PHYCFGR_RST       = (1 << 7), // Reset PHY.
};

enum __W5500_S_MR_VALUES
{
    W5500_S_MR_CLOSED       = (0 << 0), // Unused socket.
    W5500_S_MR_TCP          = (1 << 0), // TCP.
    W5500_S_MR_UDP          = (1 << 1), // UDP.
    W5500_S_MR_MACRAW       = (1 << 2), // MAC layer raw socket (S0_MR support only).
    W5500_S_MR_UCASTB_MIP6B = (1 << 4), // Unicast block / IPv6 block in MACRAW.
    W5500_S_MR_ND_MC_MMB    = (1 << 5), // Use no delayed ACK / multicast / multicast block in MACRAW.
    W5500_S_MR_BCASTB       = (1 << 6), // Broadcast block.
    W5500_S_MR_MULTI_MFEN   = (1 << 7), // Multicasting / MAC filter enable.
};

enum __W5500_S_CR_VALUES
{
    W5500_S_CR_OPEN         = 0x01,     // Initialise or open socket.
    W5500_S_CR_LISTEN       = 0x02,     // Wait connection request in TCP mode (server mode).
    W5500_S_CR_CONNECT      = 0x04,     // Send connection request in TCP mode (client mode).
    W5500_S_CR_DISCON       = 0x08,     // Send closing request in tcp mode.
    W5500_S_CR_CLOSE        = 0x10,     // Close socket.
    W5500_S_CR_SEND         = 0x20,     // Update TXBUFF pointer, send data.
    W5500_S_CR_SEND_MAC     = 0x21,     // Send data with MAC address, so without ARP process.
    W5500_S_CR_SEND_KEEP    = 0x22,     // Send keep alive message.
    W5500_S_CR_RECV         = 0x40,     // Update RXBUFF pointer, receive data.
};

enum __W5500_S_IR_VALUES
{
    W5500_S_IR_SEND_OK      = 0x10,         // Complete sending.
    W5500_S_IR_TIMEOUT      = 0x08,         // Assert timeout.
    W5500_S_IR_RECV         = 0x04,         // Receiving data.
    W5500_S_IR_DISCON       = 0x02,         // Closed socket.
    W5500_S_IR_CON          = 0x01,         // Established connection.
};

enum __W5500_S_SR_VALUES
{
    W5500_S_SR_CLOSED       = 0x00,         // Closed.
    W5500_S_SR_INIT         = 0x13,         // Init state.
    W5500_S_SR_LISTEN       = 0x14,         // Listen state.
    W5500_S_SR_SYNSENT      = 0x15,         // Connection state.
    W5500_S_SR_SYNRECV      = 0x16,         // Connection state.
    W5500_S_SR_ESTABLISHED  = 0x17,         // Success to connect.
    W5500_S_SR_FIN_WAIT     = 0x18,         // Closing state.
    W5500_S_SR_CLOSING      = 0x1A,         // Closing state.
    W5500_S_SR_TIME_WAIT    = 0x1B,         // Closing state.
    W5500_S_SR_CLOSE_WAIT   = 0x1C,         // Disconnection requested from peer host.
    W5500_S_SR_LAST_ACK     = 0x1D,         // Closing state.
    W5500_S_SR_UDP          = 0x22,         // UDP socket.
    W5500_S_SR_MACRAW       = 0x42,         // MAC raw mode socket.
};


typedef struct
{    
    WIZ_SocketInfoTypeDef   SocketInfo[W5500_SOCKET_COUNT];
}
W5500_ChipInfoTypeDef;


WIZ_ChipApiTypeDef _W5500_ChipApi; // Structure with chip API pointers.


static inline uint8_t W5500_FillControlPhase(W5500_SpiOperationModeTypeDef operationMode, W5500_AccessModeTypeDef accessMode, W5500_BlockTypeDef block)
{
    return (uint8_t)(((uint8_t)operationMode << __W5500_OPERATION_MODE_OFFSET) | ((uint8_t)accessMode << __W5500_ACCESS_MODE_OFFSET) | ((uint8_t)block << __W5500_BLOCK_SELECT_OFFSET));
}

static inline W5500_BlockTypeDef W5500_GetSocketRegisterBlock(WIZ_SocketNumberTypeDef socket)
{
     return (W5500_BlockTypeDef)((__W5500_BLOCK_SOCKET_OFFSET * socket) + __W5500_BLOCK_SOCKET_REGISTER);
}

static inline W5500_BlockTypeDef W5500_GetSocketTxBufferBlock(WIZ_SocketNumberTypeDef socket)
{
    return (W5500_BlockTypeDef)((__W5500_BLOCK_SOCKET_OFFSET * socket) + __W5500_BLOCK_SOCKET_TX_BUFFER);
}

static inline W5500_BlockTypeDef W5500_GetSocketRxBufferBlock(WIZ_SocketNumberTypeDef socket)
{
    return (W5500_BlockTypeDef)((__W5500_BLOCK_SOCKET_OFFSET * socket) + __W5500_BLOCK_SOCKET_RX_BUFFER);
}

        NET_ErrorTypeDef W5500_Init                     (WIZ_HandleTypeDef *hWiz);
        NET_ErrorTypeDef W5500_DeInit                   (WIZ_HandleTypeDef *hWiz);

        NET_ErrorTypeDef W5500_SetMemorySizes           (WIZ_HandleTypeDef *hWiz, WIZ_MemorySizeTypeDef *pTxSizes, WIZ_MemorySizeTypeDef *pRxSizes);
        NET_ErrorTypeDef W5500_SetMacAddress            (WIZ_HandleTypeDef *hWiz, uint8_t *pMacAddress);
        NET_ErrorTypeDef W5500_SetIPAddress             (WIZ_HandleTypeDef *hWiz, NET_IPAddressTypeDef ipAddress);
        NET_ErrorTypeDef W5500_SetSubnetMask            (WIZ_HandleTypeDef *hWiz, NET_IPAddressTypeDef subnetMask);
        NET_ErrorTypeDef W5500_SetDefaultGateway        (WIZ_HandleTypeDef *hWiz, NET_IPAddressTypeDef defaultGateway);

        NET_ErrorTypeDef W5500_SendLength               (WIZ_SocketHandleTypeDef *hSocket, uint16_t *pLength);
        NET_ErrorTypeDef W5500_SendData                 (WIZ_SocketHandleTypeDef *hSocket, uint8_t *pBuffer, uint16_t sendLength);
        NET_ErrorTypeDef W5500_ReceiveLength            (WIZ_SocketHandleTypeDef *hSocket, uint16_t *pLength);
        NET_ErrorTypeDef W5500_ReceiveData              (WIZ_SocketHandleTypeDef *hSocket, uint8_t *pBuffer, uint16_t bufferLength, uint16_t *pReceivedLength);
        NET_ErrorTypeDef W5500_CloseSocket              (WIZ_SocketHandleTypeDef *hSocket);

        NET_ErrorTypeDef W5500_TCPOpen                  (WIZ_SocketHandleTypeDef *hSocket, NET_PortTypeDef port);
        NET_ErrorTypeDef W5500_TCPListen                (WIZ_SocketHandleTypeDef *hSocket);
        NET_ErrorTypeDef W5500_TCPConnect               (WIZ_SocketHandleTypeDef *hSocket, NET_EndPointTypeDef *pDestination);
        NET_ErrorTypeDef W5500_TCPDisconnect            (WIZ_SocketHandleTypeDef *hSocket);

        NET_ErrorTypeDef W5500_UDPOpen                  (WIZ_SocketHandleTypeDef *hSocket, NET_PortTypeDef port);
        NET_ErrorTypeDef W5500_UDPSendData              (WIZ_SocketHandleTypeDef *hSocket, uint8_t *pBuffer, uint16_t length, NET_EndPointTypeDef *pDestination);

        NET_ErrorTypeDef W5500_GetSocketState           (WIZ_SocketHandleTypeDef *hSocket, WIZ_SocketStateTypeDef *state);
        NET_ErrorTypeDef W5500_GetSocketFlags           (WIZ_SocketHandleTypeDef *hSocket, WIZ_SocketFlagTypeDef *pFlags, WIZ_SocketFlagTypeDef flagsMask);

        NET_ErrorTypeDef W5500_GetFlags                 (WIZ_HandleTypeDef *hWiz, WIZ_FlagTypeDef *pFlags, WIZ_FlagTypeDef flagsMask);

        NET_ErrorTypeDef W5500_GetDestination           (WIZ_SocketHandleTypeDef *hSocket, NET_EndPointTypeDef *pDestination);
        
        WIZ_SocketInfoTypeDef * W5500_GetSocketInfo     (WIZ_HandleTypeDef *hWiz);

static  NET_ErrorTypeDef W5500_Open                     (WIZ_SocketHandleTypeDef *hSocket, NET_PortTypeDef port);
static  NET_ErrorTypeDef W5500_HAL_WriteByte            (WIZ_HandleTypeDef *hWiz, uint16_t address, W5500_BlockTypeDef block, uint8_t data);
static  NET_ErrorTypeDef W5500_HAL_WriteUShort          (WIZ_HandleTypeDef *hWiz, uint16_t address, W5500_BlockTypeDef block, uint16_t data);
static  NET_ErrorTypeDef W5500_HAL_WriteUInt            (WIZ_HandleTypeDef *hWiz, uint16_t address, W5500_BlockTypeDef block, uint32_t data);
static  NET_ErrorTypeDef W5500_HAL_ReadByte             (WIZ_HandleTypeDef *hWiz, uint16_t address, W5500_BlockTypeDef block, uint8_t *pData);
static  NET_ErrorTypeDef W5500_HAL_ReadUShort           (WIZ_HandleTypeDef *hWiz, uint16_t address, W5500_BlockTypeDef block, uint16_t *pData);
static  NET_ErrorTypeDef W5500_HAL_ReadUInt             (WIZ_HandleTypeDef *hWiz, uint16_t address, W5500_BlockTypeDef block, uint32_t *pData);
static  NET_ErrorTypeDef W5500_HAL_ReadUShortWhileEqual (WIZ_HandleTypeDef *hWiz, uint16_t address, W5500_BlockTypeDef block, uint16_t *pData);
static  NET_ErrorTypeDef W5500_TakeControl              (WIZ_HandleTypeDef *hWiz);
static  NET_ErrorTypeDef W5500_ReleaseControl           (WIZ_HandleTypeDef *hWiz);


// Returns pointer on chip API struct.
void W5500_GetChipApi(WIZ_ChipApiTypeDef **hApi)
{
    NET_OS_CRITICAL()
    {
        if (!_W5500_ChipApi.__IsFilled)
        {
            _W5500_ChipApi.WIZ_CHIP_API_Init                = W5500_Init;
            _W5500_ChipApi.WIZ_CHIP_API_DeInit              = W5500_DeInit;
            
            _W5500_ChipApi.WIZ_CHIP_API_SetMemorySizes      = W5500_SetMemorySizes;
            _W5500_ChipApi.WIZ_CHIP_API_SetMacAddress       = W5500_SetMacAddress;
            _W5500_ChipApi.WIZ_CHIP_API_SetIPAddress        = W5500_SetIPAddress;
            _W5500_ChipApi.WIZ_CHIP_API_SetSubnetMask       = W5500_SetSubnetMask;
            _W5500_ChipApi.WIZ_CHIP_API_SetDefaultGateway   = W5500_SetDefaultGateway;
            
            _W5500_ChipApi.WIZ_CHIP_API_SendLength          = W5500_SendLength;
            _W5500_ChipApi.WIZ_CHIP_API_SendData            = W5500_SendData;
            _W5500_ChipApi.WIZ_CHIP_API_ReceiveLength       = W5500_ReceiveLength;
            _W5500_ChipApi.WIZ_CHIP_API_ReceiveData         = W5500_ReceiveData;
            _W5500_ChipApi.WIZ_CHIP_API_CloseSocket         = W5500_CloseSocket;
            
            _W5500_ChipApi.WIZ_CHIP_API_TCPOpen             = W5500_TCPOpen;
            _W5500_ChipApi.WIZ_CHIP_API_TCPListen           = W5500_TCPListen;
            _W5500_ChipApi.WIZ_CHIP_API_TCPConnect          = W5500_TCPConnect;
            _W5500_ChipApi.WIZ_CHIP_API_TCPDisconnect       = W5500_TCPDisconnect;
            
            _W5500_ChipApi.WIZ_CHIP_API_UDPOpen             = W5500_UDPOpen;
            _W5500_ChipApi.WIZ_CHIP_API_UDPSendData         = W5500_UDPSendData;
            
            _W5500_ChipApi.WIZ_CHIP_API_GetSocketState      = W5500_GetSocketState;
            _W5500_ChipApi.WIZ_CHIP_API_GetSocketFlags      = W5500_GetSocketFlags;
            
            _W5500_ChipApi.WIZ_CHIP_API_GetFlags            = W5500_GetFlags;
            
            _W5500_ChipApi.WIZ_CHIP_API_GetDestination      = W5500_GetDestination;
            
            _W5500_ChipApi.WIZ_CHIP_API_GetSocketInfo       = W5500_GetSocketInfo;
            
            _W5500_ChipApi.SocketCount                      = W5500_SOCKET_COUNT;
            _W5500_ChipApi.ChipType                         = WIZ_CHIP_5500;
            
            _W5500_ChipApi.__IsFilled                       = true;
        }
    }

    *hApi = &_W5500_ChipApi;
}

// Init HAL part of W5500.
NET_ErrorTypeDef W5500_Init(WIZ_HandleTypeDef *hWiz)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK; 
    
    
    if (hWiz->IsInitialized)
    {
        return WIZ_ERR_WRONGOPERATION;
    }
    
    hWiz->IsInitialized = true;
    
    
    if (hWiz->Hal.WIZ_HAL_TransmitReceiveSync   == NULL ||
        hWiz->Hal.WIZ_HAL_TakeMutex             == NULL ||
        hWiz->Hal.WIZ_HAL_ReleaseMutex          == NULL ||
        hWiz->Hal.WIZ_HAL_ActivateNss           == NULL ||
        hWiz->Hal.WIZ_HAL_DeactivateNss         == NULL ||
        hWiz->Hal.WIZ_HAL_RunChip               == NULL ||
        hWiz->Hal.WIZ_HAL_ResetChip             == NULL )
    {
        return WIZ_ERR_NULLPTR;
    }
    
    
    hWiz->Hal.WIZ_HAL_RunChip();
    
    if ((error = W5500_HAL_WriteByte(hWiz, W5500_MR, W5500_BLOCK_COMMON_REGISTER, W5500_MR_RST)) != WIZ_ERR_OK) return error; // Soft reset.
    
    
    hWiz->ChipInfo = (W5500_ChipInfoTypeDef *)net_os_malloc(sizeof(W5500_ChipInfoTypeDef));
    
    if (!hWiz->ChipInfo) return WIZ_ERR_MEM_ALLOCATION;
    
    
    return error;
}

// DeInit HAL part of W5500.
NET_ErrorTypeDef W5500_DeInit(WIZ_HandleTypeDef *hWiz)
{
    if (!(hWiz->IsInitialized))
    {
        return WIZ_ERR_WRONGOPERATION;
    }
    
    hWiz->IsInitialized = false;
    
    
    hWiz->Hal.WIZ_HAL_ResetChip();
    
    
    net_os_free(hWiz->ChipInfo);
    
    
    return WIZ_ERR_OK;
}

// Init memory sizes of W5500.
NET_ErrorTypeDef W5500_SetMemorySizes(WIZ_HandleTypeDef *hWiz, WIZ_MemorySizeTypeDef *pTxSizes, WIZ_MemorySizeTypeDef *pRxSizes)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    WIZ_MemorySizeTypeDef txTotalMemSize = WIZ_MEMSIZE_0;
    WIZ_MemorySizeTypeDef rxTotalMemSize = WIZ_MEMSIZE_0;    
    
    for (WIZ_SocketNumberTypeDef socket = WIZ_SOCKET_0; socket < W5500_SOCKET_COUNT; socket++)
    {
        uint8_t txSize = pTxSizes[socket];
        uint8_t rxSize = pRxSizes[socket];
        
        if (txSize != WIZ_MEMSIZE_0 && txSize != WIZ_MEMSIZE_1 && txSize != WIZ_MEMSIZE_2 && txSize != WIZ_MEMSIZE_4 && txSize != WIZ_MEMSIZE_8 && txSize != WIZ_MEMSIZE_16) return WIZ_ERR_WRONGSIZE;
        if (rxSize != WIZ_MEMSIZE_0 && rxSize != WIZ_MEMSIZE_1 && rxSize != WIZ_MEMSIZE_2 && rxSize != WIZ_MEMSIZE_4 && rxSize != WIZ_MEMSIZE_8 && rxSize != WIZ_MEMSIZE_16) return WIZ_ERR_WRONGSIZE;
        
        txTotalMemSize += txSize;
        rxTotalMemSize += rxSize;
    }
        
    if (txTotalMemSize != WIZ_MEMSIZE_16 || rxTotalMemSize != WIZ_MEMSIZE_16) return WIZ_ERR_WRONGSIZE;
    
    
    if ((error = hWiz->Hal.WIZ_HAL_TakeMutex()) != WIZ_ERR_OK) return error;
    
    for (WIZ_SocketNumberTypeDef socket = WIZ_SOCKET_0; socket < W5500_SOCKET_COUNT; socket++)
    {
        if (error == WIZ_ERR_OK)
        {        
            W5500_BlockTypeDef block = W5500_GetSocketRegisterBlock(socket);
            
            if ((error = W5500_HAL_WriteByte(hWiz, W5500_S_TXBUFF_SIZE, block, pTxSizes[socket])) == WIZ_ERR_OK)
            {            
                error = W5500_HAL_WriteByte(hWiz, W5500_S_RXBUFF_SIZE, block, pRxSizes[socket]);
            }
        }
    }
    
    NET_ErrorTypeDef releaseError = hWiz->Hal.WIZ_HAL_ReleaseMutex();
    
    
    return releaseError ? releaseError : error;
}

// Init Mac address of W5500.
NET_ErrorTypeDef W5500_SetMacAddress(WIZ_HandleTypeDef *hWiz, uint8_t *pMacAddress)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
        
    
    if ((error = hWiz->Hal.WIZ_HAL_TakeMutex()) != WIZ_ERR_OK) return error;
    
    if ((error = W5500_HAL_WriteUShort(hWiz, W5500_SHAR0, W5500_BLOCK_COMMON_REGISTER, htons((uint16_t)*(pMacAddress + 4)))) == WIZ_ERR_OK) // Mac settings.
    {        
        if ((error = W5500_HAL_WriteUShort(hWiz, W5500_SHAR2, W5500_BLOCK_COMMON_REGISTER, htons((uint16_t)*(pMacAddress + 2)))) == WIZ_ERR_OK)
        {            
            error = W5500_HAL_WriteUShort(hWiz, W5500_SHAR4, W5500_BLOCK_COMMON_REGISTER, htons((uint16_t)*(pMacAddress + 0)));
        }
    }
    
    NET_ErrorTypeDef releaseError = hWiz->Hal.WIZ_HAL_ReleaseMutex();
    
    
    return releaseError ? releaseError : error;
}

// Init IP address of W5500.
NET_ErrorTypeDef W5500_SetIPAddress(WIZ_HandleTypeDef *hWiz, NET_IPAddressTypeDef ipAddress)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    if ((error = hWiz->Hal.WIZ_HAL_TakeMutex()) != WIZ_ERR_OK) return error;
    
    error = W5500_HAL_WriteUInt(hWiz, W5500_SIPR, W5500_BLOCK_COMMON_REGISTER, ipAddress.Value); // Ip address settings.
    
    NET_ErrorTypeDef releaseError = hWiz->Hal.WIZ_HAL_ReleaseMutex();
    
    
    return releaseError ? releaseError : error;
}

// Init subnet mask of W5500.
NET_ErrorTypeDef W5500_SetSubnetMask(WIZ_HandleTypeDef *hWiz, NET_IPAddressTypeDef subnetMask)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    if ((error = hWiz->Hal.WIZ_HAL_TakeMutex()) != WIZ_ERR_OK) return error;
    
    error = W5500_HAL_WriteUInt(hWiz, W5500_SUBR, W5500_BLOCK_COMMON_REGISTER, subnetMask.Value); // Subnet mask settings.
    
    NET_ErrorTypeDef releaseError = hWiz->Hal.WIZ_HAL_ReleaseMutex();
    
    
    return releaseError ? releaseError : error;
}

// Init default gateway of W5500.
NET_ErrorTypeDef W5500_SetDefaultGateway(WIZ_HandleTypeDef *hWiz, NET_IPAddressTypeDef defaultGateway)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    if ((error = hWiz->Hal.WIZ_HAL_TakeMutex()) != WIZ_ERR_OK) return error;
    
    error = W5500_HAL_WriteUInt(hWiz, W5500_GAR, W5500_BLOCK_COMMON_REGISTER, defaultGateway.Value); // Gateway settings.
    
    NET_ErrorTypeDef releaseError = hWiz->Hal.WIZ_HAL_ReleaseMutex();
    
    
    return releaseError ? releaseError : error;
}

// Get free size for new data for send in wizchip buffer.
NET_ErrorTypeDef W5500_SendLength(WIZ_SocketHandleTypeDef *hSocket, uint16_t *pLength)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    uint16_t freeSizeRaw = 0;
    
    
    if ((error = hSocket->WizHandle->Hal.WIZ_HAL_TakeMutex()) != WIZ_ERR_OK) return error;
    
    error = W5500_HAL_ReadUShortWhileEqual(hSocket->WizHandle, W5500_S_TX_FSR, W5500_GetSocketRegisterBlock(hSocket->Number), &freeSizeRaw);
    
    *pLength = ntohs(freeSizeRaw);
    
    NET_ErrorTypeDef releaseError = hSocket->WizHandle->Hal.WIZ_HAL_ReleaseMutex();
    
    
    return releaseError ? releaseError : error;
}

// Send data.
NET_ErrorTypeDef W5500_SendData(WIZ_SocketHandleTypeDef *hSocket, uint8_t *pBuffer, uint16_t sendLength)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    // Check free size.
    uint16_t freeSize = 0;
    
    if ((error = W5500_SendLength(hSocket, &freeSize)) != WIZ_ERR_OK) return error;
    
    if (freeSize < sendLength)
    {
        #ifdef WIZ_DEBUG
        
        net_os_printf("W5500: not enough memory to send: socket:%u length:%u free size:%u\r\n", hSocket->Number, sendLength, freeSize);
        
        #endif
        
        return WIZ_ERR_TX_FULL;
    }
    
    #ifdef WIZ_DEBUG
    
    net_os_printf("W5500: send data: socket:%u length:%u free size:%u\r\n", hSocket->Number, sendLength, freeSize);
    
    #endif
    
    
    W5500_BlockTypeDef registerBlock = W5500_GetSocketRegisterBlock(hSocket->Number);
    
    
    // Compute send pointers.
    uint16_t txOffsetRaw;
    
    
    if ((error = hSocket->WizHandle->Hal.WIZ_HAL_TakeMutex()) != WIZ_ERR_OK) return error;
    
    error = W5500_HAL_ReadUShort(hSocket->WizHandle, W5500_S_TX_RD, registerBlock, &txOffsetRaw);
    
    NET_ErrorTypeDef releaseError = hSocket->WizHandle->Hal.WIZ_HAL_ReleaseMutex();
    
    if (releaseError) return releaseError;
    if (error) return error;
    
    
    uint16_t txOffset = ntohs(txOffsetRaw);
    
    
    W5500_BlockTypeDef txBlock = W5500_GetSocketTxBufferBlock(hSocket->Number);
    
    
    // Write data for send.
    for (uint16_t i = 0; i < sendLength; i++)
    {        
        if ((error = W5500_HAL_WriteByte(hSocket->WizHandle, txOffset + i, txBlock, pBuffer[i])) != WIZ_ERR_OK) return error;
        
        #ifdef WIZ_DEBUG_DATA
    
        net_os_printf("%02X ", pBuffer[i]);
    
        #endif
    }
    
    #ifdef WIZ_DEBUG_DATA
    
    net_os_puts("");
    
    #endif
    
    
    uint16_t txEndRaw;
    
    
    if ((error = hSocket->WizHandle->Hal.WIZ_HAL_TakeMutex()) != WIZ_ERR_OK) return error;
    
    error = W5500_HAL_ReadUShort(hSocket->WizHandle, W5500_S_TX_WR, registerBlock, &txEndRaw);
    
    releaseError = hSocket->WizHandle->Hal.WIZ_HAL_ReleaseMutex();
    
    if (releaseError) return releaseError;
    if (error) return error;
    
    
    uint16_t txEnd = ntohs(txEndRaw);
    txEnd += sendLength;
    
    
    if ((error = hSocket->WizHandle->Hal.WIZ_HAL_TakeMutex()) != WIZ_ERR_OK) return error;
    
    error = W5500_HAL_WriteUShort(hSocket->WizHandle, W5500_S_TX_WR, registerBlock, htons(txEnd));
    
    releaseError = hSocket->WizHandle->Hal.WIZ_HAL_ReleaseMutex();
    
    if (releaseError) return releaseError;
    if (error) return error;
    
    
    // Send cmd.
    if ((error = W5500_HAL_WriteByte(hSocket->WizHandle, W5500_S_CR, registerBlock, W5500_S_CR_SEND)) != WIZ_ERR_OK) return error;
    
    
    #ifdef WIZ_DEBUG
    
    net_os_puts("W5500: send data ok");
    
    #endif
    
    return WIZ_ERR_OK;
}

// Get length of received data in wizchip buffer.
NET_ErrorTypeDef W5500_ReceiveLength(WIZ_SocketHandleTypeDef *hSocket, uint16_t *pLength)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    uint16_t receivedLengthRaw = 0;
    
    
    if ((error = hSocket->WizHandle->Hal.WIZ_HAL_TakeMutex()) != WIZ_ERR_OK) return error;
    
    error = W5500_HAL_ReadUShortWhileEqual(hSocket->WizHandle, W5500_S_RX_RSR, W5500_GetSocketRegisterBlock(hSocket->Number), &receivedLengthRaw);
    
    *pLength = ntohs(receivedLengthRaw);
    
    NET_ErrorTypeDef releaseError = hSocket->WizHandle->Hal.WIZ_HAL_ReleaseMutex();
    
    
    return releaseError ? releaseError : error;
}

// Receive data.
NET_ErrorTypeDef W5500_ReceiveData(WIZ_SocketHandleTypeDef *hSocket, uint8_t *pBuffer, uint16_t bufferLength, uint16_t *pReceivedLength)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    // Read length of received data.
    uint16_t availableLength = 0;
    
    if ((error = W5500_ReceiveLength(hSocket, &availableLength)) != WIZ_ERR_OK) return error;
    
    
    *pReceivedLength = availableLength > bufferLength ? bufferLength : availableLength;    
    
    if (*pReceivedLength == 0)
    {
        #ifdef WIZ_DEBUG
        
        net_os_printf("W5500: receive buffer is empty: socket:%u\r\n", hSocket->Number);
        
        #endif
        
        return WIZ_ERR_RX_EMPTY;
    }    
    
    #ifdef WIZ_DEBUG
    
    net_os_printf("W5500: receive data: socket:%u length: %u of %u\r\n", hSocket->Number, *pReceivedLength, availableLength);
    
    #endif
    
    
    W5500_BlockTypeDef registerBlock = W5500_GetSocketRegisterBlock(hSocket->Number);
    
    
    // Compute receive pointers.
    uint16_t rxOffsetRaw;
    
    
    if ((error = hSocket->WizHandle->Hal.WIZ_HAL_TakeMutex()) != WIZ_ERR_OK) return error;
    
    error = W5500_HAL_ReadUShort(hSocket->WizHandle, W5500_S_RX_RD, registerBlock, &rxOffsetRaw);
    
    NET_ErrorTypeDef releaseError = hSocket->WizHandle->Hal.WIZ_HAL_ReleaseMutex();
    
    if (releaseError) return releaseError;
    if (error) return error;
        
        
    uint16_t rxOffset = ntohs(rxOffsetRaw);
    
    
    W5500_BlockTypeDef rxBlock = W5500_GetSocketRxBufferBlock(hSocket->Number);    
    
    
    // Read received data.
    for (uint16_t i = 0; i < *pReceivedLength; i++)
    {        
        if ((error = W5500_HAL_ReadByte(hSocket->WizHandle, rxOffset + i, rxBlock, (pBuffer + i))) != WIZ_ERR_OK) return error;
        
        #ifdef WIZ_DEBUG_DATA
    
        net_os_printf("%02X ", pBuffer[i]);
    
        #endif
    }
    
    #ifdef WIZ_DEBUG_DATA
    
    net_os_puts("");
    
    #endif
    
    
    rxOffset += *pReceivedLength;
    
    
    if ((error = hSocket->WizHandle->Hal.WIZ_HAL_TakeMutex()) != WIZ_ERR_OK) return error;
    
    error = W5500_HAL_WriteUShort(hSocket->WizHandle, W5500_S_RX_RD, registerBlock, htons(rxOffset));
    
    releaseError = hSocket->WizHandle->Hal.WIZ_HAL_ReleaseMutex();
    
    if (releaseError) return releaseError;
    if (error) return error;
    
    
    // Receive cmd.
    if ((error = W5500_HAL_WriteByte(hSocket->WizHandle, W5500_S_CR, registerBlock, W5500_S_CR_RECV)) != WIZ_ERR_OK) return error;
    
    
    #ifdef WIZ_DEBUG
    
    net_os_puts("W5500: receive data ok");
    
    #endif
    
    return WIZ_ERR_OK;
}

// Close socket.
NET_ErrorTypeDef W5500_CloseSocket(WIZ_SocketHandleTypeDef *hSocket)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;    
    
    
    if ((error = W5500_HAL_WriteByte(hSocket->WizHandle, W5500_S_CR, W5500_GetSocketRegisterBlock(hSocket->Number), W5500_S_CR_CLOSE)) != WIZ_ERR_OK) return error;
    
    
    // Clear all flags after close.
    WIZ_SocketFlagTypeDef flags = WIZ_SOCKET_FLAG_NONE;
    
    if ((error = W5500_GetSocketFlags(hSocket, &flags, WIZ_SOCKET_FLAG_ALL)) != WIZ_ERR_OK) return error;
    
    
    #ifdef WIZ_DEBUG
    
    net_os_printf("W5500: close socket: %u\r\n", hSocket->Number);
    
    #endif
    
    return error;
}

// Open in TCP mode.
NET_ErrorTypeDef W5500_TCPOpen(WIZ_SocketHandleTypeDef *hSocket, NET_PortTypeDef port)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    W5500_BlockTypeDef block = W5500_GetSocketRegisterBlock(hSocket->Number);
    
    
    #ifdef WIZ_DEBUG
    
    net_os_printf("W5500: TCP open: socket:%u port:%u\r\n", hSocket->Number, port.Value);
    
    #endif
    
    
    WIZ_RememberPort(hSocket, port, NET_PORTMODE_TCP);
    
    
    if ((error = W5500_HAL_WriteByte(hSocket->WizHandle, W5500_S_MR, block, W5500_S_MR_TCP)) != WIZ_ERR_OK) return error;
    
    
    if ((error = W5500_Open(hSocket, port)) != WIZ_ERR_OK) return error; 
    
    
    uint8_t status = 0;
    
    if ((error = W5500_HAL_ReadByte(hSocket->WizHandle, W5500_S_SR, block, &status)) != WIZ_ERR_OK) return error;
    
    #ifdef WIZ_DEBUG
    
    if (status == W5500_S_SR_INIT) net_os_puts("W5500: TCP open ok");
    
    #endif
    
    return (status == W5500_S_SR_INIT) ? WIZ_ERR_OK : WIZ_ERR_SOCKET_OPENING;
}

// Begin TCP listening.
NET_ErrorTypeDef W5500_TCPListen(WIZ_SocketHandleTypeDef *hSocket)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    W5500_BlockTypeDef block = W5500_GetSocketRegisterBlock(hSocket->Number);
    
    
    if ((error = W5500_HAL_WriteByte(hSocket->WizHandle, W5500_S_CR, block, W5500_S_CR_LISTEN)) != WIZ_ERR_OK) return error;
    
    
    uint8_t status = 0;
    
    if ((error = W5500_HAL_ReadByte(hSocket->WizHandle, W5500_S_SR, block, &status)) != WIZ_ERR_OK) return error;
    
    return (status == W5500_S_SR_LISTEN) ? WIZ_ERR_OK : WIZ_ERR_SOCKET_LISTENING;
}

// Begin TCP connection.
NET_ErrorTypeDef W5500_TCPConnect(WIZ_SocketHandleTypeDef *hSocket, NET_EndPointTypeDef *pDestination)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    W5500_BlockTypeDef block = W5500_GetSocketRegisterBlock(hSocket->Number);
    
    
    if ((error = hSocket->WizHandle->Hal.WIZ_HAL_TakeMutex()) != WIZ_ERR_OK) return error;
    
    if ((error = W5500_HAL_WriteUInt(hSocket->WizHandle, W5500_S_DIPR, block, pDestination->Port.Value)) == WIZ_ERR_OK)
    {
        error = W5500_HAL_WriteUShort(hSocket->WizHandle, W5500_S_DPORT, block, pDestination->Port.Value);
    }
    
    NET_ErrorTypeDef releaseError = hSocket->WizHandle->Hal.WIZ_HAL_ReleaseMutex();
    
    if (releaseError) return releaseError;
    if (error) return error;
    
    
    if ((error = W5500_HAL_WriteByte(hSocket->WizHandle, W5500_S_CR, block, W5500_S_CR_CONNECT)) != WIZ_ERR_OK) return error;
    
    return WIZ_ERR_OK;
}

// Begin TCP disconnection.
NET_ErrorTypeDef W5500_TCPDisconnect(WIZ_SocketHandleTypeDef *hSocket)
{
    #ifdef WIZ_DEBUG
    
    net_os_printf("W5500: TCP disconnection: socket:%u\r\n", hSocket->Number);
    
    #endif
    
    
    return W5500_HAL_WriteByte(hSocket->WizHandle, W5500_S_CR, W5500_GetSocketRegisterBlock(hSocket->Number), W5500_S_CR_DISCON);
}

// Open in UDP mode.
NET_ErrorTypeDef W5500_UDPOpen(WIZ_SocketHandleTypeDef *hSocket, NET_PortTypeDef port)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    W5500_BlockTypeDef block = W5500_GetSocketRegisterBlock(hSocket->Number);
    
    
    #ifdef WIZ_DEBUG
    
    net_os_printf("W5500: UDP open: socket:%u port:%u\r\n", hSocket->Number, port.Value);
    
    #endif
    
    
    WIZ_RememberPort(hSocket, port, NET_PORTMODE_UDP);
    
    
    if ((error = W5500_HAL_WriteByte(hSocket->WizHandle, W5500_S_MR, block, W5500_S_MR_UDP)) != WIZ_ERR_OK) return error;
    
    
    if ((error = W5500_Open(hSocket, port)) != WIZ_ERR_OK) return error;
    
    
    uint8_t status = 0;    
    
    if ((error = W5500_HAL_ReadByte(hSocket->WizHandle, W5500_S_SR, block, &status)) != WIZ_ERR_OK) return error;
    
    #ifdef WIZ_DEBUG
    
    if (status == W5500_S_SR_UDP) net_os_puts("W5500: UDP open ok");
    
    #endif
    
    return (status == W5500_S_SR_UDP) ? WIZ_ERR_OK : WIZ_ERR_SOCKET_OPENING;
}

// Send data in UDP mode.
NET_ErrorTypeDef W5500_UDPSendData(WIZ_SocketHandleTypeDef *hSocket, uint8_t *pBuffer, uint16_t length, NET_EndPointTypeDef *pDestination)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    W5500_BlockTypeDef block = W5500_GetSocketRegisterBlock(hSocket->Number);
    
    
    #ifdef WIZ_DEBUG
    
    net_os_printf("W5500: UDP send: socket:%u dst:%u.%u.%u.%u:%u\r\n", hSocket->Number,
        pDestination->IPAddress.Byte3.Value, pDestination->IPAddress.Byte2.Value, pDestination->IPAddress.Byte1.Value, pDestination->IPAddress.Byte0.Value, pDestination->Port.Value);
    
    #endif
    
    
    if ((error = hSocket->WizHandle->Hal.WIZ_HAL_TakeMutex()) != WIZ_ERR_OK) return error;
    
    if ((error = W5500_HAL_WriteUInt(hSocket->WizHandle, W5500_S_DIPR, block, pDestination->Port.Value)) == WIZ_ERR_OK)
    {
        error = W5500_HAL_WriteUShort(hSocket->WizHandle, W5500_S_DPORT, block, pDestination->Port.Value);
    }
    
    NET_ErrorTypeDef releaseError = hSocket->WizHandle->Hal.WIZ_HAL_ReleaseMutex();
    
    if (releaseError) return releaseError;
    if (error) return error;
    
    
    if ((error = W5500_SendData(hSocket, pBuffer, length)) != WIZ_ERR_OK) return error;
    
    
    return WIZ_ERR_OK;
}

// Get status.
NET_ErrorTypeDef W5500_GetSocketState(WIZ_SocketHandleTypeDef *hSocket, WIZ_SocketStateTypeDef *pState)
{
    return W5500_HAL_ReadByte(hSocket->WizHandle, W5500_S_SR, W5500_GetSocketRegisterBlock(hSocket->Number), (uint8_t *)pState);
}

// Get socket flags for specified mask.
NET_ErrorTypeDef W5500_GetSocketFlags(WIZ_SocketHandleTypeDef *hSocket, WIZ_SocketFlagTypeDef *pFlags, WIZ_SocketFlagTypeDef flagsMask)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    if (flagsMask)
    {
        if ((error = hSocket->WizHandle->Hal.WIZ_HAL_TakeMutex()) != WIZ_ERR_OK) return error;
        
        W5500_BlockTypeDef block = W5500_GetSocketRegisterBlock(hSocket->Number);
    
        if ((error = W5500_HAL_ReadByte(hSocket->WizHandle, W5500_S_IR, block, (uint8_t *)pFlags)) == WIZ_ERR_OK)
        {
            *pFlags &= flagsMask;
            
            error = W5500_HAL_WriteByte(hSocket->WizHandle, W5500_S_IR, block, *((uint8_t *)pFlags));
        }
        
        NET_ErrorTypeDef releaseError = hSocket->WizHandle->Hal.WIZ_HAL_ReleaseMutex();
        
        if (releaseError) return releaseError;
        if (error) return error;
    }
    else
    {
        *pFlags = WIZ_SOCKET_FLAG_NONE;
    }
    
    
    return WIZ_ERR_OK;
}

// Get chip flags for specified mask.
NET_ErrorTypeDef W5500_GetFlags(WIZ_HandleTypeDef *hWiz, WIZ_FlagTypeDef *pFlags, WIZ_FlagTypeDef flagsMask)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    if (flagsMask)
    {    
        uint8_t flagsRaw = 0;
        uint8_t socketFlagsRaw = 0;
        
        
        if ((error = hWiz->Hal.WIZ_HAL_TakeMutex()) != WIZ_ERR_OK) return error;
    
        if ((error = W5500_HAL_ReadByte(hWiz, W5500_IR, W5500_BLOCK_COMMON_REGISTER, &flagsRaw)) == WIZ_ERR_OK)
        {
            uint8_t flagsForClear = flagsRaw & (uint8_t)((((uint16_t)flagsMask) >> 8) & 0xFF); // Only IR bits must be cleared.
            
            if (flagsForClear)
            {
                error = W5500_HAL_WriteByte(hWiz, W5500_IR, W5500_BLOCK_COMMON_REGISTER, flagsForClear);
            }
            
            if (error == WIZ_ERR_OK)
            {
                error = W5500_HAL_ReadByte(hWiz, W5500_SIR, W5500_BLOCK_COMMON_REGISTER, &socketFlagsRaw);
            }
        }
        
        NET_ErrorTypeDef releaseError = hWiz->Hal.WIZ_HAL_ReleaseMutex();
        
        if (releaseError) return releaseError;
        if (error) return error;
                
        
        *pFlags = (WIZ_FlagTypeDef)((flagsRaw << 8) + socketFlagsRaw);
    }
    else
    {
        *pFlags = WIZ_FLAG_NONE;
    }
    
    
    return WIZ_ERR_OK;
}

// Gets destination end point.
NET_ErrorTypeDef W5500_GetDestination(WIZ_SocketHandleTypeDef *hSocket, NET_EndPointTypeDef *pDestination)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    if ((error = hSocket->WizHandle->Hal.WIZ_HAL_TakeMutex()) != WIZ_ERR_OK) return error;
    
    W5500_BlockTypeDef block = W5500_GetSocketRegisterBlock(hSocket->Number);                    
                    
    if ((error = W5500_HAL_ReadUInt(hSocket->WizHandle, W5500_S_DIPR, block, &(pDestination->IPAddress.Value))) == WIZ_ERR_OK)
    {
        error = W5500_HAL_ReadUShort(hSocket->WizHandle, W5500_S_DPORT, block, &(pDestination->Port.Value));
    }
    
    NET_ErrorTypeDef releaseError = hSocket->WizHandle->Hal.WIZ_HAL_ReleaseMutex();
    
    if (releaseError) return releaseError;
    if (error) return error;
    
    
    return WIZ_ERR_OK;
}

WIZ_SocketInfoTypeDef * W5500_GetSocketInfo(WIZ_HandleTypeDef *hWiz)
{
    return ((W5500_ChipInfoTypeDef *)hWiz->ChipInfo)->SocketInfo;
}

// Open socket in current (selected) mode.
static NET_ErrorTypeDef W5500_Open(WIZ_SocketHandleTypeDef *hSocket, NET_PortTypeDef port)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    W5500_BlockTypeDef block = W5500_GetSocketRegisterBlock(hSocket->Number);
    
    
    uint8_t status = 0;
    
    if ((error = W5500_HAL_ReadByte(hSocket->WizHandle, W5500_S_SR, block, &status)) != WIZ_ERR_OK) return error;
    
    if (status != W5500_S_SR_CLOSED)
    {
        #ifdef WIZ_DEBUG
        
        net_os_puts("W5500: warn: socket already opened");
        
        #endif
        
        if ((error = W5500_HAL_WriteByte(hSocket->WizHandle, W5500_S_CR, block, W5500_S_CR_CLOSE)) != WIZ_ERR_OK) return error;
    }
    
    
    if ((error = hSocket->WizHandle->Hal.WIZ_HAL_TakeMutex()) != WIZ_ERR_OK) return error;
    
    error = W5500_HAL_WriteUShort(hSocket->WizHandle, W5500_S_PORT0, block, port.Value);
    
    NET_ErrorTypeDef releaseError = hSocket->WizHandle->Hal.WIZ_HAL_ReleaseMutex();
    
    if (releaseError) return releaseError;
    if (error) return error;
    
    
    if ((error = W5500_HAL_WriteByte(hSocket->WizHandle, W5500_S_CR, block, W5500_S_CR_OPEN)) != WIZ_ERR_OK) return error;
    
    
    return WIZ_ERR_OK;
}

// Writes one byte to specified address.
static NET_ErrorTypeDef W5500_HAL_WriteByte(WIZ_HandleTypeDef *hWiz, uint16_t address, W5500_BlockTypeDef block, uint8_t data)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    NET_Union16TypeDef addressRaw = { .Value = address };
    
    uint8_t controlPhase = W5500_FillControlPhase(W5500_SPI_OPERATION_MODE_FDM_1, W5500_ACCESS_MODE_WRITE, block);
    
    // Compile-time-length array, placed on stack.
    uint8_t buffer[] = { addressRaw.Byte1.Value, addressRaw.Byte0.Value, controlPhase, data }; // BigEndian.
    
    if ((error = W5500_TakeControl(hWiz)) != WIZ_ERR_OK) return error;
    
    if ((error = hWiz->Hal.WIZ_HAL_TransmitReceiveSync(buffer, sizeof(buffer))) != WIZ_ERR_OK) return error;
    
    NET_ErrorTypeDef releaseError = W5500_ReleaseControl(hWiz);
    
    return releaseError ? releaseError : error;
}

// Writes two bytes to specified address.
static NET_ErrorTypeDef W5500_HAL_WriteUShort(WIZ_HandleTypeDef *hWiz, uint16_t address, W5500_BlockTypeDef block, uint16_t data)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    NET_Union16TypeDef addressRaw = { .Value = address };
    NET_Union16TypeDef dataRaw = { .Value = data };
    
    uint8_t controlPhase = W5500_FillControlPhase(W5500_SPI_OPERATION_MODE_FDM_2, W5500_ACCESS_MODE_WRITE, block);
    
    // Compile-time-length array, placed on stack.
    uint8_t buffer[] = { addressRaw.Byte1.Value, addressRaw.Byte0.Value, controlPhase, dataRaw.Byte1.Value, dataRaw.Byte0.Value }; // BigEndian.
    
    if ((error = W5500_TakeControl(hWiz)) != WIZ_ERR_OK) return error;
    
    if ((error = hWiz->Hal.WIZ_HAL_TransmitReceiveSync(buffer, sizeof(buffer))) != WIZ_ERR_OK) return error;
    
    NET_ErrorTypeDef releaseError = W5500_ReleaseControl(hWiz);
    
    return releaseError ? releaseError : error;
}

// Writes four bytes to specified address.
static NET_ErrorTypeDef W5500_HAL_WriteUInt(WIZ_HandleTypeDef *hWiz, uint16_t address, W5500_BlockTypeDef block, uint32_t data)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    NET_Union16TypeDef addressRaw = { .Value = address };
    NET_Union32TypeDef dataRaw = { .Value = data };
    
    uint8_t controlPhase = W5500_FillControlPhase(W5500_SPI_OPERATION_MODE_FDM_4, W5500_ACCESS_MODE_WRITE, block);
    
    // Compile-time-length array, placed on stack.
    uint8_t buffer[] = { addressRaw.Byte1.Value, addressRaw.Byte0.Value, controlPhase, dataRaw.Byte3.Value, dataRaw.Byte2.Value, dataRaw.Byte1.Value, dataRaw.Byte0.Value }; // BigEndian.
    
    if ((error = W5500_TakeControl(hWiz)) != WIZ_ERR_OK) return error;
    
    if ((error = hWiz->Hal.WIZ_HAL_TransmitReceiveSync(buffer, sizeof(buffer))) != WIZ_ERR_OK) return error;
    
    NET_ErrorTypeDef releaseError = W5500_ReleaseControl(hWiz);
    
    return releaseError ? releaseError : error;
}

// Reads one byte from specified address.
static NET_ErrorTypeDef W5500_HAL_ReadByte(WIZ_HandleTypeDef *hWiz, uint16_t address, W5500_BlockTypeDef block, uint8_t *pData)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    NET_Union16TypeDef addressRaw = { .Value = address };
    
    uint8_t controlPhase = W5500_FillControlPhase(W5500_SPI_OPERATION_MODE_FDM_1, W5500_ACCESS_MODE_READ, block);
    
    // Compile-time-length array, placed on stack.
    uint8_t buffer[] = { addressRaw.Byte1.Value, addressRaw.Byte0.Value, controlPhase, 0 }; // BigEndian.
    
    if ((error = W5500_TakeControl(hWiz)) != WIZ_ERR_OK) return error;
    
    if ((error = hWiz->Hal.WIZ_HAL_TransmitReceiveSync(buffer, sizeof(buffer))) != WIZ_ERR_OK) return error;
    
    NET_ErrorTypeDef releaseError = W5500_ReleaseControl(hWiz);
    
    error = releaseError ? releaseError : error;
    
    if (!error)
    {
        *pData = buffer[3];
    }
    
    return error;
}

// Reads two bytes from specified address.
static NET_ErrorTypeDef W5500_HAL_ReadUShort(WIZ_HandleTypeDef *hWiz, uint16_t address, W5500_BlockTypeDef block, uint16_t *pData)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    NET_Union16TypeDef addressRaw = { .Value = address };
    
    uint8_t controlPhase = W5500_FillControlPhase(W5500_SPI_OPERATION_MODE_FDM_2, W5500_ACCESS_MODE_READ, block);
    
    // Compile-time-length array, placed on stack.
    uint8_t buffer[] = { addressRaw.Byte1.Value, addressRaw.Byte0.Value, controlPhase, 0, 0 }; // BigEndian.
    
    if ((error = W5500_TakeControl(hWiz)) != WIZ_ERR_OK) return error;
    
    if ((error = hWiz->Hal.WIZ_HAL_TransmitReceiveSync(buffer, sizeof(buffer))) != WIZ_ERR_OK) return error;
    
    NET_ErrorTypeDef releaseError = W5500_ReleaseControl(hWiz);
    
    error = releaseError ? releaseError : error;
    
    if (!error)
    {
        NET_Union16TypeDef *dataRaw = (NET_Union16TypeDef *)pData;    
        dataRaw->Byte1.Value = buffer[3];
        dataRaw->Byte0.Value = buffer[4];
    }
    
    return error;
}

// Reads four bytes from specified address.
static NET_ErrorTypeDef W5500_HAL_ReadUInt(WIZ_HandleTypeDef *hWiz, uint16_t address, W5500_BlockTypeDef block, uint32_t *pData)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    NET_Union16TypeDef addressRaw = { .Value = address };
    
    uint8_t controlPhase = W5500_FillControlPhase(W5500_SPI_OPERATION_MODE_FDM_4, W5500_ACCESS_MODE_READ, block);
    
    // Compile-time-length array, placed on stack.
    uint8_t buffer[] = { addressRaw.Byte1.Value, addressRaw.Byte0.Value, controlPhase, 0, 0, 0, 0 }; // BigEndian.
    
    if ((error = W5500_TakeControl(hWiz)) != WIZ_ERR_OK) return error;
    
    if ((error = hWiz->Hal.WIZ_HAL_TransmitReceiveSync(buffer, sizeof(buffer))) != WIZ_ERR_OK) return error;
    
    NET_ErrorTypeDef releaseError = W5500_ReleaseControl(hWiz);
    
    error = releaseError ? releaseError : error;
    
    if (!error)
    {
        NET_Union32TypeDef *dataRaw = (NET_Union32TypeDef *)pData;    
        dataRaw->Byte3.Value = buffer[3];
        dataRaw->Byte2.Value = buffer[4];
        dataRaw->Byte1.Value = buffer[5];
        dataRaw->Byte0.Value = buffer[6];
    }
    
    return error;
}

// Reads two bytes from specified address minimum twice, until the results are equal.
static NET_ErrorTypeDef W5500_HAL_ReadUShortWhileEqual(WIZ_HandleTypeDef *hWiz, uint16_t address, W5500_BlockTypeDef block, uint16_t *pData)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    uint16_t data = 0;
    
    
    if ((error = hWiz->Hal.WIZ_HAL_TakeMutex()) != WIZ_ERR_OK) return error;
                    
    if ((error = W5500_HAL_ReadUShort(hWiz, address, block, &data)) == WIZ_ERR_OK)
    {
        while (true)
        {
            if ((error = W5500_HAL_ReadUShort(hWiz, address, block, pData)) == WIZ_ERR_OK)
            {            
                if (data == *pData) break;
                
                data = *pData;
            }
        }
    }
    
    NET_ErrorTypeDef releaseError = hWiz->Hal.WIZ_HAL_ReleaseMutex();
    
    
    return releaseError ? releaseError : error;
}

static NET_ErrorTypeDef W5500_TakeControl(WIZ_HandleTypeDef *hWiz)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    if ((error = hWiz->Hal.WIZ_HAL_TakeMutex()) != WIZ_ERR_OK) return error;
    
    hWiz->Hal.WIZ_HAL_ActivateNss();
    
    return error;
}

static NET_ErrorTypeDef W5500_ReleaseControl(WIZ_HandleTypeDef *hWiz)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    hWiz->Hal.WIZ_HAL_DeactivateNss();
    
    if ((error = hWiz->Hal.WIZ_HAL_ReleaseMutex()) != WIZ_ERR_OK) return error;
    
    return error;
}
