#ifndef __WIZCHIP_H
#define __WIZCHIP_H


#include <stdbool.h>
#include <stdint.h>

#include "net_def.h"


// WIZnet chip type.
typedef enum
{
    WIZ_CHIP_5100 = 5100,
    WIZ_CHIP_5500 = 5500,
}
WIZ_ChipTypeDef;

// Existing socket numbers.
typedef enum
{
    WIZ_SOCKET_0 = 0,
    WIZ_SOCKET_1 = 1,
    WIZ_SOCKET_2 = 2,
    WIZ_SOCKET_3 = 3,    
    WIZ_SOCKET_4 = 4,
    WIZ_SOCKET_5 = 5,
    WIZ_SOCKET_6 = 6,
    WIZ_SOCKET_7 = 7,
    
    __WIZ_SOCKET_NONE = 127, // Disabled some warnings.
}
WIZ_SocketNumberTypeDef;

// Socket memory sizes.
typedef enum
{
    WIZ_MEMSIZE_0 = 0,
    WIZ_MEMSIZE_1 = 1,
    WIZ_MEMSIZE_2 = 2,
    WIZ_MEMSIZE_4 = 4,
    WIZ_MEMSIZE_8 = 8,
    
    WIZ_MEMSIZE_16 = 16,
}
WIZ_MemorySizeTypeDef;

// WIZ chip flags.
typedef enum
{
    WIZ_FLAG_NONE       = 0,
    
    WIZ_FLAG_SOCKET_0   = (1 << 0),
    WIZ_FLAG_SOCKET_1   = (1 << 1),
    WIZ_FLAG_SOCKET_2   = (1 << 2),
    WIZ_FLAG_SOCKET_3   = (1 << 3),
    
    WIZ_FLAG_SOCKET_4   = (1 << 4),
    WIZ_FLAG_SOCKET_5   = (1 << 5),
    WIZ_FLAG_SOCKET_6   = (1 << 6),
    WIZ_FLAG_SOCKET_7   = (1 << 7),
    
    WIZ_FLAG_MP         = (1 << 12),
    
    WIZ_FLAG_PPPoE      = (1 << 13),
    WIZ_FLAG_UNREACH    = (1 << 14),
    WIZ_FLAG_CONFLICT   = (1 << 15),
    
    WIZ_FLAG_ALL        = 0xFFFF,
}
WIZ_FlagTypeDef;

// Socket states.
typedef enum
{
    WIZ_SOCKET_STATE_NONE           = 0x00,
    
    WIZ_SOCKET_STATE_CLOSED         = 0x00, // Closed.
    WIZ_SOCKET_STATE_INIT           = 0x13, // Init state.
    WIZ_SOCKET_STATE_LISTEN         = 0x14, // Listen state.
    WIZ_SOCKET_STATE_SYNSENT        = 0x15, // Connection state.
    WIZ_SOCKET_STATE_SYNRECV        = 0x16, // Connection state.
    WIZ_SOCKET_STATE_ESTABLISHED    = 0x17, // Success to connect.
    WIZ_SOCKET_STATE_FIN_WAIT       = 0x18, // Closing state.
    WIZ_SOCKET_STATE_CLOSING        = 0x1A, // Closing state.
    WIZ_SOCKET_STATE_TIME_WAIT      = 0x1B, // Closing state.
    WIZ_SOCKET_STATE_CLOSE_WAIT     = 0x1C, // Disconnection requested from peer host.
    WIZ_SOCKET_STATE_LAST_ACK       = 0x1D, // Closing state.
    WIZ_SOCKET_STATE_UDP            = 0x22, // UDP socket.
    WIZ_SOCKET_STATE_IPRAW          = 0x32, // IP raw mode socket.
    WIZ_SOCKET_STATE_MACRAW         = 0x42, // MAC raw mode socket.
    WIZ_SOCKET_STATE_PPPOE          = 0x5F, // PPPoE socket.
    WIZ_SOCKET_STATE_ARP_TCP        = 0x11, // ARP socket.
    WIZ_SOCKET_STATE_ARP_UDP        = 0x21, // ARP socket.
    WIZ_SOCKET_STATE_ARP_ICMP       = 0x31, // ARP socket.
}
WIZ_SocketStateTypeDef;

// Socket flags.
typedef enum
{
    WIZ_SOCKET_FLAG_NONE        = 0x00,
    
    WIZ_SOCKET_FLAG_SEND_OK     = 0x10, // Complete sending.
    WIZ_SOCKET_FLAG_TIMEOUT     = 0x08, // Assert timeout.
    WIZ_SOCKET_FLAG_RECV        = 0x04, // Receiving data.
    WIZ_SOCKET_FLAG_DISCON      = 0x02, // Closed socket.
    WIZ_SOCKET_FLAG_CON         = 0x01, // Established connection.
    
    WIZ_SOCKET_FLAG_ALL         = 0xFF,
}
WIZ_SocketFlagTypeDef;


// Chip socket info.
typedef struct
{
    WIZ_MemorySizeTypeDef   TxSize;
    
    WIZ_MemorySizeTypeDef   RxSize;
    
    bool                    IsBusy;
    
    NET_PortModeTypeDef     PortMode;
    
    NET_PortTypeDef         Port;
}
WIZ_SocketInfoTypeDef;

// HAL init params.
typedef struct
{
    NET_ErrorTypeDef    (*WIZ_HAL_TransmitReceiveSync)  (uint8_t *pTxRxData, uint16_t length);
    
    NET_ErrorTypeDef    (*WIZ_HAL_TakeMutex)            (void);
    NET_ErrorTypeDef    (*WIZ_HAL_ReleaseMutex)         (void);
    
    void                (*WIZ_HAL_ActivateNss)          (void);
    void                (*WIZ_HAL_DeactivateNss)        (void);    
    
    void                (*WIZ_HAL_RunChip)              (void);
    void                (*WIZ_HAL_ResetChip)            (void);
    
    bool                (*WIZ_HAL_IsInt)                (void);
    void                (*WIZ_HAL_Yield)                (void);
}
WIZ_HalTypeDef;

// Pointer on chip API struct.
typedef struct WIZ_ChipApiTypeDef WIZ_ChipApiTypeDef;

// WIZnet chip handle.
typedef struct
{
    WIZ_HalTypeDef      Hal;
    
    WIZ_ChipApiTypeDef  *ChipApi;
    
    void                *ChipInfo;
    
    bool                IsInitialized;
    
    bool                __SocketPoolLocker;
}
WIZ_HandleTypeDef;

// Socket handle. This parameters can be set ONLY in TakeSocket and ReleaseSocket functions.
typedef struct
{
    WIZ_HandleTypeDef       *WizHandle;
    
    WIZ_SocketNumberTypeDef Number;
    
    WIZ_MemorySizeTypeDef   TxSize;
    
    WIZ_MemorySizeTypeDef   RxSize;
    
    bool                    IsOwner; // Is this handle is owner of socket with specified number.
}
WIZ_SocketHandleTypeDef;

// Chip API pointers and fields.
struct WIZ_ChipApiTypeDef
{
    NET_ErrorTypeDef    (*WIZ_CHIP_API_Init)                (WIZ_HandleTypeDef *hWiz);
    NET_ErrorTypeDef    (*WIZ_CHIP_API_DeInit)              (WIZ_HandleTypeDef *hWiz);

    NET_ErrorTypeDef    (*WIZ_CHIP_API_SetMemorySizes)      (WIZ_HandleTypeDef *hWiz, WIZ_MemorySizeTypeDef *pTxSizes, WIZ_MemorySizeTypeDef *pRxSizes);
    NET_ErrorTypeDef    (*WIZ_CHIP_API_SetMacAddress)       (WIZ_HandleTypeDef *hWiz, uint8_t *pMacAddress);
    NET_ErrorTypeDef    (*WIZ_CHIP_API_SetIPAddress)        (WIZ_HandleTypeDef *hWiz, NET_IPAddressTypeDef ipAddress);
    NET_ErrorTypeDef    (*WIZ_CHIP_API_SetSubnetMask)       (WIZ_HandleTypeDef *hWiz, NET_IPAddressTypeDef subnetMask);
    NET_ErrorTypeDef    (*WIZ_CHIP_API_SetDefaultGateway)   (WIZ_HandleTypeDef *hWiz, NET_IPAddressTypeDef defaultGateway);

    NET_ErrorTypeDef    (*WIZ_CHIP_API_SendLength)          (WIZ_SocketHandleTypeDef *hSocket, uint16_t *pLength);
    NET_ErrorTypeDef    (*WIZ_CHIP_API_SendData)            (WIZ_SocketHandleTypeDef *hSocket, uint8_t *pBuffer, uint16_t sendLength);
    NET_ErrorTypeDef    (*WIZ_CHIP_API_ReceiveLength)       (WIZ_SocketHandleTypeDef *hSocket, uint16_t *pLength);
    NET_ErrorTypeDef    (*WIZ_CHIP_API_ReceiveData)         (WIZ_SocketHandleTypeDef *hSocket, uint8_t *pBuffer, uint16_t bufferLength, uint16_t *pReceivedLength);
    NET_ErrorTypeDef    (*WIZ_CHIP_API_CloseSocket)         (WIZ_SocketHandleTypeDef *hSocket);

    NET_ErrorTypeDef    (*WIZ_CHIP_API_TCPOpen)             (WIZ_SocketHandleTypeDef *hSocket, NET_PortTypeDef port);
    NET_ErrorTypeDef    (*WIZ_CHIP_API_TCPListen)           (WIZ_SocketHandleTypeDef *hSocket);
    NET_ErrorTypeDef    (*WIZ_CHIP_API_TCPConnect)          (WIZ_SocketHandleTypeDef *hSocket, NET_EndPointTypeDef *pDestination);
    NET_ErrorTypeDef    (*WIZ_CHIP_API_TCPDisconnect)       (WIZ_SocketHandleTypeDef *hSocket);

    NET_ErrorTypeDef    (*WIZ_CHIP_API_UDPOpen)             (WIZ_SocketHandleTypeDef *hSocket, NET_PortTypeDef port);
    NET_ErrorTypeDef    (*WIZ_CHIP_API_UDPSendData)         (WIZ_SocketHandleTypeDef *hSocket, uint8_t *pBuffer, uint16_t length, NET_EndPointTypeDef *pDestination);

    NET_ErrorTypeDef    (*WIZ_CHIP_API_GetSocketState)      (WIZ_SocketHandleTypeDef *hSocket, WIZ_SocketStateTypeDef *state);
    NET_ErrorTypeDef    (*WIZ_CHIP_API_GetSocketFlags)      (WIZ_SocketHandleTypeDef *hSocket, WIZ_SocketFlagTypeDef *pFlags, WIZ_SocketFlagTypeDef flagsMask);

    NET_ErrorTypeDef    (*WIZ_CHIP_API_GetFlags)            (WIZ_HandleTypeDef *hWiz, WIZ_FlagTypeDef *pFlags, WIZ_FlagTypeDef flagsMask);

    NET_ErrorTypeDef    (*WIZ_CHIP_API_GetDestination)      (WIZ_SocketHandleTypeDef *hSocket, NET_EndPointTypeDef *pDestination);
    
    WIZ_SocketInfoTypeDef * (*WIZ_CHIP_API_GetSocketInfo)   (WIZ_HandleTypeDef *hWiz);
    
    uint16_t            SocketCount;
    
    WIZ_ChipTypeDef     ChipType;
    
    bool                __IsFilled;
};


NET_ErrorTypeDef WIZ_Init               (WIZ_HandleTypeDef *hWiz);
NET_ErrorTypeDef WIZ_DeInit             (WIZ_HandleTypeDef *hWiz);

NET_ErrorTypeDef WIZ_SetMemorySizes     (WIZ_HandleTypeDef *hWiz, WIZ_MemorySizeTypeDef *pTxSizes, WIZ_MemorySizeTypeDef *pRxSizes);
NET_ErrorTypeDef WIZ_SetMacAddress      (WIZ_HandleTypeDef *hWiz, uint8_t *pMacAddress);
NET_ErrorTypeDef WIZ_SetIPAddress       (WIZ_HandleTypeDef *hWiz, NET_IPAddressTypeDef ipAddress);
NET_ErrorTypeDef WIZ_SetSubnetMask      (WIZ_HandleTypeDef *hWiz, NET_IPAddressTypeDef subnetMask);
NET_ErrorTypeDef WIZ_SetDefaultGateway  (WIZ_HandleTypeDef *hWiz, NET_IPAddressTypeDef defaultGateway);

NET_ErrorTypeDef WIZ_TakeSocket         (WIZ_SocketHandleTypeDef *hSocket);
NET_ErrorTypeDef WIZ_ReleaseSocket      (WIZ_SocketHandleTypeDef *hSocket);

void WIZ_RememberPort                   (WIZ_SocketHandleTypeDef *hSocket, NET_PortTypeDef port, NET_PortModeTypeDef portMode);
bool WIZ_IsPortBusy                     (WIZ_HandleTypeDef *hWiz, NET_PortTypeDef port, NET_PortModeTypeDef portMode);

NET_ErrorTypeDef WIZ_GetFlags           (WIZ_HandleTypeDef *hWiz, WIZ_FlagTypeDef *pFlags, WIZ_FlagTypeDef flagsMask);
NET_ErrorTypeDef WIZ_AddFlagUsingSocket (WIZ_SocketHandleTypeDef *hSocket, WIZ_FlagTypeDef *pFlags);
NET_ErrorTypeDef WIZ_ApplyMaskForSocket (WIZ_SocketHandleTypeDef *hSocket, WIZ_FlagTypeDef *pFlags);


#endif /* __WIZCHIP_H */
