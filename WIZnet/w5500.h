#ifndef __W5500_H
#define __W5500_H


#include "wizchip.h"


#define W5500_SOCKET_COUNT 8


void W5500_GetChipApi(WIZ_ChipApiTypeDef **hApi);


#endif /* __W5500_H */
