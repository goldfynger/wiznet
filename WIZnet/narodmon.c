#include "narodmon.h"


#define NM_DEVICE_MAC_LEN       ((uint8_t)(2 * 6 + 5))  // MAC address length.
#define NM_DEVICE_POS_MAX_LEN   ((uint8_t)8)            // Up to 3 integer, dot and up to 4 fractional (123.4567).
#define NM_DEVICE_ELE_MAX_LEN   ((uint8_t)8)            // Up to 5 integer, dot and up to 2 fractional (12345.67).

#define NM_SENSOR_VAL_MAX_LEN   ((uint8_t)12)           // Up to 9 integer, dot and up to 2 fractional (123456789.01).
#define NM_SENSOR_TIME_MAX_LEN  ((uint8_t)10)           // Max length of Unix time.

#define NM_DEV_TCP_MSG_MAX_LEN  ((uint16_t)((1 + NM_DEVICE_MAC_LEN) + (1 + NM_DEVICE_NAME_MAX_LEN) + ((1 + NM_DEVICE_POS_MAX_LEN) * 2) + (1 + NM_DEVICE_ELE_MAX_LEN) + 1))  // #MAC[#NAME][#LAT][#LNG][#ELE]\n
#define NM_SENS_TCP_MSG_MAX_LEN ((uint16_t)((1 + NM_SENSOR_MAC_MAX_LEN) + (1 + NM_SENSOR_VAL_MAX_LEN) + (1 + NM_SENSOR_TIME_MAX_LEN) + (1 + NM_SENSOR_NAME_MAX_LEN) + 1))   // #mac#value[#time][#name]\n
#define NM_LAST_STR_TCP_LEN     ((uint16_t)2)                                                                                                                               // ##

#define NM_QUEUE_CHECK_DELAY    ((TickType_t)10)

#define NM_TCP_PORT             ((uint16_t)8283)        // Remote port for TCP messages.


const char NM_HostName[] = "narodmon.ru";


static NET_ErrorTypeDef NM_AllocTcpMessage(NM_HandleTypeDef *hNm);
static NET_ErrorTypeDef NM_SendTcpMessage(NM_HandleTypeDef *hNm);


// Allocate narodmon service handle.
NM_HandleTypeDef * NM_AllocateHandle(NET_HandleTypeDef *hNet, DNS_HandleTypeDef *hDns, uint8_t msgQueueLen)
{
    if (hNet == NULL || hDns == NULL) return NULL;
    
    
    size_t handleSize = sizeof(NM_HandleTypeDef);
    
    NM_HandleTypeDef *hNm = (NM_HandleTypeDef *)pvPortMalloc(handleSize);
    
    if (hNm == NULL) return NULL;
    
    
    hNm->NetHandle = hNet;
    hNm->DnsHandle = hDns;
    
    memset(hNm->Name, 0, NM_DEVICE_NAME_MAX_LEN + 1);
    
    hNm->Latitude = NAN;
    hNm->Longitude = NAN;
    hNm->Elevation = NAN;
    
    hNm->Interval = NM_MIN_SENDING_INTERVAL;
    
    
    hNm->MutexHandle = xSemaphoreCreateRecursiveMutex();
    
    if (hNm->MutexHandle == NULL)
    {
        NM_FreeHandle(hNm);
    }
    
    
    hNm->QueueHandle = xQueueCreate(msgQueueLen, sizeof(NM_SensorValueTypeDef));
    
    if (hNm->QueueHandle == NULL)
    {
        NM_FreeHandle(hNm);
    }
    
    
    hNm->RequestBuffer = NULL;
    hNm->ResponseBuffer = NULL;
    
    
    return hNm;
}

// Free narodmon service handle.
void NM_FreeHandle(NM_HandleTypeDef *hNm)
{
    if (hNm == NULL) return;
    
    if (hNm->MutexHandle != NULL)
    {
        vSemaphoreDelete(hNm->MutexHandle);
    }
    
    if (hNm->QueueHandle != NULL)
    {
        vQueueDelete(hNm->QueueHandle);
    }
    
    vPortFree(hNm->RequestBuffer);
    vPortFree(hNm->ResponseBuffer);
    vPortFree(hNm);
}

// Allocate narodmon sensor handle.
NM_SensorHandleTypeDef * NM_AllocateSensorHandle(NM_HandleTypeDef *hNm, char *pMac, uint8_t len)
{
    if (hNm == NULL || pMac == NULL || pMac[0] == '\0' || len > NM_SENSOR_MAC_MAX_LEN) return NULL;
    
    
    size_t handleSize = sizeof(NM_SensorHandleTypeDef);
    
    NM_SensorHandleTypeDef *hSensor = (NM_SensorHandleTypeDef *)pvPortMalloc(handleSize);
    
    if (hSensor == NULL) return NULL;
    
    
    hSensor->NmHandle = hNm;
    
    memset(hSensor->Mac, 0, NM_SENSOR_MAC_MAX_LEN + 1);
    memcpy(hSensor->Mac, pMac, len);
    
    memset(hSensor->Name, 0, NM_SENSOR_NAME_MAX_LEN + 1);
    
    
    return hSensor;
}

// Free narodmon sensor handle.
void NM_FreeSensorHandle(NM_SensorHandleTypeDef *hSensor)
{
    if (hSensor == NULL) return;
    
    vPortFree(hSensor);
}

// Sets the device name.
NET_ErrorTypeDef NM_SetName(NM_HandleTypeDef *hNm, char *pName, uint8_t len)
{
    if (hNm == NULL || hNm->MutexHandle == NULL || pName == NULL) return NM_ERR_NULLPTR;
    
    if (len > NM_DEVICE_NAME_MAX_LEN) return NM_ERR_WRONG_STRING_LENGTH;
    
    
    if (xSemaphoreTakeRecursive(hNm->MutexHandle, portMAX_DELAY) != pdTRUE) return NM_ERR_RTOS_FAILURE;
    
    memset(hNm->Name, 0, NM_DEVICE_NAME_MAX_LEN + 1);
    memcpy(hNm->Name, pName, len);
    
    if (xSemaphoreGiveRecursive(hNm->MutexHandle) != pdTRUE) return NM_ERR_RTOS_FAILURE;
    
    
    return NM_ERR_OK;
}

// Sets the device position.
NET_ErrorTypeDef NM_SetPosition(NM_HandleTypeDef *hNm, float lat, float lng, float ele)
{
    if (hNm == NULL || hNm->MutexHandle == NULL) return NM_ERR_NULLPTR;
    
    
    if (xSemaphoreTakeRecursive(hNm->MutexHandle, portMAX_DELAY) != pdTRUE) return NM_ERR_RTOS_FAILURE;
    
    hNm->Latitude = lat;
    hNm->Longitude = lng;
    hNm->Elevation = ele;

    if (xSemaphoreGiveRecursive(hNm->MutexHandle) != pdTRUE) return NM_ERR_RTOS_FAILURE;
    
    
    return NM_ERR_OK;
}

// Sets the sending interval.
NET_ErrorTypeDef NM_SetInterval(NM_HandleTypeDef *hNm, time_t interval)
{
    if (hNm == NULL || hNm->MutexHandle == NULL) return NM_ERR_NULLPTR;
    
    if (interval > NM_MIN_SENDING_INTERVAL) return NM_ERR_WRONG_SENDING_INTERVAL;
    
    
    if (xSemaphoreTakeRecursive(hNm->MutexHandle, portMAX_DELAY) != pdTRUE) return NM_ERR_RTOS_FAILURE;   

    hNm->Interval = interval;
    
    if (xSemaphoreGiveRecursive(hNm->MutexHandle) != pdTRUE) return NM_ERR_RTOS_FAILURE;
    
    
    return NM_ERR_OK;
}

// Sets the sensor identifier.
NET_ErrorTypeDef NM_SetSensorMac(NM_SensorHandleTypeDef *hSensor, char *pMac, uint8_t len)
{
    if (hSensor == NULL || hSensor->NmHandle == NULL || hSensor->NmHandle->MutexHandle == NULL || pMac == NULL) return NM_ERR_NULLPTR;
    
    if (pMac[0] == '\0' || len > NM_SENSOR_MAC_MAX_LEN) return NM_ERR_WRONG_STRING_LENGTH;
    
    
    if (xSemaphoreTakeRecursive(hSensor->NmHandle->MutexHandle, portMAX_DELAY) != pdTRUE) return NM_ERR_RTOS_FAILURE;   

    memset(hSensor->Mac, 0, NM_SENSOR_MAC_MAX_LEN + 1);
    memcpy(hSensor->Mac, pMac, len);
    
    if (xSemaphoreGiveRecursive(hSensor->NmHandle->MutexHandle) != pdTRUE) return NM_ERR_RTOS_FAILURE;
    
    
    return NM_ERR_OK;
}

// Sets the sensor name.
NET_ErrorTypeDef NM_SetSensorName(NM_SensorHandleTypeDef *hSensor, char *pName, uint8_t len)
{
    if (hSensor == NULL || hSensor->NmHandle == NULL || hSensor->NmHandle->MutexHandle == NULL || pName == NULL) return NM_ERR_NULLPTR;
    
    if (len > NM_SENSOR_NAME_MAX_LEN) return NM_ERR_WRONG_STRING_LENGTH;
    
    
    if (xSemaphoreTakeRecursive(hSensor->NmHandle->MutexHandle, portMAX_DELAY) != pdTRUE) return NM_ERR_RTOS_FAILURE;   

    memset(hSensor->Name, 0, NM_SENSOR_NAME_MAX_LEN + 1);
    memcpy(hSensor->Name, pName, len);
    
    if (xSemaphoreGiveRecursive(hSensor->NmHandle->MutexHandle) != pdTRUE) return NM_ERR_RTOS_FAILURE;
    
    
    return NM_ERR_OK;
}

// Adds new value for specified sensor.
NET_ErrorTypeDef NM_AddSensorValue(NM_SensorHandleTypeDef *hSensor, float value, time_t time)
{
    if (hSensor == NULL || hSensor->NmHandle == NULL || hSensor->NmHandle->QueueHandle == NULL) return NM_ERR_NULLPTR;
    
    
    NM_SensorValueTypeDef sensorVal = { .SensorHandle = hSensor, .Value = value, .Time = time };
    
    if (xQueueSendToBack(hSensor->NmHandle->QueueHandle, (void *)&sensorVal, (TickType_t)0) != pdTRUE) return NM_ERR_MSG_QUEUE_FULL;
    
    
    #ifdef NM_DEBUG
       
    net_os_printf("NM: new val of %s: %.2f:%u\r\n", hSensor->Mac, value, time);
    
    #endif
    
    
    return NM_ERR_OK;
}

// Process narodmon.
NET_ErrorTypeDef NM_Run(NM_HandleTypeDef *hNm)
{
    NET_ErrorTypeDef error = NET_ERR_OK;
    
    
    if (hNm == NULL || hNm->NetHandle == NULL || hNm->DnsHandle == NULL || hNm->MutexHandle == NULL || hNm->QueueHandle == NULL) return NM_ERR_NULLPTR;
    
    
    while (true)
    {
        #ifdef NM_DEBUG
        
        net_os_printf("NM: loop begin free:%u\r\n", xPortGetFreeHeapSize());
        
        #endif
               
        
        if ((error = NM_AllocTcpMessage(hNm)) != NM_ERR_OK)
        {
            vPortFree(hNm->RequestBuffer);
            
            return error;
        }
        
        if ((error = NM_SendTcpMessage(hNm)) != NM_ERR_OK)
        {
            vPortFree(hNm->RequestBuffer);
            vPortFree(hNm->ResponseBuffer);
            
            return error;
        }        
        
        vPortFree(hNm->RequestBuffer);
        vPortFree(hNm->ResponseBuffer);
        
        hNm->RequestBuffer = NULL;
        hNm->ResponseBuffer = NULL;
        
        
        #ifdef NM_DEBUG
        
        net_os_printf("NM: loop end free:%u\r\n", xPortGetFreeHeapSize());
        
        #endif
        
        
        vTaskDelay(hNm->Interval * configTICK_RATE_HZ);
    }
}

// Allocates and fills string with TCP-format request.
static NET_ErrorTypeDef NM_AllocTcpMessage(NM_HandleTypeDef *hNm)
{
    // Wait for messages in queue.
    uint8_t msgsInQueue = 0;        
    
    do
    {
        msgsInQueue = uxQueueMessagesWaiting(hNm->QueueHandle);
        
        if (!msgsInQueue) vTaskDelay(NM_QUEUE_CHECK_DELAY);
    }
    while (!msgsInQueue);
    
    
    // Computes and allocates memory buffer for TCP message.
    size_t buffLen = (NM_DEV_TCP_MSG_MAX_LEN + (NM_SENS_TCP_MSG_MAX_LEN * msgsInQueue) + NM_LAST_STR_TCP_LEN + 1) * sizeof(char);
    
    hNm->RequestBuffer = (char *)pvPortMalloc(buffLen);
    
    if (hNm->RequestBuffer == NULL) return NM_ERR_MEM_ALLOCATION;
    
    memset(hNm->RequestBuffer, 0, buffLen);
    
        
    uint8_t *pMacAddr = NET_GetMacAddress(hNm->NetHandle);
    
    
    // Locks device info.
    if (xSemaphoreTakeRecursive(hNm->MutexHandle, portMAX_DELAY) != pdTRUE) return NM_ERR_RTOS_FAILURE;
    
    
    uint16_t strIdx = 0;
    uint8_t substrLen = 0;
    
    
    // Adds device info to message.
    strIdx = sprintf(hNm->RequestBuffer + strIdx, "#%02X-%02X-%02X-%02X-%02X-%02X", pMacAddr[5], pMacAddr[4], pMacAddr[3], pMacAddr[2], pMacAddr[1], pMacAddr[0]);
    
    if (hNm->Name[0] != '\0')
    {
        hNm->RequestBuffer[strIdx++] = '#';
        substrLen = strlen(hNm->Name);
        memcpy(hNm->RequestBuffer + strIdx, hNm->Name, substrLen);
        strIdx += substrLen;
        
        if (isnormal(hNm->Latitude) && isnormal(hNm->Longitude))
        {        
            strIdx += sprintf(hNm->RequestBuffer + strIdx, "#%.4f#%.4f", hNm->Latitude, hNm->Longitude);
            
            if (isnormal(hNm->Elevation))
            {
                strIdx += sprintf(hNm->RequestBuffer + strIdx, "#%.2f", hNm->Elevation);
            }
        }
    }    
    
    hNm->RequestBuffer[strIdx++] = '\n';
    
    
    // Adds sensors info to message.
    while (msgsInQueue--)
    {
        NM_SensorValueTypeDef sensorVal;
        memset(&sensorVal, 0, sizeof(NM_SensorValueTypeDef));
        
        if (xQueueReceive(hNm->QueueHandle, (void *)&sensorVal, (TickType_t)0) != pdTRUE)
        {            
            xSemaphoreGiveRecursive(hNm->MutexHandle);            
            
            return NM_ERR_RTOS_FAILURE;
        }
        
        
        hNm->RequestBuffer[strIdx++] = '#';
        substrLen = strlen(sensorVal.SensorHandle->Mac);
        memcpy(hNm->RequestBuffer + strIdx, sensorVal.SensorHandle->Mac, substrLen);
        strIdx += substrLen;
        
        strIdx += sprintf(hNm->RequestBuffer + strIdx, "#%.2f", sensorVal.Value);
        
        if (sensorVal.Time != 0)
        {
            strIdx += sprintf(hNm->RequestBuffer + strIdx, "#%u", sensorVal.Time);
        }
        
        if (sensorVal.SensorHandle->Name[0] != '\0')
        {
            hNm->RequestBuffer[strIdx++] = '#';
            substrLen = strlen(sensorVal.SensorHandle->Name);
            memcpy(hNm->RequestBuffer + strIdx, sensorVal.SensorHandle->Name, substrLen);
            strIdx += substrLen;
        }
        
        hNm->RequestBuffer[strIdx++] = '\n';
    }
    
    
    hNm->RequestBuffer[strIdx++] = '#';
    hNm->RequestBuffer[strIdx++] = '#';
    
    
    // Unlocks device info.
    if (xSemaphoreGiveRecursive(hNm->MutexHandle) != pdTRUE) return NM_ERR_RTOS_FAILURE;
    
    
    #ifdef NM_DEBUG
    
    net_os_printf("NM: buff alloc info: bufflen:%u strlen:%u\r\n", buffLen, strIdx);
    
    #endif
    
    #ifdef NM_DEBUG_DATA
    
    net_os_puts(hNm->RequestBuffer);
    
    #endif
    
    
    return NM_ERR_OK;
}

// Sends TCP message and processes response.
static NET_ErrorTypeDef NM_SendTcpMessage(NM_HandleTypeDef *hNm)
{
    NET_ErrorTypeDef error = NET_ERR_OK;
        
    
    TCP_SocketHandleTypeDef tcpSocket = { .LocalPort.Value = NM_TCP_PORT, .RemotePoint.IPAddress.Value = 0, .RemotePoint.Port.Value = NM_TCP_PORT };
    
    if ((error = DNS_Resolve(hNm->DnsHandle, NM_HostName, &tcpSocket.RemotePoint.IPAddress)) != DNS_ERR_OK)
    {
        error |= NM_ERR_LOW_LEVEL;
        
        return error;
    }
    
    
    if ((error = TCP_ConnectSocket(&tcpSocket, hNm->NetHandle)) != TCP_ERR_OK)
    {
        error |= NM_ERR_LOW_LEVEL;
        
        return error;
    }
    
    
    TCP_FlagTypeDef tcpFlags = TCP_FLAG_NONE;    
    uint16_t timeout = NM_POLL_TIMEOUT;
    
    if ((error = TCP_PollForFlags(&tcpSocket, &tcpFlags, TCP_FLAG_CONNECTED, &timeout)) != TCP_ERR_OK)
    {
        error |= NM_ERR_LOW_LEVEL;
        
        return error;
    }
    
    
    if ((error = TCP_SendData(&tcpSocket, (uint8_t *)hNm->RequestBuffer, strlen(hNm->RequestBuffer))) != TCP_ERR_OK)
    {
        error |= NM_ERR_LOW_LEVEL;
        
        return error;
    }
    
    
    tcpFlags = TCP_FLAG_NONE;    
    timeout = NM_POLL_TIMEOUT;
    
    if ((error = TCP_PollForFlags(&tcpSocket, &tcpFlags, TCP_FLAG_SENT, &timeout)) != TCP_ERR_OK)
    {
        error |= NM_ERR_LOW_LEVEL;
        
        return error;
    }
    
    
    tcpFlags = TCP_FLAG_NONE;    
    timeout = NM_POLL_TIMEOUT;
    
    if ((error = TCP_PollForFlags(&tcpSocket, &tcpFlags, TCP_FLAG_RECEIVED, &timeout)) != TCP_ERR_OK)
    {
        error |= NM_ERR_LOW_LEVEL;
        
        return error;
    }

    
    uint16_t length = 0;
    
    if ((error = TCP_ReceiveLength(&tcpSocket, &length)) != TCP_ERR_OK)
    {
        error |= NM_ERR_LOW_LEVEL;
        
        return error;
    }    
    
    
    hNm->ResponseBuffer = pvPortMalloc(length);
    
    if (hNm->ResponseBuffer == NULL) return NM_ERR_MEM_ALLOCATION;
    
    
    uint16_t receivedLen = 0;
    
    if ((error = TCP_ReceiveData(&tcpSocket, (uint8_t *)hNm->ResponseBuffer, length, &receivedLen)) != TCP_ERR_OK)
    {
        error |= NM_ERR_LOW_LEVEL;
        
        return error;
    }
    
    
    #ifdef NM_DEBUG_DATA
    
    net_os_puts(hNm->ResponseBuffer);
    
    #endif
    
    
    if ((error = TCP_CloseSocket(&tcpSocket)) != TCP_ERR_OK)
    {
        error |= NM_ERR_LOW_LEVEL;
        
        return error;
    }
    
    
    return NM_ERR_OK;
}
