#ifndef __DHCP_H
#define __DHCP_H


#include "net.h"


#define DHCP_TRY_COUNT              4

#define DHCP_POLL_TIMEOUT           5000


// https://www.ietf.org/rfc/rfc2131.txt Page 34
typedef enum
{
    DHCP_STATE_INIT                         = 0, // Initial state.
    DHCP_STATE_SELECTING                    = 1, // Send DISCOVER broadcast, collecting OFFER replies.
    DHCP_STATE_REQUESTING                   = 2, // Send REQUEST broadcast, wait ACK or NACK, check collisions.
    DHCP_STATE_BOUND                        = 3, // Bounded.
    DHCP_STATE_RENEWING                     = 4, // Send REQUEST directly, wait ACK or NACK.
    DHCP_STATE_REBINDING                    = 5, // Send REUQEST broadcast, wait ACK or NACK.
    DHCP_STATE_INIT_REBOOT                  = 6, // Initial state after reboot.
    DHCP_STATE_REBOOTING                    = 7, // Send REUQEST broadcast, wait ACK or NACK.
}
DHCP_StateTypeDef;


typedef struct
{
    NET_HandleTypeDef       *NetHandle;
    
    uint32_t                TransactionId;
    
    NET_IPAddressTypeDef    TempServerAddress;
    
    NET_IPAddressTypeDef    TempClientAddress;
    
    uint16_t                Timeout;
    
    DHCP_StateTypeDef       State;
    
    uint8_t                 TryCounter;
}
DHCP_HandleTypeDef;


NET_ErrorTypeDef DHCP_Run(DHCP_HandleTypeDef *hDhcp);


#endif /* __DHCP_H */
