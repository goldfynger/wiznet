#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "net.h"
#include "net_os.h"
#include "udp.h"
#include "wizchip.h"

#include "dhcp.h"


#define DHCP_SERVER_PORT                67
#define DHCP_CLIENT_PORT                68

#define DHCP_HADDRESS_LENGTH            6
#define DHCP_CLIENT_HOPS                0
#define DHCP_CLIENT_SECONDS             0

#define DHCP_FLAGSBROADCAST             0x8000
#define DHCP_FLAGSUNICAST               0x0000

#define DHCP_MAGIC_COOKIE               0x63825363
#define DHCP_MAGIC_COOKIE_LENGTH        4

#define DHCP_OPTIONS_MAX_SIZE           312

#define DHCP_ADDRESS_ZERO               ((uint32_t)0x00000000)
#define DHCP_ADDRESS_BROADCAST          ((uint32_t)0xFFFFFFFF)


/*
 --------                               -------
|        | +-------------------------->|       |<-------------------+
| INIT-  | |     +-------------------->| INIT  |                    |
| REBOOT |DHCPNAK/         +---------->|       |<---+               |
|        |Restart|         |            -------     |               |
 --------  |  DHCPNAK/     |               |                        |
    |      Discard offer   |      -/Send DHCPDISCOVER               |
-/Send DHCPREQUEST         |               |                        |
    |      |     |      DHCPACK            v        |               |
 -----------     |   (not accept.)/   -----------   |               |
|           |    |  Send DHCPDECLINE |           |                  |
| REBOOTING |    |         |         | SELECTING |<----+            |
|           |    |        /          |           |     |DHCPOFFER/  |
 -----------     |       /            -----------   |  |Collect     |
    |            |      /                  |   |       |  replies   |
DHCPACK/         |     /  +----------------+   +-------+            |
Record lease, set|    |   v   Select offer/                         |
timers T1, T2   ------------  send DHCPREQUEST      |               |
    |   +----->|            |             DHCPNAK, Lease expired/   |
    |   |      | REQUESTING |                  Halt network         |
    DHCPOFFER/ |            |                       |               |
    Discard     ------------                        |               |
    |   |        |        |                   -----------           |
    |   +--------+     DHCPACK/              |           |          |
    |              Record lease, set    -----| REBINDING |          |
    |                timers T1, T2     /     |           |          |
    |                     |        DHCPACK/   -----------           |
    |                     v     Record lease, set   ^               |
    +----------------> -------      /timers T1,T2   |               |
               +----->|       |<---+                |               |
               |      | BOUND |<---+                |               |
  DHCPOFFER, DHCPACK, |       |    |            T2 expires/   DHCPNAK/
   DHCPNAK/Discard     -------     |             Broadcast  Halt network
               |       | |         |            DHCPREQUEST         |
               +-------+ |        DHCPACK/          |               |
                    T1 expires/   Record lease, set |               |
                 Send DHCPREQUEST timers T1, T2     |               |
                 to leasing server |                |               |
                         |   ----------             |               |
                         |  |          |------------+               |
                         +->| RENEWING |                            |
                            |          |----------------------------+
                             ----------
*/

typedef enum
{
    DHCP_OPCODE_BOOTREQUEST                 = 1,
    DHCP_OPCODE_BOOTREPLY                   = 2,
}
DHCP_OPCodeTypeDef;

typedef enum
{
    DHCP_TYPE_NONE                          = 0,
    DHCP_TYPE_DISCOVER                      = 1,
    DHCP_TYPE_OFFER                         = 2,
    DHCP_TYPE_REQUEST                       = 3,
    DHCP_TYPE_DECLINE                       = 4,
    DHCP_TYPE_ACK                           = 5,
    DHCP_TYPE_NAK                           = 6,
    DHCP_TYPE_RELEASE                       = 7,
    DHCP_TYPE_INFORM                        = 8,
}
DHCP_TypeTypeDef;

typedef enum
{
    DHCP_HTYPE_10MB                         = 1,
    DHCP_HTYPE_100MB                        = 2,
}
DHCP_HAddressTypeDef;

// https://www.ietf.org/rfc/rfc2132.txt
typedef enum
{
    DHCP_OPTION_PAD                         = 0,
    DHCP_OPTION_END                         = 255,    
    DHCP_OPTION_SUBNETMASK                  = 1,
    DHCP_OPTION_TIMEOFFSET                  = 2,
    DHCP_OPTION_ROUTER                      = 3,
    DHCP_OPTION_TIMESERVER                  = 4,
    DHCP_OPTION_NAMESERVER                  = 5,
    DHCP_OPTION_DOMAINNAMESERVER            = 6,
    DHCP_OPTION_LOGSERVER                   = 7,
    DHCP_OPTION_COOKIESERVER                = 8,
    DHCP_OPTION_LPRSERVER                   = 9,
    DHCP_OPTION_IMPRESSSERVER               = 10,
    DHCP_OPTION_RESOURCELOCATIONSERVER      = 11,
    DHCP_OPTION_HOSTNAME                    = 12,
    DHCP_OPTION_BOOTFILESIZE                = 13,
    DHCP_OPTION_MERITDUMPFILE               = 14,
    DHCP_OPTION_DOMAINNAME                  = 15,
    DHCP_OPTION_SWAPSERVER                  = 16,
    DHCP_OPTION_ROOTPATH                    = 17,
    DHCP_OPTION_EXTENSIONSPATH              = 18,
    
    DHCP_OPTION_IPFORWARDING                = 19,
    DHCP_OPTION_NONLOCALSOURCEROUTING       = 20,
    DHCP_OPTION_POLICYFILTER                = 21,
    DHCP_OPTION_MAXDATAGRAMREASSEMBLYSIZE   = 22,
    DHCP_OPTION_DEFAULTIPTTL                = 23,
    DHCP_OPTION_PATHMTUAGINGTIMEOUT         = 24,
    DHCP_OPTION_PATHMTUPLATEAUTABLE         = 25,
    
    DHCP_OPTION_INTERFACEMTU                = 26,
    DHCP_OPTION_ALLSUBNETSARELOCAL          = 27,
    DHCP_OPTION_BROADCASTADDRESS            = 28,
    DHCP_OPTION_PERFORMMASKDISCOVERY        = 29,
    DHCP_OPTION_MASKSUPPLIER                = 30,
    DHCP_OPTION_PERFORMROUTERDISCOVERY      = 31,
    DHCP_OPTION_ROUTERSOLICITATIONADDRESS   = 32,
    DHCP_OPTION_STATICROUTE                 = 33,
    
    DHCP_OPTION_TRAILERENCAPSULATION        = 34,
    DHCP_OPTION_ARPCACHETIMEOUT             = 35,
    DHCP_OPTION_ETHERNETENCAPSULATION       = 36,
    
    DHCP_OPTION_TCPDEFAULTTTL               = 37,
    DHCP_OPTION_TCPKEEPALIVEINTERVAL        = 38,
    DHCP_OPTION_TCPKEEPALIVEGARBADGE        = 39,
    
    DHCP_OPTION_NISDOMAIN                   = 40,
    DHCP_OPTION_NISSERVERS                  = 41,
    DHCP_OPTION_NTPSERVERS                  = 42,
    DHCP_OPTION_VENDORSPECIFICINFO          = 43,
    DHCP_OPTION_NETBIOSNAMESERVER           = 44,
    DHCP_OPTION_NETBIOSDATAGRAMDISTSERVER   = 45,
    DHCP_OPTION_NETBIOSNODETYPE             = 46,
    DHCP_OPTION_NETBIOSSCOPE                = 47,
    DHCP_OPTION_XWINDOWSYSTEMFONTSERVER     = 48,
    DHCP_OPTION_XWINDOWSYSTEMDISPLAYMANAGER = 49,    
    DHCP_OPTION_NISPLUSDOMAIN               = 64,
    DHCP_OPTION_NISPLUSSERVERS              = 65,
    DHCP_OPTION_MOBILEIPHOMEAGENT           = 68,
    DHCP_OPTION_SMTPSERVER                  = 69,
    DHCP_OPTION_POP3SERVER                  = 70,
    DHCP_OPTION_NNTPSERVER                  = 71,
    DHCP_OPTION_DEFAULTWWWSERVER            = 72,
    DHCP_OPTION_DEFAULTFINGERSERVER         = 73,
    DHCP_OPTION_DEFAULTIRCSERVER            = 74,
    DHCP_OPTION_STREETTALKSERVER            = 75,
    DHCP_OPTION_STDASERVER                  = 76,
    
    DHCP_OPTION_REQUESTEDIPADDRESS          = 50,
    DHCP_OPTION_IPADDRESSLEASETIME          = 51,
    DHCP_OPTION_OVERLOAD                    = 52,    
    DHCP_OPTION_TFTP                        = 66,
    DHCP_OPTION_BOOTFILE                    = 67,    
    DHCP_OPTION_MESSAGETYPE                 = 53,
    DHCP_OPTION_SERVERIDENTIFIER            = 54,
    DHCP_OPTION_PARAMETERREQUESTLIST        = 55,
    DHCP_OPTION_MESSAGE                     = 56,
    DHCP_OPTION_MAXIMUMMESSAGESIZE          = 57,
    DHCP_OPTION_RENEWALT1TIMEVALUE          = 58,
    DHCP_OPTION_REBINDINGT2TIMEVALUE        = 59,
    DHCP_OPTION_CLASSIDENTIFIER             = 60,
    DHCP_OPTION_CLIENTIDENTIFIER            = 61,
}
DHCP_OptionTypeDef;


// https://www.ietf.org/rfc/rfc2131.txt
typedef struct
{
	uint8_t     MessageOPCode;
	uint8_t     HAddressType;
	uint8_t     HAddressLength;
	uint8_t     Hops;    
	uint32_t    TransactionID;    
	uint16_t    Seconds;
	uint16_t    Flags;    
	uint8_t     ClientIPAddress[4];
	uint8_t     YourIPAddress[4];
	uint8_t     ServerIPAddress[4];
	uint8_t     RelayAgentIPAddress[4];
	uint8_t     ClientHAddress[16];
	uint8_t     ServerName[64];
	uint8_t     FileName[128];
	uint8_t     Options[];
}
DHCP_MessageTypeDef; // Big-endian.


static void DHCP_FillMessage                    (DHCP_HandleTypeDef *hDhcp, DHCP_MessageTypeDef *pMessage);

static NET_ErrorTypeDef DHCP_SendDISCOVER       (DHCP_HandleTypeDef *hDhcp, DHCP_MessageTypeDef *pMessage, UDP_SocketHandleTypeDef *hUdp);
static NET_ErrorTypeDef DHCP_SendREQUEST        (DHCP_HandleTypeDef *hDhcp, DHCP_MessageTypeDef *pMessage, UDP_SocketHandleTypeDef *hUdp);
static NET_ErrorTypeDef DHCP_SendDECLINE        (DHCP_HandleTypeDef *hDhcp, DHCP_MessageTypeDef *pMessage, UDP_SocketHandleTypeDef *hUdp);
static NET_ErrorTypeDef DHCP_SendRELEASE        (DHCP_HandleTypeDef *hDhcp, DHCP_MessageTypeDef *pMessage, UDP_SocketHandleTypeDef *hUdp);
static NET_ErrorTypeDef DHCP_SendINFORM         (DHCP_HandleTypeDef *hDhcp, DHCP_MessageTypeDef *pMessage, UDP_SocketHandleTypeDef *hUdp);

static void DHCP_AddOptionHOSTNAME              (DHCP_HandleTypeDef *hDhcp, DHCP_MessageTypeDef *pMessage, uint16_t *pOptionIdx);
static void DHCP_AddOptionREQUESTEDIPADDRESS    (DHCP_HandleTypeDef *hDhcp, DHCP_MessageTypeDef *pMessage, uint16_t *pOptionIdx);
static void DHCP_AddOptionMESSAGETYPE                                      (DHCP_MessageTypeDef *pMessage, uint16_t *pOptionIdx, DHCP_TypeTypeDef type);
static void DHCP_AddOptionSERVERIDENTIFIER      (DHCP_HandleTypeDef *hDhcp, DHCP_MessageTypeDef *pMessage, uint16_t *pOptionIdx);
static void DHCP_AddOptionPARAMETERREQUESTLIST                             (DHCP_MessageTypeDef *pMessage, uint16_t *pOptionIdx);
static void DHCP_AddOptionCLIENTIDENTIFIER      (DHCP_HandleTypeDef *hDhcp, DHCP_MessageTypeDef *pMessage, uint16_t *pOptionIdx);

static NET_ErrorTypeDef DHCP_ProcessResponse    (DHCP_HandleTypeDef *hDhcp, UDP_SocketHandleTypeDef *hUdp);
static NET_ErrorTypeDef DHCP_ParseResponse      (DHCP_HandleTypeDef *hDhcp, DHCP_MessageTypeDef *pAnswer, uint16_t answerLen);

static NET_ErrorTypeDef DHCP_AllocMsgIfNeed     (DHCP_MessageTypeDef **ppMessage);
static void             DHCP_FreeMsgIfNeed      (DHCP_MessageTypeDef **ppMessage);

static NET_ErrorTypeDef DHCP_OpenUDPIfNeed      (DHCP_HandleTypeDef *hDhcp, UDP_SocketHandleTypeDef *hUdp);
static NET_ErrorTypeDef DHCP_CloseUDPIfNeed                                (UDP_SocketHandleTypeDef *hUdp);
static NET_ErrorTypeDef DHCP_SendUDPMessage     (DHCP_HandleTypeDef *hDhcp, UDP_SocketHandleTypeDef *hUdp, DHCP_MessageTypeDef *pMessage, uint16_t messageLen);

static bool DHCP_IsNormalIPAddress              (NET_IPAddressTypeDef address);

static bool DHCP_IsMacEqualToInversed           (uint8_t *firstMac, uint8_t *secondMac);


NET_ErrorTypeDef DHCP_Run(DHCP_HandleTypeDef *hDhcp)
{
    NET_ErrorTypeDef error = DHCP_ERR_OK;
    
    
    DHCP_MessageTypeDef *pMessage = NULL;
    
    UDP_SocketHandleTypeDef udpSocket;
    
    
    while(true)
    {
        switch(hDhcp->State)
        {
            case DHCP_STATE_INIT:
            {                
                // Init handle params.
                hDhcp->TempClientAddress.Value = DHCP_ADDRESS_ZERO;
                hDhcp->TempServerAddress.Value = DHCP_ADDRESS_ZERO;
                hDhcp->TryCounter = DHCP_TRY_COUNT;
                hDhcp->TransactionId = 1234567890;
                hDhcp->Timeout = DHCP_POLL_TIMEOUT;
                
                // Alloc message buffer.
                if ((error = DHCP_AllocMsgIfNeed(&pMessage)) != DHCP_ERR_OK) return error;
                
                // Open UDP socket.
                if ((error = DHCP_OpenUDPIfNeed(hDhcp, &udpSocket)) != DHCP_ERR_OK)
                {
                    DHCP_FreeMsgIfNeed(&pMessage);
                    return error;
                }
                
                // Send DISCOVER message.
                if ((error = DHCP_SendDISCOVER(hDhcp, pMessage, &udpSocket)) != DHCP_ERR_OK)
                {
                    NET_ErrorTypeDef closeError = DHCP_CloseUDPIfNeed(&udpSocket);
                    if (closeError != DHCP_ERR_OK) error = closeError;
                    
                    DHCP_FreeMsgIfNeed(&pMessage);
                    return error;
                }
                
                // Next state.
                hDhcp->State = DHCP_STATE_SELECTING;
            }
            break;
            
            case DHCP_STATE_SELECTING:
            {
                // Process OFFER messages.
                while(true)
                {
                    // If response OK, return.
                    if ((error = DHCP_ProcessResponse(hDhcp, &udpSocket)) == DHCP_ERR_OK) break;
                    
                    if (error == DHCP_ERR_NO_ANSWER && hDhcp->TryCounter--)
                    {
                        // Previous state.
                        hDhcp->State = DHCP_STATE_INIT;
                        hDhcp->TransactionId++;
                        error = DHCP_ERR_OK;
                        
                        // Resend DISCOVER message.
                        if ((error = DHCP_SendDISCOVER(hDhcp, pMessage, &udpSocket)) != DHCP_ERR_OK)
                        {
                            NET_ErrorTypeDef closeError = DHCP_CloseUDPIfNeed(&udpSocket);
                            if (closeError != DHCP_ERR_OK) error = closeError;
                            
                            DHCP_FreeMsgIfNeed(&pMessage);
                            return error;
                        }
                        
                        // Next state.
                        hDhcp->State = DHCP_STATE_SELECTING;
                    }
                    else
                    {
                        NET_ErrorTypeDef closeError = DHCP_CloseUDPIfNeed(&udpSocket);
                        if (closeError != DHCP_ERR_OK) error = closeError;
                        
                        DHCP_FreeMsgIfNeed(&pMessage);
                        return error;
                    }
                }
                
                // Send REQUEST message.                
                if ((error = DHCP_SendREQUEST(hDhcp, pMessage, &udpSocket)) != DHCP_ERR_OK)
                {
                    NET_ErrorTypeDef closeError = DHCP_CloseUDPIfNeed(&udpSocket);
                    if (closeError != DHCP_ERR_OK) error = closeError;
                    
                    DHCP_FreeMsgIfNeed(&pMessage);
                    return error;
                }
                
                // Next state.
                hDhcp->State = DHCP_STATE_REQUESTING;
            }
            break;
            
            case DHCP_STATE_REQUESTING:
            {
                // Process OFFER messages.
                while(true)
                {
                    // If response OK, return.
                    if ((error = DHCP_ProcessResponse(hDhcp, &udpSocket)) == DHCP_ERR_OK) break;
                    
                    if (error == DHCP_ERR_NO_ANSWER && hDhcp->TryCounter--)
                    {
                        // Previous state.
                        hDhcp->State = DHCP_STATE_SELECTING;
                        hDhcp->TransactionId++;
                        error = DHCP_ERR_OK;
                        
                        // Resend DISCOVER message.
                        if ((error = DHCP_SendREQUEST(hDhcp, pMessage, &udpSocket)) != DHCP_ERR_OK)
                        {
                            NET_ErrorTypeDef closeError = DHCP_CloseUDPIfNeed(&udpSocket);
                            if (closeError != DHCP_ERR_OK) error = closeError;
                            
                            DHCP_FreeMsgIfNeed(&pMessage);
                            return error;
                        }
                        
                        // Next state.
                        hDhcp->State = DHCP_STATE_REQUESTING;
                    }
                    else
                    {
                        NET_ErrorTypeDef closeError = DHCP_CloseUDPIfNeed(&udpSocket);
                        if (closeError != DHCP_ERR_OK) error = closeError;
                        
                        DHCP_FreeMsgIfNeed(&pMessage);
                        return error;
                    }
                }
                
                // Close port.
                if ((error = DHCP_CloseUDPIfNeed(&udpSocket)) != DHCP_ERR_OK) return error;
                
                // net_os_free mesage.
                DHCP_FreeMsgIfNeed(&pMessage);  
                
                // Next state.
                hDhcp->State = DHCP_STATE_BOUND;
            }
            break;
            
            case DHCP_STATE_BOUND:
            {
                // Check lease time.
                uint32_t leaseTime = 0;
                uint32_t leftTime = 0;
                
                NET_OS_CRITICAL()
                {
                    leaseTime = NET_GetDHCPLeaseTime(hDhcp->NetHandle);
                    leftTime = NET_GetDHCPLeftTime(hDhcp->NetHandle);
                }
                
                if ((leaseTime / 2) > leftTime)
                {
                    return DHCP_ERR_OK;
                }                
                /*
                // Alloc message buffer.
                if ((error = DHCP_AllocMsgIfNeed(&pMessage)) != DHCP_ERR_OK) return error;
                
                // Open UDP socket.
                if ((error = DHCP_OpenUDPIfNeed(&udpSocket)) != DHCP_ERR_OK)
                {
                    DHCP_net_os_freeMsgIfNeed(&pMessage);
                    return error;
                }
                
                
                while(true)
                {
                    
                }*/
                
            }
            return DHCP_ERR_OK;
            
            default:                
                /* disable warnings*/
                DHCP_SendRELEASE(NULL, NULL, NULL);
                DHCP_SendDECLINE(NULL, NULL, NULL);
                DHCP_SendINFORM(NULL, NULL, NULL);
                error = DHCP_ERR_WRONG_STATE;
                return error;
        }        
    }
}

// Fills all fields of DHCP message, whose values are the same in all requests to server.
static void DHCP_FillMessage(DHCP_HandleTypeDef *hDhcp, DHCP_MessageTypeDef *pMessage)
{
    // Message must be cleared previously because empty fields not set directly.
    
    uint8_t *pMacAddress = NET_GetMacAddress(hDhcp->NetHandle);
    NET_IPAddressTypeDef ipAddress = NET_GetIPAddress(hDhcp->NetHandle);
    
    pMessage->MessageOPCode = DHCP_OPCODE_BOOTREQUEST;
    pMessage->HAddressType = DHCP_HTYPE_10MB;
    pMessage->HAddressLength = DHCP_HADDRESS_LENGTH;
    pMessage->Hops = DHCP_CLIENT_HOPS;
    pMessage->TransactionID = htonl(hDhcp->TransactionId);
    pMessage->Seconds = htons(DHCP_CLIENT_SECONDS);    
    
    pMessage->ClientHAddress[0] = pMacAddress[5];
    pMessage->ClientHAddress[1] = pMacAddress[4];
    pMessage->ClientHAddress[2] = pMacAddress[3];
    pMessage->ClientHAddress[3] = pMacAddress[2];
    pMessage->ClientHAddress[4] = pMacAddress[1];
    pMessage->ClientHAddress[5] = pMacAddress[0];
    
    if (ipAddress.Value == DHCP_ADDRESS_ZERO)
    {
        // Broadcast message. Local IP is 0.0.0.0. Client address field already set to 0
        pMessage->Flags = htons(DHCP_FLAGSBROADCAST);
    }
    else
    {
        // Unicast message. Local IP is normal.
        pMessage->Flags = htons(DHCP_FLAGSUNICAST);
        
        *((uint32_t *)pMessage->ClientIPAddress) = htonl(ipAddress.Value);
    }
    
    *((uint32_t *)pMessage->Options) = htonl(DHCP_MAGIC_COOKIE);
    
    // Another fields set to 0 by memset.
}

static NET_ErrorTypeDef DHCP_SendDISCOVER(DHCP_HandleTypeDef *hDhcp, DHCP_MessageTypeDef *pMessage, UDP_SocketHandleTypeDef *hUdp)
{
    NET_ErrorTypeDef error = DHCP_ERR_OK;
    
    
    // Clear message, fill common message fields and magic cookie.
    memset(pMessage, 0, sizeof(DHCP_MessageTypeDef) + DHCP_OPTIONS_MAX_SIZE);
    
    DHCP_FillMessage(hDhcp, pMessage);
    
    
    // Add options.
    uint16_t optionLen = DHCP_MAGIC_COOKIE_LENGTH; // Magic cookie already added.
    
    DHCP_AddOptionMESSAGETYPE                  (pMessage, &optionLen, DHCP_TYPE_DISCOVER);
    DHCP_AddOptionCLIENTIDENTIFIER      (hDhcp, pMessage, &optionLen);
    DHCP_AddOptionREQUESTEDIPADDRESS    (hDhcp, pMessage, &optionLen);
    DHCP_AddOptionHOSTNAME              (hDhcp, pMessage, &optionLen);
    DHCP_AddOptionPARAMETERREQUESTLIST         (pMessage, &optionLen);
     
    pMessage->Options[optionLen++] = DHCP_OPTION_END;
    
    
    #ifdef DHCP_DEBUG
    
    net_os_puts("DHCP: send DISCOVER");
    
    #endif
    
    
    // Send DISCOVER message.
    if ((error = DHCP_SendUDPMessage(hDhcp, hUdp, pMessage, sizeof(DHCP_MessageTypeDef) + optionLen)) != DHCP_ERR_OK) return error;
    
    
    return error;
}

static NET_ErrorTypeDef DHCP_SendREQUEST(DHCP_HandleTypeDef *hDhcp, DHCP_MessageTypeDef *pMessage, UDP_SocketHandleTypeDef *hUdp)
{
    NET_ErrorTypeDef error = DHCP_ERR_OK;
    
    
    // Clear message, fill common message fields and magic cookie.
    memset(pMessage, 0, sizeof(DHCP_MessageTypeDef) + DHCP_OPTIONS_MAX_SIZE);
    
    DHCP_FillMessage(hDhcp, pMessage);
    
    
    // Add options.
    uint16_t optionLen = DHCP_MAGIC_COOKIE_LENGTH; // Magic cookie already added.
    
    DHCP_AddOptionMESSAGETYPE                  (pMessage, &optionLen, DHCP_TYPE_REQUEST);
    DHCP_AddOptionCLIENTIDENTIFIER      (hDhcp, pMessage, &optionLen);
    DHCP_AddOptionHOSTNAME              (hDhcp, pMessage, &optionLen);
    DHCP_AddOptionPARAMETERREQUESTLIST         (pMessage, &optionLen);
    
    switch(hDhcp->State)
    {
        case DHCP_STATE_SELECTING:
            DHCP_AddOptionSERVERIDENTIFIER      (hDhcp, pMessage, &optionLen);
            // No break.
        
        case DHCP_STATE_INIT_REBOOT:
            DHCP_AddOptionREQUESTEDIPADDRESS    (hDhcp, pMessage, &optionLen);
            break;
        
        default:
            break;
    }
    
    pMessage->Options[optionLen++] = DHCP_OPTION_END;
    
    
    #ifdef DHCP_DEBUG
    
    net_os_puts("DHCP: send REQUEST");
    
    #endif
        
    
    // Send REQUEST message.
    if ((error = DHCP_SendUDPMessage(hDhcp, hUdp, pMessage, sizeof(DHCP_MessageTypeDef) + optionLen)) != DHCP_ERR_OK) return error;
    
    
    return error;
}

static NET_ErrorTypeDef DHCP_SendDECLINE(DHCP_HandleTypeDef *hDhcp, DHCP_MessageTypeDef *pMessage, UDP_SocketHandleTypeDef *hUdp)
{
    NET_ErrorTypeDef error = DHCP_ERR_OK;
    
    
    // Clear message, fill common message fields and magic cookie.
    memset(pMessage, 0, sizeof(DHCP_MessageTypeDef) + DHCP_OPTIONS_MAX_SIZE);
    
    DHCP_FillMessage(hDhcp, pMessage);
    
    
    // Add options.
    //uint16_t optionLen = DHCP_MAGIC_COOKIE_LENGTH; // Magic cookie already added.
    
    
    return error;
}

static NET_ErrorTypeDef DHCP_SendRELEASE(DHCP_HandleTypeDef *hDhcp, DHCP_MessageTypeDef *pMessage, UDP_SocketHandleTypeDef *hUdp)
{
    NET_ErrorTypeDef error = DHCP_ERR_OK;
    
    
    // Clear message, fill common message fields and magic cookie.
    memset(pMessage, 0, sizeof(DHCP_MessageTypeDef) + DHCP_OPTIONS_MAX_SIZE);
    
    DHCP_FillMessage(hDhcp, pMessage);
    
    
    // Add options.
    //uint16_t optionLen = DHCP_MAGIC_COOKIE_LENGTH; // Magic cookie already added.
    
    
    return error;
}

static NET_ErrorTypeDef DHCP_SendINFORM(DHCP_HandleTypeDef *hDhcp, DHCP_MessageTypeDef *pMessage, UDP_SocketHandleTypeDef *hUdp)
{
    NET_ErrorTypeDef error = DHCP_ERR_OK;
    
    
    // Clear message, fill common message fields and magic cookie.
    memset(pMessage, 0, sizeof(DHCP_MessageTypeDef) + DHCP_OPTIONS_MAX_SIZE);
    
    DHCP_FillMessage(hDhcp, pMessage);
    
    
    // Add options.
    //uint16_t optionLen = DHCP_MAGIC_COOKIE_LENGTH; // Magic cookie already added.
    
    
    return error;
}

static void DHCP_AddOptionHOSTNAME(DHCP_HandleTypeDef *hDhcp, DHCP_MessageTypeDef *pMessage, uint16_t *pOptionIdx)
{
    char *hostName = NET_GetHostName(hDhcp->NetHandle);
    
    if (hostName != NULL)
    {
        pMessage->Options[*pOptionIdx++] = DHCP_OPTION_HOSTNAME;
        uint16_t lenIdx = *pOptionIdx++;
        
        for (uint8_t i = 0; i < hostName[i] != 0; i++)
        {
            pMessage->Options[*pOptionIdx++] = hostName[i];
        }
        
        
        uint8_t *macAddress = NET_GetMacAddress(hDhcp->NetHandle);
        
        pMessage->Options[*pOptionIdx++] = macAddress[2];
        pMessage->Options[*pOptionIdx++] = macAddress[1];
        pMessage->Options[*pOptionIdx++] = macAddress[0];
        
        pMessage->Options[lenIdx] = *pOptionIdx - 1 - lenIdx;
    }
}

static void DHCP_AddOptionREQUESTEDIPADDRESS(DHCP_HandleTypeDef *hDhcp, DHCP_MessageTypeDef *pMessage, uint16_t *pOptionIdx)
{
    NET_IPAddressTypeDef ipAddress = NET_GetIPAddress(hDhcp->NetHandle);
    NET_IPAddressTypeDef requestedAddress = { .Value = DHCP_ADDRESS_ZERO };
    
    if (DHCP_IsNormalIPAddress(ipAddress))
    {
        requestedAddress = ipAddress;
    }
    else if (DHCP_IsNormalIPAddress(hDhcp->TempClientAddress))
    {
        requestedAddress = hDhcp->TempClientAddress;
    }
    else return;
    
    pMessage->Options[(*pOptionIdx)++] = DHCP_OPTION_REQUESTEDIPADDRESS;
    pMessage->Options[(*pOptionIdx)++] = 4;
    pMessage->Options[(*pOptionIdx)++] = requestedAddress.Byte3.Value;
    pMessage->Options[(*pOptionIdx)++] = requestedAddress.Byte2.Value;
    pMessage->Options[(*pOptionIdx)++] = requestedAddress.Byte1.Value;
    pMessage->Options[(*pOptionIdx)++] = requestedAddress.Byte0.Value;
}

static void DHCP_AddOptionMESSAGETYPE(DHCP_MessageTypeDef *pMessage, uint16_t *pOptionIdx, DHCP_TypeTypeDef type)
{
    pMessage->Options[(*pOptionIdx)++] = DHCP_OPTION_MESSAGETYPE;
    pMessage->Options[(*pOptionIdx)++] = 1;
    pMessage->Options[(*pOptionIdx)++] = type;
}

static void DHCP_AddOptionSERVERIDENTIFIER(DHCP_HandleTypeDef *hDhcp, DHCP_MessageTypeDef *pMessage, uint16_t *pOptionIdx)
{
    NET_IPAddressTypeDef dhcpServerAddress = NET_GetDHCPServerAddress(hDhcp->NetHandle);
    NET_IPAddressTypeDef serverAddress = DHCP_IsNormalIPAddress(dhcpServerAddress) ? dhcpServerAddress : hDhcp->TempServerAddress;
    
    pMessage->Options[(*pOptionIdx)++] = DHCP_OPTION_SERVERIDENTIFIER;
    pMessage->Options[(*pOptionIdx)++] = 4;
    pMessage->Options[(*pOptionIdx)++] = serverAddress.Byte3.Value;
    pMessage->Options[(*pOptionIdx)++] = serverAddress.Byte2.Value;
    pMessage->Options[(*pOptionIdx)++] = serverAddress.Byte1.Value;
    pMessage->Options[(*pOptionIdx)++] = serverAddress.Byte0.Value;
}

static void DHCP_AddOptionPARAMETERREQUESTLIST(DHCP_MessageTypeDef *pMessage, uint16_t *pOptionIdx)
{
    pMessage->Options[(*pOptionIdx)++] = DHCP_OPTION_PARAMETERREQUESTLIST;
    uint16_t lenIdx = (*pOptionIdx)++;
    
    pMessage->Options[(*pOptionIdx)++] = DHCP_OPTION_SUBNETMASK;
    pMessage->Options[(*pOptionIdx)++] = DHCP_OPTION_ROUTER;
    pMessage->Options[(*pOptionIdx)++] = DHCP_OPTION_DOMAINNAMESERVER;
    pMessage->Options[(*pOptionIdx)++] = DHCP_OPTION_DOMAINNAME;
    pMessage->Options[(*pOptionIdx)++] = DHCP_OPTION_PERFORMROUTERDISCOVERY;
    pMessage->Options[(*pOptionIdx)++] = DHCP_OPTION_STATICROUTE;
    pMessage->Options[(*pOptionIdx)++] = DHCP_OPTION_NTPSERVERS;
    pMessage->Options[(*pOptionIdx)++] = DHCP_OPTION_RENEWALT1TIMEVALUE;
    pMessage->Options[(*pOptionIdx)++] = DHCP_OPTION_REBINDINGT2TIMEVALUE;
    
    pMessage->Options[lenIdx] = *pOptionIdx - 1 - lenIdx;
}

static void DHCP_AddOptionCLIENTIDENTIFIER(DHCP_HandleTypeDef *hDhcp, DHCP_MessageTypeDef *pMessage, uint16_t *pOptionIdx)
{
    uint8_t *pMacAddress = NET_GetMacAddress(hDhcp->NetHandle);
    
    pMessage->Options[(*pOptionIdx)++] = DHCP_OPTION_CLIENTIDENTIFIER;
    pMessage->Options[(*pOptionIdx)++] = 7;
    pMessage->Options[(*pOptionIdx)++] = DHCP_HTYPE_10MB;
    pMessage->Options[(*pOptionIdx)++] = pMacAddress[5];
    pMessage->Options[(*pOptionIdx)++] = pMacAddress[4];
    pMessage->Options[(*pOptionIdx)++] = pMacAddress[3];
    pMessage->Options[(*pOptionIdx)++] = pMacAddress[2];
    pMessage->Options[(*pOptionIdx)++] = pMacAddress[1];
    pMessage->Options[(*pOptionIdx)++] = pMacAddress[0];
}

static NET_ErrorTypeDef DHCP_ProcessResponse(DHCP_HandleTypeDef *hDhcp, UDP_SocketHandleTypeDef *hUdp)
{
    NET_ErrorTypeDef error = DHCP_ERR_OK;
    
    
    while (true)
    {
        // Wait for answer datagram.
        UDP_DatagramTypeDef datagram;
        memset(&datagram, 0, sizeof(UDP_DatagramTypeDef));
            
        if ((error = UDP_PollForReceiveDatagram(hUdp, &datagram, &(hDhcp->Timeout))) != UDP_ERR_OK)
        {
            if (error == UDP_ERR_POLL_TIMEOUT)
            {
                #ifdef DHCP_DEBUG

                net_os_puts("DHCP: data receive timeout");

                #endif
                
                return DHCP_ERR_NO_ANSWER;
            }
            else if (error == UDP_ERR_RX_EMPTY)
            {
                error = UDP_ERR_OK;
                continue;
            }
            else
            {
                return (NET_ErrorTypeDef)(error | DHCP_ERR_LOW_LEVEL);
            }
        }


        // Check length (with magic cookie) and remote port equality.
        if ((datagram.FullLength < sizeof(DHCP_MessageTypeDef) + DHCP_MAGIC_COOKIE_LENGTH) || (DHCP_SERVER_PORT != datagram.RemotePoint.Port.Value))
        {
            // Flush datagram.
            if ((error = UDP_FlushDatagram(&datagram)) != UDP_ERR_OK)
            {                    
                return (NET_ErrorTypeDef)(error | DHCP_ERR_LOW_LEVEL);
            }
                
            continue;
        }
        
        #ifdef DHCP_DEBUG
        
        net_os_puts("DHCP: datagram checked");
        
        #endif
        
        
        // Allocate buffer for answer.
        DHCP_MessageTypeDef *pAnswer = (DHCP_MessageTypeDef *)net_os_malloc(datagram.FullLength);
        
        if (pAnswer == NULL)
        {                
            return DHCP_ERR_MEM_ALLOCATION;
        }
        
        
        // Receive data of datagram.
        uint16_t receivedAnswerLength = 0;
        
        if ((error = UDP_ReceiveData(&datagram, ((uint8_t *)pAnswer), datagram.FullLength, &receivedAnswerLength)) != UDP_ERR_OK)
        {
            net_os_free(pAnswer);
            
            return (NET_ErrorTypeDef)(error | DHCP_ERR_LOW_LEVEL);
        }
        
        #ifdef DHCP_DEBUG
        
        net_os_puts("DHCP: datagram received");
        
        #endif
        
        
        // Try parse response.
        if ((error = DHCP_ParseResponse(hDhcp, pAnswer, datagram.FullLength)) != DHCP_ERR_BAD_RESPONSE)
        {
            #ifdef DHCP_DEBUG
        
            if (error == DHCP_ERR_OK) net_os_puts("DHCP: datagram parsed");
            
            #endif
            
            
            net_os_free(pAnswer);
            
            return error;
        }
        
        
        net_os_free(pAnswer);
    };
}

static NET_ErrorTypeDef DHCP_ParseResponse(DHCP_HandleTypeDef *hDhcp, DHCP_MessageTypeDef *pAnswer, uint16_t answerLen)
{
    NET_ErrorTypeDef error = DHCP_ERR_OK;
    
    
    // Check OpCode, transaction ID, client Mac, magic cookie.
    if (pAnswer->MessageOPCode != DHCP_OPCODE_BOOTREPLY || pAnswer->TransactionID != htonl(hDhcp->TransactionId) ||
        !DHCP_IsMacEqualToInversed(NET_GetMacAddress(hDhcp->NetHandle), pAnswer->ClientHAddress) || *((uint32_t *)&(pAnswer->Options)) != htonl(DHCP_MAGIC_COOKIE))
    {
        return DHCP_ERR_BAD_RESPONSE;
    }
    
    NET_IPAddressTypeDef ipAddress = { .Value = ntohl(*((uint32_t *)pAnswer->YourIPAddress)) };
    
    #ifdef DHCP_DEBUG

    net_os_print_ip("DHCP: IPAddress:", ipAddress, "\0");

    #endif
    
    
    uint8_t *pLast = pAnswer->Options + (answerLen - sizeof(DHCP_MessageTypeDef) - 1);
    uint8_t *pIdx = pAnswer->Options + DHCP_MAGIC_COOKIE_LENGTH;
    
    
    DHCP_TypeTypeDef messageType = DHCP_TYPE_NONE;
    NET_IPAddressTypeDef subnetMask = { .Value = DHCP_ADDRESS_ZERO };
    NET_IPAddressTypeDef defaultGateway = { .Value = DHCP_ADDRESS_ZERO };
    NET_IPAddressTypeDef dnsServer = { .Value = DHCP_ADDRESS_ZERO };
    NET_IPAddressTypeDef dhcpServer = { .Value = DHCP_ADDRESS_ZERO };
    uint32_t leaseTime = 0;
    NET_IPAddressTypeDef sntpServer = { .Value = DHCP_ADDRESS_ZERO };
    
    
    while (pIdx <= pLast)
    {
        switch (*pIdx)
        {
            case DHCP_OPTION_PAD:
                pIdx++;
                break;
            
            case DHCP_OPTION_END:
                pIdx = pLast + 1;
                break;
            
            case DHCP_OPTION_SUBNETMASK:
                {
                    pIdx++;
                    uint8_t len = *pIdx;
                    pIdx++;
                    uint8_t *next = pIdx + len;
                    
                    subnetMask.Value = ntohl(*((uint32_t *)pIdx));
                    
                    pIdx = next;
                    
                    #ifdef DHCP_DEBUG
                    
                    net_os_print_ip("DHCP: SUBNETMASK:", subnetMask, "\0");
                    
                    #endif
                }
                break;
                
            case DHCP_OPTION_ROUTER:
                {
                    pIdx++;
                    uint8_t len = *pIdx;
                    pIdx++;
                    uint8_t *next = pIdx + len;
                    
                    defaultGateway.Value = ntohl(*((uint32_t *)pIdx));
                    
                    pIdx = next;
                    
                    #ifdef DHCP_DEBUG
                    
                    net_os_print_ip("DHCP: ROUTER:", defaultGateway, "\0");
                    
                    #endif
                }
                break;
                
            case DHCP_OPTION_DOMAINNAMESERVER:
                {
                    pIdx++;
                    uint8_t len = *pIdx;
                    pIdx++;
                    uint8_t *next = pIdx + len;
                    
                    dnsServer.Value = ntohl(*((uint32_t *)pIdx));
                    
                    pIdx = next;
                    
                    #ifdef DHCP_DEBUG
                    
                    net_os_print_ip("DHCP: DOMAINNAMESERVER:", dnsServer, "\0");
                    
                    #endif
                }
                break;
                
                case DHCP_OPTION_NTPSERVERS:
                    {
                        pIdx++;
                        uint8_t len = *pIdx;
                        pIdx++;
                        uint8_t *next = pIdx + len;
                        
                        sntpServer.Value = ntohl(*((uint32_t *)pIdx));
                        
                        pIdx = next;
                        
                        #ifdef DHCP_DEBUG
                        
                        net_os_print_ip("DHCP: NTPSERVERS:", sntpServer, "\0");
                        
                        #endif
                    }
                    break;
                
            case DHCP_OPTION_IPADDRESSLEASETIME:
                {
                    pIdx++;
                    uint8_t len = *pIdx;
                    pIdx++;
                    uint8_t *next = pIdx + len;
                    
                    leaseTime = ntohl(*((uint32_t *)pIdx));
                    
                    pIdx = next;
                    
                    #ifdef DHCP_DEBUG
                    
                    net_os_printf("DHCP: IPADDRESSLEASETIME:%u\r\n", leaseTime);
                    
                    #endif
                }
                break;
                
            case DHCP_OPTION_MESSAGETYPE:
                {
                    pIdx++;
                    pIdx++;
                    messageType = (DHCP_TypeTypeDef)*pIdx;
                    pIdx++;
                    
                    #ifdef DHCP_DEBUG
                    
                    net_os_printf("DHCP: MESSAGETYPE:%u\r\n", messageType);
                    
                    #endif
                }
                break;
                
            case DHCP_OPTION_SERVERIDENTIFIER:
                {
                    pIdx++;
                    uint8_t len = *pIdx;
                    pIdx++;
                    uint8_t *next = pIdx + len;
                    
                    dhcpServer.Value = ntohl(*((uint32_t *)pIdx));
                    
                    pIdx = next;
                    
                    #ifdef DHCP_DEBUG
                    
                    net_os_print_ip("DHCP: SERVERIDENTIFIER:", dhcpServer, "\0");
                    
                    #endif
                }
                break;
                
            default:
                 {
                     pIdx++;
                     uint8_t len = *pIdx;
                     pIdx += len + 1;
                 }
                 break;
        }
    }
    
    
    switch (hDhcp->State)
    {
        case DHCP_STATE_SELECTING:
            if (messageType != DHCP_TYPE_OFFER)
            {
                return DHCP_ERR_BAD_RESPONSE;
            }
            hDhcp->TempClientAddress = ipAddress;
            hDhcp->TempServerAddress = dhcpServer;
            break;
        
        case DHCP_STATE_REQUESTING:
            if (messageType == DHCP_TYPE_OFFER)
            {
                return DHCP_ERR_BAD_RESPONSE;
            }
        
            NET_SetIPAddress(hDhcp->NetHandle, ipAddress);        
            NET_SetSubnetMask(hDhcp->NetHandle, subnetMask);
            NET_SetDefaultGateway(hDhcp->NetHandle, defaultGateway);
            NET_SetDNSServerAddress(hDhcp->NetHandle, dnsServer);
            NET_SetDHCPServerAddress(hDhcp->NetHandle, dhcpServer);
            NET_SetDHCPLeaseTime(hDhcp->NetHandle, leaseTime);
            NET_SetDHCPLeftTime(hDhcp->NetHandle, leaseTime); // Same with lease time.
            NET_SetSNTPServerAddress(hDhcp->NetHandle, sntpServer);
        
        default: break; /* remove warning */
    }
    
    
    return error;
}

static NET_ErrorTypeDef DHCP_AllocMsgIfNeed(DHCP_MessageTypeDef **ppMessage)
{
    NET_ErrorTypeDef error = DHCP_ERR_OK;
    
    
    if (*ppMessage == NULL)
    {
        *ppMessage = (DHCP_MessageTypeDef *)net_os_malloc(sizeof(DHCP_MessageTypeDef) + DHCP_OPTIONS_MAX_SIZE);
    
        if (*ppMessage == NULL)
        {
            return DHCP_ERR_MEM_ALLOCATION;
        }
    }
    
    
    return error;
}

static void DHCP_FreeMsgIfNeed(DHCP_MessageTypeDef **ppMessage)
{
    if (*ppMessage != NULL)
    {
        net_os_free(*ppMessage);
        
        *ppMessage = NULL;
    }
}

static NET_ErrorTypeDef DHCP_OpenUDPIfNeed(DHCP_HandleTypeDef *hDhcp, UDP_SocketHandleTypeDef *hUdp)
{
    NET_ErrorTypeDef error = DHCP_ERR_OK;
    
    
    memset(hUdp, 0, sizeof(UDP_SocketHandleTypeDef));
    hUdp->LocalPort.Value = DHCP_CLIENT_PORT;
    
    
    uint16_t timeout = NET_POLL_INFINITE;    
    
    UDP_StateTypeDef udpState = UDP_STATE_UNKNOWN;
        
    if ((error = UDP_GetState(hUdp, &udpState)) != UDP_ERR_OK)
    {
        return (NET_ErrorTypeDef)(error | DHCP_ERR_LOW_LEVEL);
    }
    
    if (udpState == UDP_STATE_CLOSED)
    {
        if ((error = UDP_PollForOpenSocket(hUdp, &timeout, hDhcp->NetHandle)) != UDP_ERR_OK)
        {
            return (NET_ErrorTypeDef)(error | DHCP_ERR_LOW_LEVEL);
        }
    }
    
    
    return error;
}

static NET_ErrorTypeDef DHCP_CloseUDPIfNeed(UDP_SocketHandleTypeDef *hUdp)
{
    NET_ErrorTypeDef error = DHCP_ERR_OK;
    
    
    if ((error = UDP_CloseSocket(hUdp)) != UDP_ERR_OK)
    {
        return (NET_ErrorTypeDef)(error | DHCP_ERR_LOW_LEVEL);
    }
    
    
    return error;
}

static NET_ErrorTypeDef DHCP_SendUDPMessage(DHCP_HandleTypeDef *hDhcp, UDP_SocketHandleTypeDef *hUdp, DHCP_MessageTypeDef *pMessage, uint16_t messageLen)
{
    NET_ErrorTypeDef error = DHCP_ERR_OK;
    
    NET_IPAddressTypeDef dhcpServerAddress = NET_GetDHCPServerAddress(hDhcp->NetHandle);
    NET_EndPointTypeDef serverEndPoint =
        { .IPAddress.Value = DHCP_IsNormalIPAddress(dhcpServerAddress) ? dhcpServerAddress.Value : DHCP_ADDRESS_BROADCAST, .Port.Value = DHCP_SERVER_PORT };
    
        
    while (true)
    {        
        hDhcp->Timeout = DHCP_POLL_TIMEOUT;
        
        if ((error = UDP_PollForSendData(hUdp, (uint8_t *)pMessage, messageLen, &serverEndPoint, &(hDhcp->Timeout))) == UDP_ERR_OK) break;
        
        if (error == UDP_ERR_SEND_TIMEOUT || error == UDP_ERR_POLL_TIMEOUT)
        {
            if (!(hDhcp->TryCounter--))
            {
                return DHCP_ERR_SERVER_UNREACHABLE;
            }
            
            error = DHCP_ERR_OK;
            hDhcp->TransactionId++; // Increment ID in each resending.
        }
        else
        {
            return (NET_ErrorTypeDef)(error | DHCP_ERR_LOW_LEVEL);
        }
    }
    
    
    return error;
}

static bool DHCP_IsNormalIPAddress(NET_IPAddressTypeDef address)
{
    return address.Value != DHCP_ADDRESS_ZERO && address.Value != DHCP_ADDRESS_BROADCAST;
}

static bool DHCP_IsMacEqualToInversed(uint8_t *firstMac, uint8_t *secondMac)
{
    return firstMac[0] == secondMac[5] &&
           firstMac[1] == secondMac[4] &&
           firstMac[2] == secondMac[3] &&
           firstMac[3] == secondMac[2] &&
           firstMac[4] == secondMac[1] &&
           firstMac[5] == secondMac[0];
}
