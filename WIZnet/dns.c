#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "net.h"
#include "net_def.h"
#include "net_os.h"
#include "udp.h"

#include "dns.h"


#define DNS_PORT    53


enum __DNS_FLAGS
{
    DNS_FLAG_QUERY                  = (0x00 << 15),
    DNS_FLAG_RESPONSE               = (0x01 << 15),
    DNS_FLAG_STANDART_QUERY         = (0x00 << 11),
    DNS_FLAG_INVERSE_QUERY          = (0x04 << 11),
    DNS_FLAG_NAUTH_ANSWER           = (0x00 << 10),
    DNS_FLAG_AUTH_ANSWER            = (0x01 << 10),
    DNS_FLAG_MES_NTRUNC             = (0x00 << 9),
    DNS_FLAG_MES_TRUNC              = (0x01 << 9),
    DNS_FLAG_NRECURSIVE_QUERY       = (0x00 << 8),
    DNS_FLAG_RECURSIVE_QUERY        = (0x01 << 8), // Recursion desired in query
    DNS_FLAG_RECURSION_NAV          = (0x00 << 7),
    DNS_FLAG_RECURSION_AV           = (0x01 << 7), // Recursion available in query
    DNS_FLAG_ANSWER_NAUTH           = (0x00 << 5),
    DNS_FLAG_ANSWER_AUTH            = (0x01 << 5), // Answer authorized
    DNS_FLAG_CHECK_ENABLE           = (0x00 << 4),
    DNS_FLAG_CHECK_DISABLE          = (0x01 << 4), // Check disable
    
    DNS_FLAG_ERROR_OK               = (0x00 << 0), // Answer ok, no error
    DNS_FLAG_ERROR_FORMAT_ERROR     = (0x04 << 0), // Packet format error
    DNS_FLAG_ERROR_SERVER_FAILURE   = (0x02 << 0), // Server failure
    DNS_FLAG_ERROR_NOT_EXIST        = (0x01 << 0), // Name not exist
    
    __DNS_FLAG_ERROR_MASK           = (0x07 << 0)
};

enum __DNS_QUERY_TYPES
{
    DNS_QUERY_TYPE_A                = 0x0001, // IP address.
    DNS_QUERY_TYPE_NS               = 0x0002, // DNS Server.
    DNS_QUERY_TYPE_CNAME            = 0x0005, // Canonical name.
    DNS_QUERY_TYPE_PTR              = 0x000C, // Pointer.
    DNS_QUERY_TYPE_HINFO            = 0x000D, // Host info.
    DNS_QUERY_TYPE_MX               = 0x000F, // Mail exchange record.
    DNS_QUERY_TYPE_AXFR             = 0x00FC, // Zone request for transfering.
    DNS_QUERY_TYPE_ANY              = 0x00FF, // Query for all records transfering.
    
    __DNS_QUERY_TYPE_RESERVED       = 0x7FFF,
};

enum __DNS_QUERY_CLASSES
{
    DNS_QUERY_CLASS_IN              = 0x0001, // Internet.
    
    __DNS_QUERY_CLASS_RESERVED      = 0x7FFF,
};


typedef struct
{
    uint16_t TransactionID;     // Transaction identifier.
    uint16_t Flags;             // Flags for query.
    uint16_t Questions;         // Number of questions.
    uint16_t AnswerRRs;         // Number of answers.
    uint16_t AuthorityRRs;      // Number of authorities sections.
    uint16_t AdditionalRRs;     // Number of additional sections.
    uint8_t Body[];             // Query/Answers for name resolving.
}
DNS_QueryTypeDef; // Big-endian.

typedef DNS_QueryTypeDef DNS_ResponseTypeDef; // Big-endian.


static NET_ErrorTypeDef DNS_ResolveFromServer(DNS_HandleTypeDef *hDns, const char *pHostName, NET_IPAddressTypeDef *pHostAddress, uint32_t *timeToLive);
static NET_ErrorTypeDef DNS_AllocQuery(const char *hostName, DNS_QueryTypeDef **ppQuery, uint16_t *pLength);
static NET_ErrorTypeDef DNS_UDPProcess(DNS_HandleTypeDef *hDns, DNS_QueryTypeDef *pQuery, uint16_t queryLen, uint8_t **ppResponse, UDP_SocketHandleTypeDef *hUdp, NET_IPAddressTypeDef *pHostAddress, uint32_t *pTimeToLive);
static NET_ErrorTypeDef DNS_ParseResponse(uint8_t *pResponse, uint16_t responseLength, uint16_t queryLength, NET_IPAddressTypeDef *pHostAddress, uint32_t *timeToLive);


// Allocate DNS handle.
DNS_HandleTypeDef * DNS_AllocateHandle(NET_HandleTypeDef *hNet, uint8_t cacheSize)
{
    if (hNet == NULL || cacheSize == 0) return NULL;
    
    size_t handleSize = sizeof(DNS_HandleTypeDef) + (sizeof(DNS_RecordTypeDef) * cacheSize);
    
    DNS_HandleTypeDef *hDns = (DNS_HandleTypeDef *)net_os_malloc(handleSize);
    
    if (hDns == NULL) return NULL;
    
    NET_OS_MutexHandle *hMutex = NET_OS_CreateMutex();
    
    if (hMutex == NULL)
    {
        net_os_free(hDns);
        
        return NULL;
    }
    
    memset(hDns, 0, handleSize);
    
    hDns->NetHandle = hNet;
    hDns->CacheSize = cacheSize;
    hDns->TransactionId = 0;
    hDns->CacheMutex = hMutex;
    
    return hDns;
}

// Free DNS handle.
void DNS_FreeHandle(DNS_HandleTypeDef *hDns)
{
    if (hDns == NULL) return;
    
    if (hDns->CacheMutex != NULL)
    {
        NET_OS_DeleteMutex(hDns->CacheMutex);
    }
    
    net_os_free(hDns);
}

// DNS resolve host name.
NET_ErrorTypeDef DNS_Resolve(DNS_HandleTypeDef *hDns, const char *pHostName, NET_IPAddressTypeDef *pHostAddress)
{
    NET_ErrorTypeDef error = DNS_ERR_OK;
    
    
    if (hDns == NULL || hDns->NetHandle == NULL || hDns->CacheMutex == NULL || /* pHostName == NULL || */ pHostAddress == NULL)
    {
        return DNS_ERR_NULLPTR;
    }    
    
    if (pHostName == NULL)
    {
        return DNS_ERR_WRONG_HOSTNAME;
    }
    
    
    // Lock cache.
    NET_OS_MUTEX(hDns->CacheMutex, NET_OS_TIME_INFINITE)
    {
        // Search in cache.
        for (uint8_t i = 0; i < hDns->CacheSize; i++)
        {
            // Record found; unlock and return.
            if (hDns->Cache[i].TimeToLive != 0 && hDns->Cache[i].HostName != NULL && !strcmp(pHostName, hDns->Cache[i].HostName))
            {
                *pHostAddress = hDns->Cache[i].IPAddress;
                
                
                #ifdef DNS_DEBUG
                
                uint32_t debugTTL = hDns->Cache[i].TimeToLive;
                
                #endif
                
                
                NET_OS_ReleaseMutex(hDns->CacheMutex); // Unlock cache.
                
                
                #ifdef DNS_DEBUG
                
                net_os_printf("DNS: hostname:\"%s\" found in cache:%u.%u.%u.%u TTL:%u\r\n",
                    pHostName, pHostAddress->Byte3.Value, pHostAddress->Byte2.Value, pHostAddress->Byte1.Value, pHostAddress->Byte0.Value, debugTTL);
                        
                #endif
                
                
                return DNS_ERR_OK;
            }
        }
    }
    
    
    // Resolve host name in DNS server.
    uint32_t timeToLive;
    
    if ((error = DNS_ResolveFromServer(hDns, pHostName, pHostAddress, &timeToLive)) != DNS_ERR_OK)
    {
        return error;
    }
    
    
    // Add to cache.
    uint32_t lowTimeToLive = 0xFFFFFFFF;
    uint8_t recordWithLowTTL = 0xFF;
    
    // Lock cache.
    NET_OS_MUTEX(hDns->CacheMutex, NET_OS_TIME_INFINITE)
    {
        for (uint8_t i = 0; i < hDns->CacheSize; i++)
        {
            // Previous record found; replace, unlock and return.
            if (hDns->Cache[i].HostName != NULL && !strcmp(pHostName, hDns->Cache[i].HostName))
            {
                hDns->Cache[i].IPAddress = *pHostAddress;
                
                hDns->Cache[i].TimeToLive = timeToLive;
                
                
                NET_OS_ReleaseMutex(hDns->CacheMutex); // Unlock cache.
                
                
                return DNS_ERR_OK;
            }
            else
            {
                if (hDns->Cache[i].TimeToLive < lowTimeToLive)
                {
                    lowTimeToLive = hDns->Cache[i].TimeToLive;
                    recordWithLowTTL = i;
                }
            }
        }
        
        // Previouses records in cache not found. Replace record with lowest TTL.
        hDns->Cache[recordWithLowTTL].HostName = pHostName;
        hDns->Cache[recordWithLowTTL].IPAddress = *pHostAddress;
        hDns->Cache[recordWithLowTTL].TimeToLive = timeToLive;
    }
    
    
    return DNS_ERR_OK;
}

// Substract from all DNS records in cache.
void DNS_SecondCallback(DNS_HandleTypeDef *hDns)
{
    if (hDns == NULL || hDns->CacheMutex == NULL) return;
    
    // Lock cache.
    NET_OS_MUTEX(hDns->CacheMutex, NET_OS_TIME_INFINITE)
    {
        for (uint8_t i = 0; i < hDns->CacheSize; i++)
        {
            if (hDns->Cache[i].TimeToLive != 0)
            {
                hDns->Cache[i].TimeToLive--;
            }
        }
    }
}

// Send requests to server several times, wait for response for timeout.
static NET_ErrorTypeDef DNS_ResolveFromServer(DNS_HandleTypeDef *hDns, const char *pHostName, NET_IPAddressTypeDef *pHostAddress, uint32_t *pTimeToLive)
{
    NET_ErrorTypeDef error = DNS_ERR_OK;
    
    
    #ifdef DNS_DEBUG
            
    net_os_printf("DNS: resolve \"%s\"\r\n", pHostName);
            
    #endif
    
    
    // Allocate and fill query.
    DNS_QueryTypeDef *pQuery = NULL;
    uint16_t queryLength = 0;
    
    if ((error = DNS_AllocQuery(pHostName, &pQuery, &queryLength)) != DNS_ERR_OK)
    {
        return error;
    }
    

    // Open or wait for UDP socket.
    UDP_SocketHandleTypeDef udpSocket;
    memset(&udpSocket, 0, sizeof(UDP_SocketHandleTypeDef));
    udpSocket.LocalPort = NET_GetFreePort(hDns->NetHandle, NET_PORTMODE_UDP);
    
    uint16_t timeout = NET_POLL_INFINITE;
    
    if ((error = UDP_PollForOpenSocket(&udpSocket, &timeout, hDns->NetHandle)) != UDP_ERR_OK)
    {
        net_os_free(pQuery);
        
        return (NET_ErrorTypeDef)(error | DNS_ERR_LOW_LEVEL);
    }
    
    #ifdef DNS_DEBUG
            
    net_os_puts("DNS: socket opened");
            
    #endif
    
    
    // Process UDP.
    uint8_t *pResponse = NULL;
    
    error = DNS_UDPProcess(hDns, pQuery, queryLength, &pResponse, &udpSocket, pHostAddress, pTimeToLive);
    
    #ifdef DNS_DEBUG
            
    if (error == DNS_ERR_OK) net_os_printf("DNS: resolved %u.%u.%u.%u TTL:%u\r\n",
        pHostAddress->Byte3.Value, pHostAddress->Byte2.Value, pHostAddress->Byte1.Value, pHostAddress->Byte0.Value, *pTimeToLive);
            
    #endif
    
    
    // Close socket, net_os_free resources.
    NET_ErrorTypeDef closeError;
    
    if ((closeError = UDP_CloseSocket(&udpSocket)) != UDP_ERR_OK)
    {
        if (error != DNS_ERR_OK && error != DNS_ERR_CANNOT_RESOLVE)
        {
            error = (NET_ErrorTypeDef)(closeError | DNS_ERR_LOW_LEVEL);
        }
    }
    
    net_os_free(pQuery);
    net_os_free(pResponse);
    
    return error;
}

// Allocate DNS query for specified host name. 
static NET_ErrorTypeDef DNS_AllocQuery(const char *pHostName, DNS_QueryTypeDef **ppQuery, uint16_t *pLength)
{    
    uint16_t hostNameLen = strlen(pHostName);
    uint16_t queryBodyLen = 1 + hostNameLen + 1 + 2 + 2;
    *pLength = sizeof(DNS_QueryTypeDef) + queryBodyLen;

    *ppQuery = net_os_malloc(*pLength);
    
    if (*ppQuery == NULL)
    {
        return DNS_ERR_MEM_ALLOCATION;
    }
    
    
    // Transaction ID set later.
    (*ppQuery)->Flags = htons((uint16_t)(DNS_FLAG_QUERY | DNS_FLAG_STANDART_QUERY | DNS_FLAG_MES_NTRUNC | DNS_FLAG_RECURSIVE_QUERY | DNS_FLAG_CHECK_ENABLE));
    (*ppQuery)->Questions = htons(1);
    (*ppQuery)->AnswerRRs = 0;
    (*ppQuery)->AuthorityRRs = 0;
    (*ppQuery)->AdditionalRRs = 0;
    
    
    (*ppQuery)->Body[hostNameLen + 1] = 0;
    
    uint8_t charCounter = 0;
    
    for (uint16_t i = hostNameLen; i > 0; i--)
    {
        uint16_t srcIdx = i - 1;
        uint16_t dstIdx = i;
        char character = pHostName[srcIdx];
        
        if ((character >= '0' && character <= '9') || (character >= 'A' && character <= 'Z') || (character >= 'a' && character <= 'z'))
        {
            charCounter++;
            (*ppQuery)->Body[dstIdx] = character;
        }        
        else if (character == '.')
        {
            (*ppQuery)->Body[dstIdx] = charCounter;
            charCounter = 0;
        }
        else
        {
            net_os_free(*ppQuery);
            *ppQuery = NULL;
            
            return DNS_ERR_WRONG_HOSTNAME;
        }
    }
    
    (*ppQuery)->Body[0] = charCounter;
    
    
    NET_Union16TypeDef typeUnion = { .Value = DNS_QUERY_TYPE_A };    
    NET_Union16TypeDef classUnion = { .Value = DNS_QUERY_CLASS_IN };
    
    (*ppQuery)->Body[hostNameLen + 2] = typeUnion.Byte1.Value;
    (*ppQuery)->Body[hostNameLen + 3] = typeUnion.Byte0.Value;
    (*ppQuery)->Body[hostNameLen + 4] = classUnion.Byte1.Value;
    (*ppQuery)->Body[hostNameLen + 5] = classUnion.Byte0.Value;
    
    
    return DNS_ERR_OK;
}

// Process work with UDP. Created for simple resource releasing.
static NET_ErrorTypeDef DNS_UDPProcess(DNS_HandleTypeDef *hDns, DNS_QueryTypeDef *pQuery, uint16_t queryLen, uint8_t **ppResponse, UDP_SocketHandleTypeDef *hUdp, NET_IPAddressTypeDef *pHostAddress, uint32_t *pTimeToLive)
{
    NET_ErrorTypeDef error = DNS_ERR_OK;
    
    uint8_t tryCounter = DNS_TRY_COUNT;
    
    
    // Try send packet, receive and parse answer.
    while (tryCounter--)
    {
        uint16_t timeLeft = DNS_POLL_TIMEOUT;
        
        
        // Create copy of transaction ID.
        NET_OS_CRITICAL()
        {
            pQuery->TransactionID = htons(hDns->TransactionId++);
        }
        
        
        // Wait for send packet to server.
        NET_EndPointTypeDef dnsServerEndPoint = { .IPAddress = NET_GetDNSServerAddress(hDns->NetHandle), .Port.Value = DNS_PORT };        
            
        if ((error = UDP_PollForSendData(hUdp, (uint8_t *)pQuery, queryLen, &dnsServerEndPoint, &timeLeft)) != UDP_ERR_OK)
        {
            if (error == UDP_ERR_POLL_TIMEOUT || error == UDP_ERR_SEND_TIMEOUT)
            {
                #ifdef DNS_DEBUG
            
                net_os_puts("DNS: data send timeout");
                    
                #endif
                
                error = UDP_ERR_OK;
                continue;
            }
            else
            {
                return (NET_ErrorTypeDef)(error | DNS_ERR_LOW_LEVEL);
            }
        }
        
        #ifdef DNS_DEBUG
            
        net_os_puts("DNS: data sent ok");
            
        #endif
        
        
        while (true)
        {
            // Wait for answer datagram.
            UDP_DatagramTypeDef datagram;
            memset(&datagram, 0, sizeof(UDP_DatagramTypeDef));
            
            if ((error = UDP_PollForReceiveDatagram(hUdp, &datagram, &timeLeft)) != UDP_ERR_OK)
            {
                if (error == UDP_ERR_POLL_TIMEOUT)
                {
                    #ifdef DNS_DEBUG
                
                    net_os_puts("DNS: data receive timeout");
                        
                    #endif
                    
                    error = UDP_ERR_OK;
                    break;
                }
                else if (error == UDP_ERR_RX_EMPTY)
                {
                    error = UDP_ERR_OK;
                    continue;
                }
                else
                {
                    return (NET_ErrorTypeDef)(error | DNS_ERR_LOW_LEVEL);
                }
            }


            // Check length and remote point equality.
            if ((datagram.FullLength <= queryLen) ||
                (dnsServerEndPoint.IPAddress.Value != datagram.RemotePoint.IPAddress.Value) || (dnsServerEndPoint.Port.Value != datagram.RemotePoint.Port.Value))
            {
                // Flush datagram.
                if ((error = UDP_FlushDatagram(&datagram)) != UDP_ERR_OK)
                {                    
                    return (NET_ErrorTypeDef)(error | DNS_ERR_LOW_LEVEL);
                }
                    
                continue;
            }
            
            #ifdef DNS_DEBUG
            
            net_os_puts("DNS: datagram checked");
            
            #endif
                        

            // Allocate buffer for response header.
            *ppResponse = (uint8_t *)net_os_malloc(sizeof(DNS_ResponseTypeDef));
            
            if (*ppResponse == NULL)
            {                
                return DNS_ERR_MEM_ALLOCATION;
            }
            
            // Receive head of datagram.
            uint16_t receivedResponseLength = 0;
            
            if ((error = UDP_ReceiveData(&datagram, ((uint8_t *)*ppResponse), sizeof(DNS_ResponseTypeDef), &receivedResponseLength)) != UDP_ERR_OK)
            {                
                return (NET_ErrorTypeDef)(error | DNS_ERR_LOW_LEVEL);
            }            
            
            
            // Check transaction ID, recursive query support, error flags and answers existence.
            DNS_ResponseTypeDef *pTypedResponse = (DNS_ResponseTypeDef *)*ppResponse;
            
            bool responseTransactionOk = pTypedResponse->TransactionID == pQuery->TransactionID;
            uint16_t responseFlags = ntohs(pTypedResponse->Flags);
            uint16_t neededFlagsExists = ((responseFlags & (DNS_FLAG_RESPONSE | DNS_FLAG_RECURSIVE_QUERY)) == (DNS_FLAG_RESPONSE | DNS_FLAG_RECURSIVE_QUERY));
            uint16_t errorFlags = (responseFlags & __DNS_FLAG_ERROR_MASK);
            uint16_t answerCount = ntohs(pTypedResponse->AnswerRRs);
            uint16_t authorityCount = ntohs(pTypedResponse->AuthorityRRs);
            uint16_t additionalCount = ntohs(pTypedResponse->AdditionalRRs);
            
            if (!responseTransactionOk || !neededFlagsExists|| errorFlags || (!answerCount && !authorityCount && !additionalCount))
            {
                // Flush datagram.
                if ((error = UDP_FlushDatagram(&datagram)) != UDP_ERR_OK)
                {                    
                    return (NET_ErrorTypeDef)(error | DNS_ERR_LOW_LEVEL);
                }
                    
                continue;
            }
            
            
            // Allocate buffer for query body flush.
            uint16_t flushLen = queryLen - sizeof(DNS_ResponseTypeDef);
            
            net_os_free(*ppResponse);
            
            *ppResponse = (uint8_t *)net_os_malloc(flushLen);
            
            if (*ppResponse == NULL)
            {                
                return DNS_ERR_MEM_ALLOCATION;
            }
            
            
            // Flush query body.
            if ((error = UDP_ReceiveData(&datagram, ((uint8_t *)*ppResponse), flushLen, &receivedResponseLength)) != UDP_ERR_OK)
            {                
                return (NET_ErrorTypeDef)(error | DNS_ERR_LOW_LEVEL);
            } 
            
            
            // Allocate buffer for response body.
            uint16_t responseLen = datagram.FullLength - queryLen;
                        
            net_os_free(*ppResponse);
            
            *ppResponse = (uint8_t *)net_os_malloc(responseLen);
            
            if (*ppResponse == NULL)
            {                
                return DNS_ERR_MEM_ALLOCATION;
            }
            
            if ((error = UDP_ReceiveData(&datagram, ((uint8_t *)*ppResponse), responseLen, &receivedResponseLength)) != UDP_ERR_OK)
            {                
                return (NET_ErrorTypeDef)(error | DNS_ERR_LOW_LEVEL);
            } 
            
            #ifdef DNS_DEBUG
            
            net_os_puts("DNS: datagram received");
            
            #endif
            
            
            // Parse response.
            if ((error = DNS_ParseResponse(*ppResponse, receivedResponseLength, queryLen, pHostAddress, pTimeToLive)) != DNS_ERR_BAD_RESPONSE)
            {                
                #ifdef DNS_DEBUG
            
                if (error == DNS_ERR_OK) net_os_puts("DNS: datagram parsed");
            
                #endif
                
                
                return error;
            }
        };
    }
    
    
    return DNS_ERR_CANNOT_RESOLVE;
}

// Parse DNS response.
static NET_ErrorTypeDef DNS_ParseResponse(uint8_t *pResponse, uint16_t responseLength, uint16_t queryLength, NET_IPAddressTypeDef *pHostAddress, uint32_t *pTimeToLive)
{
    NET_Union16TypeDef namePointer = { .Value = 0xC00C };
    
    for (uint8_t i = 1; i < responseLength; i++)
    {
        if (pResponse[i] == namePointer.Byte0.Value && pResponse[i - 1] == namePointer.Byte1.Value)
        {
            NET_Union16TypeDef type;
            type.Byte1.Value = pResponse[i + 1];
            type.Byte0.Value = pResponse[i + 2];
            
            if (type.Value == DNS_QUERY_TYPE_A)
            {
                NET_Union16TypeDef class_;
                class_.Byte1.Value = pResponse[i + 3];
                class_.Byte0.Value = pResponse[i + 4];
                
                NET_Union32TypeDef *pTimeToLiveRaw = (NET_Union32TypeDef *)pTimeToLive;
                pTimeToLiveRaw->Byte3.Value = pResponse[i + 5];
                pTimeToLiveRaw->Byte2.Value = pResponse[i + 6];
                pTimeToLiveRaw->Byte1.Value = pResponse[i + 7];
                pTimeToLiveRaw->Byte0.Value = pResponse[i + 8];
                
                NET_Union16TypeDef len;
                len.Byte1.Value = pResponse[i + 9];
                len.Byte0.Value = pResponse[i + 10];
                
                
                if (class_.Value != DNS_QUERY_CLASS_IN || len.Value != 4) return DNS_ERR_BAD_RESPONSE;
                
                
                pHostAddress->Byte3.Value = pResponse[i + 11];
                pHostAddress->Byte2.Value = pResponse[i + 12];
                pHostAddress->Byte1.Value = pResponse[i + 13];
                pHostAddress->Byte0.Value = pResponse[i + 14];
                
                
                return DNS_ERR_OK;
            }
            else if (type.Value == DNS_QUERY_TYPE_CNAME)
            {
                NET_Union16TypeDef len;
                len.Byte1.Value = pResponse[i + 9];
                len.Byte0.Value = pResponse[i + 10];
                
                namePointer.Value = 0xC000 + queryLength + i + 11;
                
                i += 11 + len.Value;
            }
            else
            {
                return DNS_ERR_BAD_RESPONSE;
            }            
        }
    }
    
    
    return DNS_ERR_OK;
}
