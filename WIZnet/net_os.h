#ifndef __NET_OS_H
#define __NET_OS_H


#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include "cmsis_os.h"

#include "net_def.h"


#define NET_OS_TIME_INFINITE    osWaitForever


typedef osStatus        NET_OS_OpStatus;

typedef osThreadId      NET_OS_TaskHandle;
typedef osMessageQId    NET_OS_QueueHandle;
typedef osMutexId       *NET_OS_MutexHandle;
typedef osMutexId       *NET_OS_RecursiveMutexHandle;
typedef osSemaphoreId   NET_OS_BinarySemaphoreHandle;
typedef osSemaphoreId   NET_OS_CountingSemaphoreHandle;


static inline bool NET_OS_IsStatusOK(NET_OS_OpStatus status)
{
    return status == osOK;
}


void * net_os_malloc(size_t size);
void net_os_free(void *ptr);


#define net_os_printf(format, args...) \
    do { \
        taskENTER_CRITICAL(); \
        printf (format, ##args); \
        taskEXIT_CRITICAL(); \
    } while(false) \
// End net_os_printf

#define net_os_puts(s) \
    do { \
        taskENTER_CRITICAL(); \
        puts(s); \
        taskEXIT_CRITICAL(); \
    } while(false) \
// End net_os_puts


void net_os_print_ip(const char *pre, NET_IPAddressTypeDef ipAddress, const char *post);

    
void            NET_OS_Delay                (uint32_t millisec);
uint32_t        NET_OS_Ticks                (void);
void            NET_OS_Yield                (void);

void            NET_OS_EnterCritical        (void);
void            NET_OS_ExitCritical         (void);
void            NET_OS_Lock                 (bool *locker);
void            NET_OS_Unlock               (bool *locker);

NET_OS_MutexHandle          * NET_OS_CreateMutex          (void);
NET_OS_RecursiveMutexHandle * NET_OS_CreateRecursiveMutex (void);
NET_OS_OpStatus             NET_OS_TakeMutex            (NET_OS_MutexHandle *hMutex, uint32_t time);
NET_OS_OpStatus             NET_OS_TakeRecursiveMutex   (NET_OS_RecursiveMutexHandle *hMutex, uint32_t time);
NET_OS_OpStatus             NET_OS_ReleaseMutex         (NET_OS_MutexHandle *hMutex);
NET_OS_OpStatus             NET_OS_ReleaseRecursiveMutex(NET_OS_RecursiveMutexHandle *hMutex);
NET_OS_OpStatus             NET_OS_DeleteMutex          (NET_OS_MutexHandle *hMutex);
NET_OS_OpStatus             NET_OS_DeleteRecursiveMutex (NET_OS_RecursiveMutexHandle *hMutex);


#define NET_OS_CRITICAL() \
    for (bool __loop = true; __loop; NET_OS_ExitCritical()) \
        for (NET_OS_EnterCritical(); __loop; __loop = false) \
// End NET_CRITICAL
    
#define NET_OS_LOCK(__l) \
    for (bool __loop = true; __loop; NET_OS_Unlock(__l)) \
        for (NET_OS_Lock(__l); __loop; __loop = false) \
// End NET_OS_LOCK

#define NET_OS_MUTEX(__m, __t) \
    for (bool __loop = true; __loop; NET_OS_TakeMutex(__m, __t)) \
        for (NET_OS_ReleaseMutex(__m); __loop; __loop = false) \
// End NET_OS_MUTEX

#define NET_OS_RECURSIVE_MUTEX(__m, __t) \
    for (bool __loop = true; __loop; NET_OS_TakeRecursiveMutex(__m, __t)) \
        for (NET_OS_ReleaseRecursiveMutex(__m); __loop; __loop = false) \
// End NET_OS_RECURSIVE_MUTEX
    

#endif /* __NET_OS_H */
