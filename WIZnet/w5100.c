#include <stdint.h>
#include <string.h>

#include "net_def.h"
#include "net_os.h"
#include "wizchip.h"

#include "w5100.h"


#define W5100_COMMON_REG_BASE       0x0000
#define W5100_SOCKET_REG_BASE       0x0400
#define W5100_SOCKET_REG_SIZE       0x0100
#define W5100_TX_MEMORY_BASE        0x4000
#define W5100_RX_MEMORY_BASE        0x6000
#define W5100_MEMORY_0KB            0x0000
#define W5100_MEMORY_1KB            0x0400

#define W5100_WRITE_OPCODE          0xF0
#define W5100_READ_OPCODE           0x0F

// Mode register address.
#define W5100_MR         (W5100_COMMON_REG_BASE + 0x0000)

// Gateway IP register address.
#define W5100_GAR0       (W5100_COMMON_REG_BASE + 0x0001)
#define W5100_GAR1       (W5100_COMMON_REG_BASE + 0x0002)
#define W5100_GAR2       (W5100_COMMON_REG_BASE + 0x0003)
#define W5100_GAR3       (W5100_COMMON_REG_BASE + 0x0004)

// Subnet mask register address.
#define W5100_SUBR0      (W5100_COMMON_REG_BASE + 0x0005)
#define W5100_SUBR1      (W5100_COMMON_REG_BASE + 0x0006)
#define W5100_SUBR2      (W5100_COMMON_REG_BASE + 0x0007)
#define W5100_SUBR3      (W5100_COMMON_REG_BASE + 0x0008)

// Source MAC register address.
#define W5100_SHAR0      (W5100_COMMON_REG_BASE + 0x0009)
#define W5100_SHAR1      (W5100_COMMON_REG_BASE + 0x000A)
#define W5100_SHAR2      (W5100_COMMON_REG_BASE + 0x000B)
#define W5100_SHAR3      (W5100_COMMON_REG_BASE + 0x000C)
#define W5100_SHAR4      (W5100_COMMON_REG_BASE + 0x000D)
#define W5100_SHAR5      (W5100_COMMON_REG_BASE + 0x000E)

// Source IP register address.
#define W5100_SIPR0      (W5100_COMMON_REG_BASE + 0x000F)
#define W5100_SIPR1      (W5100_COMMON_REG_BASE + 0x0010)
#define W5100_SIPR2      (W5100_COMMON_REG_BASE + 0x0011)
#define W5100_SIPR3      (W5100_COMMON_REG_BASE + 0x0012)

// Interrupt register.
#define W5100_IR         (W5100_COMMON_REG_BASE + 0x0015)

// Interrupt mask register.
#define W5100_IMR        (W5100_COMMON_REG_BASE + 0x0016)

// Timeout register address( 1 is 100us ).
#define W5100_RTR0       (W5100_COMMON_REG_BASE + 0x0017)
#define W5100_RTR1       (W5100_COMMON_REG_BASE + 0x0018)

// Retry count register.
#define W5100_RCR        (W5100_COMMON_REG_BASE + 0x0019)

// Receive memory size register.
#define W5100_RMSR       (W5100_COMMON_REG_BASE + 0x001A)

// Transmit memory size register.
#define W5100_TMSR       (W5100_COMMON_REG_BASE + 0x001B)

// Authentication type register address in PPPoE mode.
#define W5100_PATR0      (W5100_COMMON_REG_BASE + 0x001C)
#define W5100_PATR1      (W5100_COMMON_REG_BASE + 0x001D)

// Chip version register address.
#define W5100_VERSIONR   (W5100_COMMON_REG_BASE + 0x001F)

// PPP LCP request timer register in PPPoE mode.
#define W5100_PTIMER     (W5100_COMMON_REG_BASE + 0x0028)

// PPP LCP magic number register in PPPoE mode.
#define W5100_PMAGIC     (W5100_COMMON_REG_BASE + 0x0029)

// Unreachable IP register address in UDP mode.
#define W5100_UIPR0      (W5100_COMMON_REG_BASE + 0x002A)
#define W5100_UIPR1      (W5100_COMMON_REG_BASE + 0x002B)
#define W5100_UIPR2      (W5100_COMMON_REG_BASE + 0x002C)
#define W5100_UIPR3      (W5100_COMMON_REG_BASE + 0x002D)

// Unreachable port register address in UDP mode.
#define W5100_UPORT0     (W5100_COMMON_REG_BASE + 0x002E)
#define W5100_UPORT1     (W5100_COMMON_REG_BASE + 0x002F)

// Socket mode register.
#define W5100_S_MR(socket)         (W5100_SOCKET_REG_BASE + (socket * W5100_SOCKET_REG_SIZE) + 0x0000)

// Channel command register.
#define W5100_S_CR(socket)         (W5100_SOCKET_REG_BASE + (socket * W5100_SOCKET_REG_SIZE) + 0x0001)

// Channel interrupt register.
#define W5100_S_IR(socket)         (W5100_SOCKET_REG_BASE + (socket * W5100_SOCKET_REG_SIZE) + 0x0002)

// Channel status register.
#define W5100_S_SR(socket)         (W5100_SOCKET_REG_BASE + (socket * W5100_SOCKET_REG_SIZE) + 0x0003)

// Source port register.
#define W5100_S_PORT0(socket)      (W5100_SOCKET_REG_BASE + (socket * W5100_SOCKET_REG_SIZE) + 0x0004)
#define W5100_S_PORT1(socket)      (W5100_SOCKET_REG_BASE + (socket * W5100_SOCKET_REG_SIZE) + 0x0005)

// Destination MAC register address.
#define W5100_S_DHAR0(socket)      (W5100_SOCKET_REG_BASE + (socket * W5100_SOCKET_REG_SIZE) + 0x0006)
#define W5100_S_DHAR1(socket)      (W5100_SOCKET_REG_BASE + (socket * W5100_SOCKET_REG_SIZE) + 0x0007)
#define W5100_S_DHAR2(socket)      (W5100_SOCKET_REG_BASE + (socket * W5100_SOCKET_REG_SIZE) + 0x0008)
#define W5100_S_DHAR3(socket)      (W5100_SOCKET_REG_BASE + (socket * W5100_SOCKET_REG_SIZE) + 0x0009)
#define W5100_S_DHAR4(socket)      (W5100_SOCKET_REG_BASE + (socket * W5100_SOCKET_REG_SIZE) + 0x000A)
#define W5100_S_DHAR5(socket)      (W5100_SOCKET_REG_BASE + (socket * W5100_SOCKET_REG_SIZE) + 0x000B)

// Destination IP register address.
#define W5100_S_DIPR0(socket)      (W5100_SOCKET_REG_BASE + (socket * W5100_SOCKET_REG_SIZE) + 0x000C)
#define W5100_S_DIPR1(socket)      (W5100_SOCKET_REG_BASE + (socket * W5100_SOCKET_REG_SIZE) + 0x000D)
#define W5100_S_DIPR2(socket)      (W5100_SOCKET_REG_BASE + (socket * W5100_SOCKET_REG_SIZE) + 0x000E)
#define W5100_S_DIPR3(socket)      (W5100_SOCKET_REG_BASE + (socket * W5100_SOCKET_REG_SIZE) + 0x000F)

// Destination port register address.
#define W5100_S_DPORT0(socket)     (W5100_SOCKET_REG_BASE + (socket * W5100_SOCKET_REG_SIZE) + 0x0010)
#define W5100_S_DPORT1(socket)     (W5100_SOCKET_REG_BASE + (socket * W5100_SOCKET_REG_SIZE) + 0x0011)

// Maximum segment size register address.
#define W5100_S_MSSR0(socket)      (W5100_SOCKET_REG_BASE + (socket * W5100_SOCKET_REG_SIZE) + 0x0012)
#define W5100_S_MSSR1(socket)      (W5100_SOCKET_REG_BASE + (socket * W5100_SOCKET_REG_SIZE) + 0x0013)

// Protocol of IP header field register in IP raw mode.
#define W5100_S_PROTO(socket)      (W5100_SOCKET_REG_BASE + (socket * W5100_SOCKET_REG_SIZE) + 0x0014)

// IP type of service (TOS) Register.
#define W5100_S_TOS(socket)        (W5100_SOCKET_REG_BASE + (socket * W5100_SOCKET_REG_SIZE) + 0x0015)

// IP time to live (TTL) Register.
#define W5100_S_TTL(socket)        (W5100_SOCKET_REG_BASE + (socket * W5100_SOCKET_REG_SIZE) + 0x0016)

// Transmit free memory size register.
#define W5100_S_TX_FSR0(socket)    (W5100_SOCKET_REG_BASE + (socket * W5100_SOCKET_REG_SIZE) + 0x0020)
#define W5100_S_TX_FSR1(socket)    (W5100_SOCKET_REG_BASE + (socket * W5100_SOCKET_REG_SIZE) + 0x0021)

// Transmit memory read pointer register address.
#define W5100_S_TX_RD0(socket)     (W5100_SOCKET_REG_BASE + (socket * W5100_SOCKET_REG_SIZE) + 0x0022)
#define W5100_S_TX_RD1(socket)     (W5100_SOCKET_REG_BASE + (socket * W5100_SOCKET_REG_SIZE) + 0x0023)

// Transmit memory write pointer register address.
#define W5100_S_TX_WR0(socket)     (W5100_SOCKET_REG_BASE + (socket * W5100_SOCKET_REG_SIZE) + 0x0024)
#define W5100_S_TX_WR1(socket)     (W5100_SOCKET_REG_BASE + (socket * W5100_SOCKET_REG_SIZE) + 0x0025)

// Received data size register.
#define W5100_S_RX_RSR0(socket)    (W5100_SOCKET_REG_BASE + (socket * W5100_SOCKET_REG_SIZE) + 0x0026)
#define W5100_S_RX_RSR1(socket)    (W5100_SOCKET_REG_BASE + (socket * W5100_SOCKET_REG_SIZE) + 0x0027)

// Read point of receive memory.
#define W5100_S_RX_RD0(socket)     (W5100_SOCKET_REG_BASE + (socket * W5100_SOCKET_REG_SIZE) + 0x0028)
#define W5100_S_RX_RD1(socket)     (W5100_SOCKET_REG_BASE + (socket * W5100_SOCKET_REG_SIZE) + 0x0029)

// MR values.
#define W5100_MR_RST                    0x80        // Reset.
#define W5100_MR_PB                     0x10        // Ping block.
#define W5100_MR_PPPoE                  0x08        // Enable PPPoE.
#define W5100_MR_LB                     0x04        // Little or big endian selector in indirect mode.
#define W5100_MR_AI                     0x02        // Auto-increment in indirect mode.
#define W5100_MR_IND                    0x01        // Enable indirect mode.

// IR values.
#define W5100_IR_CONFLICT               0x80        // Check ip conflict.
#define W5100_IR_UNREACH                0x40        // Get the destination unreachable message in UDP sending.
#define W5100_IR_PPPoE                  0x20        // Get the PPPoE close message.
#define W5100_IR_SOCKET(socket)         (0x01 << socket) // Check socket interrupt.

#define W5100_BUFF_SIZE1                0x00        // 1 KB size.
#define W5100_BUFF_SIZE2                0x01        // 2 KB size.
#define W5100_BUFF_SIZE4                0x02        // 4 KB size.
#define W5100_BUFF_SIZE8                0x03        // 8 KB size.

#define W5100_BUFF_SIZE(socket, size)   (size << (socket * 2)) // Set size for socket in right place of byte.

// S_MR values.
#define W5100_S_MR_CLOSE                0x00        // Unused socket.
#define W5100_S_MR_TCP                  0x01        // TCP.
#define W5100_S_MR_UDP                  0x02        // UDP.
#define W5100_S_MR_IPRAW                0x03        // IP layer raw socket.
#define W5100_S_MR_MACRAW               0x04        // MAC layer raw socket (S0_MR support only).
#define W5100_S_MR_PPPOE                0x05        // PPPoE (S0_MR support only).
#define W5100_S_MR_ND                   0x20        // No Delayed Ack(TCP) flag.
#define W5100_S_MR_MULTI                0x80        // Support multi-casting flag.

// S_CR values.
#define W5100_S_CR_OPEN                 0x01        // Initialise or open socket.
#define W5100_S_CR_LISTEN               0x02        // Wait connection request in TCP mode(Server mode).
#define W5100_S_CR_CONNECT              0x04        // Send connection request in TCP mode(Client mode).
#define W5100_S_CR_DISCON               0x08        // Send closing request in TCP mode.
#define W5100_S_CR_CLOSE                0x10        // Close socket.
#define W5100_S_CR_SEND                 0x20        // Update TXBUFF pointer, send data.
#define W5100_S_CR_SEND_MAC             0x21        // Send data with MAC address, so without ARP process.
#define W5100_S_CR_SEND_KEEP            0x22        // Send keep alive message.
#define W5100_S_CR_RECV                 0x40        // Update RXBUFF pointer, receive data.

// S_IR values.
#define W5100_S_IR_SEND_OK              0x10        // Complete sending.
#define W5100_S_IR_TIMEOUT              0x08        // Assert timeout.
#define W5100_S_IR_RECV                 0x04        // Receiving data.
#define W5100_S_IR_DISCON               0x02        // Closed socket.
#define W5100_S_IR_CON                  0x01        // Established connection.

// S_SR values.
#define W5100_S_SR_CLOSED               0x00        // Closed.
#define W5100_S_SR_INIT                 0x13        // Init state.
#define W5100_S_SR_LISTEN               0x14        // Listen state.
#define W5100_S_SR_SYNSENT              0x15        // Connection state.
#define W5100_S_SR_SYNRECV              0x16        // Connection state.
#define W5100_S_SR_ESTABLISHED          0x17        // Success to connect.
#define W5100_S_SR_FIN_WAIT             0x18        // Closing state.
#define W5100_S_SR_CLOSING              0x1A        // Closing state.
#define W5100_S_SR_TIME_WAIT            0x1B        // Closing state.
#define W5100_S_SR_CLOSE_WAIT           0x1C        // Disconnection requested from peer host.
#define W5100_S_SR_LAST_ACK             0x1D        // Closing state.
#define W5100_S_SR_UDP                  0x22        // UDP socket.
#define W5100_S_SR_IPRAW                0x32        // IP raw mode socket.
#define W5100_S_SR_MACRAW               0x42        // MAC raw mode socket.
#define W5100_S_SR_PPPOE                0x5F        // PPPoE socket.
#define W5100_S_SR_ARP_TCP              0x11        // ARP socket.
#define W5100_S_SR_ARP_UDP              0x21        // ARP socket.
#define W5100_S_SR_ARP_ICMP             0x31        // ARP socket.


typedef struct
{    
    uint16_t TxBase;
    
    uint16_t TxSize;
    
    uint16_t RxBase;
    
    uint16_t RxSize;
}
W5100_MemoryInfoTypeDef;

typedef struct
{
    W5100_MemoryInfoTypeDef MemoryInfo[W5100_SOCKET_COUNT];
    
    WIZ_SocketInfoTypeDef   SocketInfo[W5100_SOCKET_COUNT];
}
W5100_ChipInfoTypeDef;


WIZ_ChipApiTypeDef _W5100_ChipApi; // Structure with chip API pointers. Common for all chip instances.


        NET_ErrorTypeDef W5100_Init                 (WIZ_HandleTypeDef *hWiz);
        NET_ErrorTypeDef W5100_DeInit               (WIZ_HandleTypeDef *hWiz);

        NET_ErrorTypeDef W5100_SetMemorySizes       (WIZ_HandleTypeDef *hWiz, WIZ_MemorySizeTypeDef *pTxSizes, WIZ_MemorySizeTypeDef *pRxSizes);
        NET_ErrorTypeDef W5100_SetMacAddress        (WIZ_HandleTypeDef *hWiz, uint8_t *pMacAddress);
        NET_ErrorTypeDef W5100_SetIPAddress         (WIZ_HandleTypeDef *hWiz, NET_IPAddressTypeDef ipAddress);
        NET_ErrorTypeDef W5100_SetSubnetMask        (WIZ_HandleTypeDef *hWiz, NET_IPAddressTypeDef subnetMask);
        NET_ErrorTypeDef W5100_SetDefaultGateway    (WIZ_HandleTypeDef *hWiz, NET_IPAddressTypeDef defaultGateway);

        NET_ErrorTypeDef W5100_SendLength           (WIZ_SocketHandleTypeDef *hSocket, uint16_t *pLength);
        NET_ErrorTypeDef W5100_SendData             (WIZ_SocketHandleTypeDef *hSocket, uint8_t *pBuffer, uint16_t sendLength);
        NET_ErrorTypeDef W5100_ReceiveLength        (WIZ_SocketHandleTypeDef *hSocket, uint16_t *pLength);
        NET_ErrorTypeDef W5100_ReceiveData          (WIZ_SocketHandleTypeDef *hSocket, uint8_t *pBuffer, uint16_t bufferLength, uint16_t *pReceivedLength);
        NET_ErrorTypeDef W5100_CloseSocket          (WIZ_SocketHandleTypeDef *hSocket);

        NET_ErrorTypeDef W5100_TCPOpen              (WIZ_SocketHandleTypeDef *hSocket, NET_PortTypeDef port);
        NET_ErrorTypeDef W5100_TCPListen            (WIZ_SocketHandleTypeDef *hSocket);
        NET_ErrorTypeDef W5100_TCPConnect           (WIZ_SocketHandleTypeDef *hSocket, NET_EndPointTypeDef *pDestination);
        NET_ErrorTypeDef W5100_TCPDisconnect        (WIZ_SocketHandleTypeDef *hSocket);

        NET_ErrorTypeDef W5100_UDPOpen              (WIZ_SocketHandleTypeDef *hSocket, NET_PortTypeDef port);
        NET_ErrorTypeDef W5100_UDPSendData          (WIZ_SocketHandleTypeDef *hSocket, uint8_t *pBuffer, uint16_t length, NET_EndPointTypeDef *pDestination);

        NET_ErrorTypeDef W5100_GetSocketState       (WIZ_SocketHandleTypeDef *hSocket, WIZ_SocketStateTypeDef *state);
        NET_ErrorTypeDef W5100_GetSocketFlags       (WIZ_SocketHandleTypeDef *hSocket, WIZ_SocketFlagTypeDef *pFlags, WIZ_SocketFlagTypeDef flagsMask);

        NET_ErrorTypeDef W5100_GetFlags             (WIZ_HandleTypeDef *hWiz, WIZ_FlagTypeDef *pFlags, WIZ_FlagTypeDef flagsMask);

        NET_ErrorTypeDef W5100_GetDestination       (WIZ_SocketHandleTypeDef *hSocket, NET_EndPointTypeDef *pDestination);
        
        WIZ_SocketInfoTypeDef * W5100_GetSocketInfo (WIZ_HandleTypeDef *hWiz);

static  NET_ErrorTypeDef W5100_Open                 (WIZ_SocketHandleTypeDef *hSocket, NET_PortTypeDef port);
static  NET_ErrorTypeDef W5100_WriteByte            (WIZ_HandleTypeDef *hWiz, uint16_t address, uint8_t data);
static  NET_ErrorTypeDef W5100_ReadByte             (WIZ_HandleTypeDef *hWiz, uint16_t address, uint8_t *pData);
static  NET_ErrorTypeDef W5100_TakeControl          (WIZ_HandleTypeDef *hWiz);
static  NET_ErrorTypeDef W5100_ReleaseControl       (WIZ_HandleTypeDef *hWiz);


// Returns pointer on chip API struct.
void W5100_GetChipApi(WIZ_ChipApiTypeDef **hApi)
{
    NET_OS_CRITICAL()
    {
        if (!_W5100_ChipApi.__IsFilled)
        {
            _W5100_ChipApi.WIZ_CHIP_API_Init                = W5100_Init;
            _W5100_ChipApi.WIZ_CHIP_API_DeInit              = W5100_DeInit;
            
            _W5100_ChipApi.WIZ_CHIP_API_SetMemorySizes      = W5100_SetMemorySizes;
            _W5100_ChipApi.WIZ_CHIP_API_SetMacAddress       = W5100_SetMacAddress;
            _W5100_ChipApi.WIZ_CHIP_API_SetIPAddress        = W5100_SetIPAddress;
            _W5100_ChipApi.WIZ_CHIP_API_SetSubnetMask       = W5100_SetSubnetMask;
            _W5100_ChipApi.WIZ_CHIP_API_SetDefaultGateway   = W5100_SetDefaultGateway;
            
            _W5100_ChipApi.WIZ_CHIP_API_SendLength          = W5100_SendLength;
            _W5100_ChipApi.WIZ_CHIP_API_SendData            = W5100_SendData;
            _W5100_ChipApi.WIZ_CHIP_API_ReceiveLength       = W5100_ReceiveLength;
            _W5100_ChipApi.WIZ_CHIP_API_ReceiveData         = W5100_ReceiveData;
            _W5100_ChipApi.WIZ_CHIP_API_CloseSocket         = W5100_CloseSocket;
            
            _W5100_ChipApi.WIZ_CHIP_API_TCPOpen             = W5100_TCPOpen;
            _W5100_ChipApi.WIZ_CHIP_API_TCPListen           = W5100_TCPListen;
            _W5100_ChipApi.WIZ_CHIP_API_TCPConnect          = W5100_TCPConnect;
            _W5100_ChipApi.WIZ_CHIP_API_TCPDisconnect       = W5100_TCPDisconnect;
            
            _W5100_ChipApi.WIZ_CHIP_API_UDPOpen             = W5100_UDPOpen;
            _W5100_ChipApi.WIZ_CHIP_API_UDPSendData         = W5100_UDPSendData;
            
            _W5100_ChipApi.WIZ_CHIP_API_GetSocketState      = W5100_GetSocketState;
            _W5100_ChipApi.WIZ_CHIP_API_GetSocketFlags      = W5100_GetSocketFlags;
            
            _W5100_ChipApi.WIZ_CHIP_API_GetFlags            = W5100_GetFlags;
            
            _W5100_ChipApi.WIZ_CHIP_API_GetDestination      = W5100_GetDestination;
            
            _W5100_ChipApi.WIZ_CHIP_API_GetSocketInfo       = W5100_GetSocketInfo;
            
            _W5100_ChipApi.SocketCount                      = W5100_SOCKET_COUNT;
            _W5100_ChipApi.ChipType                         = WIZ_CHIP_5100;
            
            _W5100_ChipApi.__IsFilled                       = true;
        }
    }

    *hApi = &_W5100_ChipApi;
}

// Init W5100.
NET_ErrorTypeDef W5100_Init(WIZ_HandleTypeDef *hWiz)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK; 
    
    
    if (hWiz->IsInitialized)
    {
        return WIZ_ERR_WRONGOPERATION;
    }
    
    hWiz->IsInitialized = true;
    
    
    if (hWiz->Hal.WIZ_HAL_TransmitReceiveSync   == NULL ||
        hWiz->Hal.WIZ_HAL_TakeMutex             == NULL ||
        hWiz->Hal.WIZ_HAL_ReleaseMutex          == NULL ||
        hWiz->Hal.WIZ_HAL_ActivateNss           == NULL ||
        hWiz->Hal.WIZ_HAL_DeactivateNss         == NULL ||
        hWiz->Hal.WIZ_HAL_RunChip               == NULL ||
        hWiz->Hal.WIZ_HAL_ResetChip             == NULL )
    {
        return WIZ_ERR_NULLPTR;
    }
    
    
    hWiz->Hal.WIZ_HAL_RunChip();
    
    if ((error = W5100_WriteByte(hWiz, W5100_MR, W5100_MR_RST)) != WIZ_ERR_OK) return error; // Soft reset.
    
    
    hWiz->ChipInfo = (W5100_ChipInfoTypeDef *)net_os_malloc(sizeof(W5100_ChipInfoTypeDef));
    
    if (!hWiz->ChipInfo) return WIZ_ERR_MEM_ALLOCATION;
    
    
    return error;
}

// DeInit W5100.
NET_ErrorTypeDef W5100_DeInit(WIZ_HandleTypeDef *hWiz)
{
    if (!(hWiz->IsInitialized))
    {
        return WIZ_ERR_WRONGOPERATION;
    }
    
    hWiz->IsInitialized = false;
    
    
    hWiz->Hal.WIZ_HAL_ResetChip();
    
    
    net_os_free(hWiz->ChipInfo);
    
    
    return WIZ_ERR_OK;
}

// Init memory sizes of W5100.
NET_ErrorTypeDef W5100_SetMemorySizes(WIZ_HandleTypeDef *hWiz, WIZ_MemorySizeTypeDef *pTxSizes, WIZ_MemorySizeTypeDef *pRxSizes)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    WIZ_MemorySizeTypeDef txTotalMemSize = WIZ_MEMSIZE_0;
    WIZ_MemorySizeTypeDef rxTotalMemSize = WIZ_MEMSIZE_0;
    
    W5100_MemoryInfoTypeDef previousInfo;
    previousInfo.TxBase = W5100_TX_MEMORY_BASE;
    previousInfo.RxBase = W5100_RX_MEMORY_BASE;
    previousInfo.TxSize = 0;
    previousInfo.RxSize = 0;
    
    uint8_t txBufferRegister = 0;
    uint8_t rxBufferRegister = 0;
    
    
    W5100_MemoryInfoTypeDef *pMemoryInfo = ((W5100_ChipInfoTypeDef *)hWiz->ChipInfo)->MemoryInfo;
    
    
    for (WIZ_SocketNumberTypeDef socket = WIZ_SOCKET_0; socket < W5100_SOCKET_COUNT; socket++)
    {
        txTotalMemSize += pTxSizes[socket];
        rxTotalMemSize += pRxSizes[socket];
        
        WIZ_MemorySizeTypeDef socketMemoryValues[] = { pTxSizes[socket], pRxSizes[socket] };
        uint16_t realSizes[] = { 0, 0 };
        uint8_t socketSizes[] = { 0, 0 };
        
        for (uint8_t i = 0; i < sizeof(socketMemoryValues); i++)
        {        
            switch (socketMemoryValues[i])
            {
                case WIZ_MEMSIZE_0:
                    realSizes[i] = W5100_MEMORY_0KB;
                    socketSizes[i] = W5100_BUFF_SIZE1;
                    break;
                
                case WIZ_MEMSIZE_1:
                    realSizes[i] = W5100_MEMORY_1KB;
                    socketSizes[i] = W5100_BUFF_SIZE1;
                    break;
                
                case WIZ_MEMSIZE_2:
                    realSizes[i] = W5100_MEMORY_1KB * 2;
                    socketSizes[i] = W5100_BUFF_SIZE2;
                    break;
                
                case WIZ_MEMSIZE_4:
                    realSizes[i] = W5100_MEMORY_1KB * 4;
                    socketSizes[i] = W5100_BUFF_SIZE4;
                    break;
                
                case WIZ_MEMSIZE_8:
                    realSizes[i] = W5100_MEMORY_1KB * 8;
                    socketSizes[i] = W5100_BUFF_SIZE8;
                    break;
                
                default: return WIZ_ERR_WRONGSIZE;
            }
        }
        
        pMemoryInfo[socket].TxBase = previousInfo.TxBase + previousInfo.TxSize;
        pMemoryInfo[socket].RxBase = previousInfo.RxBase + previousInfo.RxSize;
        pMemoryInfo[socket].TxSize = realSizes[0];
        pMemoryInfo[socket].RxSize = realSizes[1];
        
        previousInfo.TxBase = pMemoryInfo[socket].TxBase;
        previousInfo.RxBase = pMemoryInfo[socket].RxBase;
        previousInfo.TxSize = realSizes[0];
        previousInfo.RxSize = realSizes[1];
        
        txBufferRegister |= W5100_BUFF_SIZE(socket, socketSizes[0]);
        rxBufferRegister |= W5100_BUFF_SIZE(socket, socketSizes[1]);
    }
    
    if (txTotalMemSize != WIZ_MEMSIZE_8 || rxTotalMemSize != WIZ_MEMSIZE_8) return WIZ_ERR_WRONGSIZE;
    
    
    if ((error = hWiz->Hal.WIZ_HAL_TakeMutex()) != WIZ_ERR_OK) return error;
    
    if ((error = W5100_WriteByte(hWiz, W5100_TMSR, txBufferRegister)) == WIZ_ERR_OK) // Memory settings
    {
        error = W5100_WriteByte(hWiz, W5100_RMSR, rxBufferRegister);
    }
    
    NET_ErrorTypeDef releaseError = hWiz->Hal.WIZ_HAL_ReleaseMutex();
    
    
    return releaseError ? releaseError : error;
}

// Init Mac address of W5100.
NET_ErrorTypeDef W5100_SetMacAddress(WIZ_HandleTypeDef *hWiz, uint8_t *pMacAddress)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    if ((error = hWiz->Hal.WIZ_HAL_TakeMutex()) != WIZ_ERR_OK) return error;    
    
    if ((error = W5100_WriteByte(hWiz, W5100_SHAR0, pMacAddress[5])) == WIZ_ERR_OK) // Mac settings.
    {
        if ((error = W5100_WriteByte(hWiz, W5100_SHAR1, pMacAddress[4])) == WIZ_ERR_OK)
        {
            if ((error = W5100_WriteByte(hWiz, W5100_SHAR2, pMacAddress[3])) == WIZ_ERR_OK)
            {
                if ((error = W5100_WriteByte(hWiz, W5100_SHAR3, pMacAddress[2])) == WIZ_ERR_OK)
                {
                    if ((error = W5100_WriteByte(hWiz, W5100_SHAR4, pMacAddress[1])) == WIZ_ERR_OK)
                    {
                        error = W5100_WriteByte(hWiz, W5100_SHAR5, pMacAddress[0]);
                    }
                }
            }
        }
    }
    
    NET_ErrorTypeDef releaseError = hWiz->Hal.WIZ_HAL_ReleaseMutex();
    
    
    return releaseError ? releaseError : error;
}

// Init IP address of W5100.
NET_ErrorTypeDef W5100_SetIPAddress(WIZ_HandleTypeDef *hWiz, NET_IPAddressTypeDef ipAddress)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    if ((error = hWiz->Hal.WIZ_HAL_TakeMutex()) != WIZ_ERR_OK) return error;
    
    if ((error = W5100_WriteByte(hWiz, W5100_SIPR0, ipAddress.Byte3.Value)) == WIZ_ERR_OK) // Ip address settings.
    {
        if ((error = W5100_WriteByte(hWiz, W5100_SIPR1, ipAddress.Byte2.Value)) == WIZ_ERR_OK)
        {
            if ((error = W5100_WriteByte(hWiz, W5100_SIPR2, ipAddress.Byte1.Value)) == WIZ_ERR_OK)
            {
                error = W5100_WriteByte(hWiz, W5100_SIPR3, ipAddress.Byte0.Value);
            }
        }
    }
    
    NET_ErrorTypeDef releaseError = hWiz->Hal.WIZ_HAL_ReleaseMutex();
    
    
    return releaseError ? releaseError : error;
}

// Init subnet mask of W5100.
NET_ErrorTypeDef W5100_SetSubnetMask(WIZ_HandleTypeDef *hWiz, NET_IPAddressTypeDef subnetMask)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    if ((error = hWiz->Hal.WIZ_HAL_TakeMutex()) != WIZ_ERR_OK) return error;
    
    if ((error = W5100_WriteByte(hWiz, W5100_SUBR0, subnetMask.Byte3.Value)) == WIZ_ERR_OK) // Subnet mask settings.
    {
        if ((error = W5100_WriteByte(hWiz, W5100_SUBR1, subnetMask.Byte2.Value)) == WIZ_ERR_OK)
        {
            if ((error = W5100_WriteByte(hWiz, W5100_SUBR2, subnetMask.Byte1.Value)) == WIZ_ERR_OK)
            {
                error = W5100_WriteByte(hWiz, W5100_SUBR3, subnetMask.Byte0.Value);
            }
        }
    }
    
    NET_ErrorTypeDef releaseError = hWiz->Hal.WIZ_HAL_ReleaseMutex();
    
    
    return releaseError ? releaseError : error;
}

// Init default gateway of W5100.
NET_ErrorTypeDef W5100_SetDefaultGateway(WIZ_HandleTypeDef *hWiz, NET_IPAddressTypeDef defaultGateway)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    if ((error = hWiz->Hal.WIZ_HAL_TakeMutex()) != WIZ_ERR_OK) return error;
    
    if ((error = W5100_WriteByte(hWiz, W5100_GAR0, defaultGateway.Byte3.Value)) == WIZ_ERR_OK) // Gateway settings.
    {
        if ((error = W5100_WriteByte(hWiz, W5100_GAR1, defaultGateway.Byte2.Value)) == WIZ_ERR_OK)
        {
            if ((error = W5100_WriteByte(hWiz, W5100_GAR2, defaultGateway.Byte1.Value)) == WIZ_ERR_OK)
            {
                error = W5100_WriteByte(hWiz, W5100_GAR3, defaultGateway.Byte0.Value);
            }
        }
    }
    
    NET_ErrorTypeDef releaseError = hWiz->Hal.WIZ_HAL_ReleaseMutex();
    
    
    return releaseError ? releaseError : error;
}

// Get free size for new data for send in wizchip buffer.
NET_ErrorTypeDef W5100_SendLength(WIZ_SocketHandleTypeDef *hSocket, uint16_t *pLength)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    NET_Union16TypeDef *freeSize = (NET_Union16TypeDef *)pLength;
    
    
    if ((error = hSocket->WizHandle->Hal.WIZ_HAL_TakeMutex()) != WIZ_ERR_OK) return error;
    
    if ((error = W5100_ReadByte(hSocket->WizHandle, W5100_S_TX_FSR0(hSocket->Number), &(freeSize->Byte1.Value))) == WIZ_ERR_OK)
    {
        error = W5100_ReadByte(hSocket->WizHandle, W5100_S_TX_FSR1(hSocket->Number), &(freeSize->Byte0.Value));
    }
    
    NET_ErrorTypeDef releaseError = hSocket->WizHandle->Hal.WIZ_HAL_ReleaseMutex();
    
    
    return releaseError ? releaseError : error;
}

// Send data.
NET_ErrorTypeDef W5100_SendData(WIZ_SocketHandleTypeDef *hSocket, uint8_t *pBuffer, uint16_t sendLength)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    // Check free size.
    uint16_t freeSize = 0;
    
    if ((error = W5100_SendLength(hSocket, &freeSize)) != WIZ_ERR_OK) return error;
    
    if (freeSize < sendLength)
    {
        #ifdef WIZ_DEBUG
        
        net_os_printf("W5100: not enough memory to send: socket:%u length:%u free size:%u\r\n", hSocket->Number, sendLength, freeSize);
        
        #endif
        
        return WIZ_ERR_TX_FULL;
    }
    
    #ifdef WIZ_DEBUG
    
    net_os_printf("W5100: send data: socket:%u length:%u free size:%u\r\n", hSocket->Number, sendLength, freeSize);
    
    #endif
    
    
    // Compute send pointers.
    NET_Union16TypeDef txOffsetRaw;
    
    
    if ((error = hSocket->WizHandle->Hal.WIZ_HAL_TakeMutex()) != WIZ_ERR_OK) return error;
    
    if ((error = W5100_ReadByte(hSocket->WizHandle, W5100_S_TX_RD0(hSocket->Number), &txOffsetRaw.Byte1.Value)) == WIZ_ERR_OK)
    {
        error = W5100_ReadByte(hSocket->WizHandle, W5100_S_TX_RD1(hSocket->Number), &txOffsetRaw.Byte0.Value);
    }
    
    NET_ErrorTypeDef releaseError = hSocket->WizHandle->Hal.WIZ_HAL_ReleaseMutex();
    
    if (releaseError) return releaseError;
    if (error) return error;
    
    
    W5100_MemoryInfoTypeDef *pMemoryInfo = ((W5100_ChipInfoTypeDef *)hSocket->WizHandle->ChipInfo)->MemoryInfo;
    
    uint16_t txBase = pMemoryInfo[hSocket->Number].TxBase;
    uint16_t txMask = pMemoryInfo[hSocket->Number].TxSize - 1;
    
    uint16_t txOffset = txOffsetRaw.Value & txMask;
    uint16_t txStart = txOffset + txBase;
    
    
    // Write data for send.
    for (uint16_t i = 0; i < sendLength; i++)
    {
        if (txStart > (txBase + txMask)) txStart = txBase;
        
        if ((error = W5100_WriteByte(hSocket->WizHandle, txStart++, pBuffer[i])) != WIZ_ERR_OK) return error;
        
        #ifdef WIZ_DEBUG_DATA
    
        net_os_printf("%02X ", pBuffer[i]);
    
        #endif
    }
    
    #ifdef WIZ_DEBUG_DATA
    
    net_os_puts("");
    
    #endif
    
    
    NET_Union16TypeDef txEnd;
    
    
    if ((error = hSocket->WizHandle->Hal.WIZ_HAL_TakeMutex()) != WIZ_ERR_OK) return error;
    
    if ((error = W5100_ReadByte(hSocket->WizHandle, W5100_S_TX_WR0(hSocket->Number), &txEnd.Byte1.Value)) == WIZ_ERR_OK)
    {
        error = W5100_ReadByte(hSocket->WizHandle, W5100_S_TX_WR1(hSocket->Number), &txEnd.Byte0.Value);
    }
    
    releaseError = hSocket->WizHandle->Hal.WIZ_HAL_ReleaseMutex();
    
    if (releaseError) return releaseError;
    if (error) return error;
    
    
    txEnd.Value += sendLength;
    
    
    if ((error = hSocket->WizHandle->Hal.WIZ_HAL_TakeMutex()) != WIZ_ERR_OK) return error;
    
    if ((error = W5100_WriteByte(hSocket->WizHandle, W5100_S_TX_WR0(hSocket->Number), txEnd.Byte1.Value)) == WIZ_ERR_OK)
    {
        error = W5100_WriteByte(hSocket->WizHandle, W5100_S_TX_WR1(hSocket->Number), txEnd.Byte0.Value);
    }
    
    releaseError = hSocket->WizHandle->Hal.WIZ_HAL_ReleaseMutex();
    
    if (releaseError) return releaseError;
    if (error) return error;
    
    
    // Send cmd.
    if ((error = W5100_WriteByte(hSocket->WizHandle, W5100_S_CR(hSocket->Number), W5100_S_CR_SEND)) != WIZ_ERR_OK) return error;
    
    
    #ifdef WIZ_DEBUG
    
    net_os_puts("W5100: send data ok");
    
    #endif
    
    return WIZ_ERR_OK;
}

// Get length of received data in wizchip buffer.
NET_ErrorTypeDef W5100_ReceiveLength(WIZ_SocketHandleTypeDef *hSocket, uint16_t *pLength)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    NET_Union16TypeDef *receivedLength = (NET_Union16TypeDef *)pLength;    
    
    
    if ((error = hSocket->WizHandle->Hal.WIZ_HAL_TakeMutex()) != WIZ_ERR_OK) return error;
    
    if ((error = W5100_ReadByte(hSocket->WizHandle, W5100_S_RX_RSR0(hSocket->Number), &(receivedLength->Byte1.Value))) == WIZ_ERR_OK)
    {
        error = W5100_ReadByte(hSocket->WizHandle, W5100_S_RX_RSR1(hSocket->Number), &(receivedLength->Byte0.Value));
    }
    
    NET_ErrorTypeDef releaseError = hSocket->WizHandle->Hal.WIZ_HAL_ReleaseMutex();
    
    
    return releaseError ? releaseError : error;
}

// Receive data.
NET_ErrorTypeDef W5100_ReceiveData(WIZ_SocketHandleTypeDef *hSocket, uint8_t *pBuffer, uint16_t bufferLength, uint16_t *pReceivedLength)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    // Read length of received data.
    uint16_t availableLength = 0;
    
    if ((error = W5100_ReceiveLength(hSocket, &availableLength)) != WIZ_ERR_OK) return error;
    
    
    *pReceivedLength = availableLength > bufferLength ? bufferLength : availableLength;    
    
    if (*pReceivedLength == 0)
    {
        #ifdef WIZ_DEBUG
        
        net_os_printf("W5100: receive buffer is empty: socket:%u\r\n", hSocket->Number);
        
        #endif
        
        return WIZ_ERR_RX_EMPTY;
    }    
    
    #ifdef WIZ_DEBUG
    
    net_os_printf("W5100: receive data: socket:%u length: %u of %u\r\n", hSocket->Number, *pReceivedLength, availableLength);
    
    #endif
    
    
    // Compute receive pointers.
    NET_Union16TypeDef rxOffsetRaw;
    
    
    if ((error = hSocket->WizHandle->Hal.WIZ_HAL_TakeMutex()) != WIZ_ERR_OK) return error;
    
    if ((error = W5100_ReadByte(hSocket->WizHandle, W5100_S_RX_RD0(hSocket->Number), &rxOffsetRaw.Byte1.Value)) == WIZ_ERR_OK)
    {
        error = W5100_ReadByte(hSocket->WizHandle, W5100_S_RX_RD1(hSocket->Number), &rxOffsetRaw.Byte0.Value);
    }
    
    NET_ErrorTypeDef releaseError = hSocket->WizHandle->Hal.WIZ_HAL_ReleaseMutex();
    
    if (releaseError) return releaseError;
    if (error) return error;

    
    W5100_MemoryInfoTypeDef *pMemoryInfo = ((W5100_ChipInfoTypeDef *)hSocket->WizHandle->ChipInfo)->MemoryInfo;
    
    uint16_t rxBase = pMemoryInfo[hSocket->Number].RxBase;
    uint16_t rxMask = pMemoryInfo[hSocket->Number].RxSize - 1;
    
    uint16_t rxOffset = rxOffsetRaw.Value & rxMask;
    uint16_t rxStart = rxOffset + rxBase;    
    
    
    // Read received data.
    for (uint16_t i = 0; i < *pReceivedLength; i++)
    {
        if (rxStart > (rxBase + rxMask)) rxStart = rxBase;
        
        if ((error = W5100_ReadByte(hSocket->WizHandle, rxStart++, (pBuffer + i))) != WIZ_ERR_OK) return error;
        
        #ifdef WIZ_DEBUG_DATA
    
        net_os_printf("%02X ", pBuffer[i]);
    
        #endif
    }
    
    #ifdef WIZ_DEBUG_DATA
    
    net_os_puts("");
    
    #endif
    
    
    rxOffsetRaw.Value += *pReceivedLength;
    
    
    if ((error = hSocket->WizHandle->Hal.WIZ_HAL_TakeMutex()) != WIZ_ERR_OK) return error;
    
    if ((error = W5100_WriteByte(hSocket->WizHandle, W5100_S_RX_RD0(hSocket->Number), rxOffsetRaw.Byte1.Value)) == WIZ_ERR_OK)
    {
        error = W5100_WriteByte(hSocket->WizHandle, W5100_S_RX_RD1(hSocket->Number), rxOffsetRaw.Byte0.Value);
    }
    
    releaseError = hSocket->WizHandle->Hal.WIZ_HAL_ReleaseMutex();
    
    if (releaseError) return releaseError;
    if (error) return error;
    
    
    // Receive cmd.
    if ((error = W5100_WriteByte(hSocket->WizHandle, W5100_S_CR(hSocket->Number), W5100_S_CR_RECV)) != WIZ_ERR_OK) return error;
    
    
    #ifdef WIZ_DEBUG
    
    net_os_puts("W5100: receive data ok");
    
    #endif
    
    return WIZ_ERR_OK;
}

// Close socket.
NET_ErrorTypeDef W5100_CloseSocket(WIZ_SocketHandleTypeDef *hSocket)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    if ((error = W5100_WriteByte(hSocket->WizHandle, W5100_S_CR(hSocket->Number), W5100_S_CR_CLOSE)) != WIZ_ERR_OK) return error;
    
    
    // Clear all flags after close.
    WIZ_SocketFlagTypeDef flags = WIZ_SOCKET_FLAG_NONE;
    
    if ((error = W5100_GetSocketFlags(hSocket, &flags, WIZ_SOCKET_FLAG_ALL)) != WIZ_ERR_OK) return error;
    
    
    #ifdef WIZ_DEBUG
    
    net_os_printf("W5100: close socket: %u\r\n", hSocket->Number);
    
    #endif
    
    return error;
}

// Open in TCP mode.
NET_ErrorTypeDef W5100_TCPOpen(WIZ_SocketHandleTypeDef *hSocket, NET_PortTypeDef port)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    #ifdef WIZ_DEBUG
    
    net_os_printf("W5100: TCP open: socket:%u port:%u\r\n", hSocket->Number, port.Value);
    
    #endif
    
    
    WIZ_RememberPort(hSocket, port, NET_PORTMODE_TCP);
    
    
    if ((error = W5100_WriteByte(hSocket->WizHandle, W5100_S_MR(hSocket->Number), W5100_S_MR_TCP)) != WIZ_ERR_OK) return error;
    
    
    if ((error = W5100_Open(hSocket, port)) != WIZ_ERR_OK) return error; 
    
    
    uint8_t status = 0;
    
    if ((error = W5100_ReadByte(hSocket->WizHandle, W5100_S_SR(hSocket->Number), &status)) != WIZ_ERR_OK) return error;
    
    #ifdef WIZ_DEBUG
    
    if (status == W5100_S_SR_INIT) net_os_puts("W5100: TCP open ok");
    
    #endif
    
    return (status == W5100_S_SR_INIT) ? WIZ_ERR_OK : WIZ_ERR_SOCKET_OPENING;
}

// Begin TCP listening.
NET_ErrorTypeDef W5100_TCPListen(WIZ_SocketHandleTypeDef *hSocket)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    if ((error = W5100_WriteByte(hSocket->WizHandle, W5100_S_CR(hSocket->Number), W5100_S_CR_LISTEN)) != WIZ_ERR_OK) return error;
    
    
    uint8_t status = 0;
    
    if ((error = W5100_ReadByte(hSocket->WizHandle, W5100_S_SR(hSocket->Number), &status)) != WIZ_ERR_OK) return error;
    
    return (status == W5100_S_SR_LISTEN) ? WIZ_ERR_OK : WIZ_ERR_SOCKET_LISTENING;
}

// Begin TCP connection.
NET_ErrorTypeDef W5100_TCPConnect(WIZ_SocketHandleTypeDef *hSocket, NET_EndPointTypeDef *pDestination)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    if ((error = hSocket->WizHandle->Hal.WIZ_HAL_TakeMutex()) != WIZ_ERR_OK) return error;
    
    if ((error = W5100_WriteByte(hSocket->WizHandle, W5100_S_DIPR0(hSocket->Number), pDestination->IPAddress.Byte3.Value)) == WIZ_ERR_OK)
    {
        if ((error = W5100_WriteByte(hSocket->WizHandle, W5100_S_DIPR1(hSocket->Number), pDestination->IPAddress.Byte2.Value)) == WIZ_ERR_OK)
        {
            if ((error = W5100_WriteByte(hSocket->WizHandle, W5100_S_DIPR2(hSocket->Number), pDestination->IPAddress.Byte1.Value)) == WIZ_ERR_OK)
            {
                if ((error = W5100_WriteByte(hSocket->WizHandle, W5100_S_DIPR3(hSocket->Number), pDestination->IPAddress.Byte0.Value)) == WIZ_ERR_OK)
                {
                    
                    if ((error = W5100_WriteByte(hSocket->WizHandle, W5100_S_DPORT0(hSocket->Number), pDestination->Port.Byte1.Value)) == WIZ_ERR_OK)
                    {
                        error = W5100_WriteByte(hSocket->WizHandle, W5100_S_DPORT1(hSocket->Number), pDestination->Port.Byte0.Value);
                    }
                }
            }
        }
    }
    
    NET_ErrorTypeDef releaseError = hSocket->WizHandle->Hal.WIZ_HAL_ReleaseMutex();
    
    if (releaseError) return releaseError;
    if (error) return error;
    
    
    if ((error = W5100_WriteByte(hSocket->WizHandle, W5100_S_CR(hSocket->Number), W5100_S_CR_CONNECT)) != WIZ_ERR_OK) return error;
    
    return WIZ_ERR_OK;
}

// Begin TCP disconnection.
NET_ErrorTypeDef W5100_TCPDisconnect(WIZ_SocketHandleTypeDef *hSocket)
{
    #ifdef WIZ_DEBUG
    
    net_os_printf("W5100: TCP disconnection: socket:%u\r\n", hSocket->Number);
    
    #endif
    
    
    return W5100_WriteByte(hSocket->WizHandle, W5100_S_CR(hSocket->Number), W5100_S_CR_DISCON);
}

// Open in UDP mode.
NET_ErrorTypeDef W5100_UDPOpen(WIZ_SocketHandleTypeDef *hSocket, NET_PortTypeDef port)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    #ifdef WIZ_DEBUG
    
    net_os_printf("W5100: UDP open: socket:%u port:%u\r\n", hSocket->Number, port.Value);
    
    #endif
    
    
    WIZ_RememberPort(hSocket, port, NET_PORTMODE_UDP);
    
    
    if ((error = W5100_WriteByte(hSocket->WizHandle, W5100_S_MR(hSocket->Number), W5100_S_MR_UDP)) != WIZ_ERR_OK) return error;
    
    
    if ((error = W5100_Open(hSocket, port)) != WIZ_ERR_OK) return error;
    
    
    uint8_t status = 0;    
    
    if ((error = W5100_ReadByte(hSocket->WizHandle, W5100_S_SR(hSocket->Number), &status)) != WIZ_ERR_OK) return error;
    
    #ifdef WIZ_DEBUG
    
    if (status == W5100_S_SR_UDP) net_os_puts("W5100: UDP open ok");
    
    #endif
    
    return (status == W5100_S_SR_UDP) ? WIZ_ERR_OK : WIZ_ERR_SOCKET_OPENING;
}

// Send data in UDP mode.
NET_ErrorTypeDef W5100_UDPSendData(WIZ_SocketHandleTypeDef *hSocket, uint8_t *pBuffer, uint16_t length, NET_EndPointTypeDef *pDestination)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    #ifdef WIZ_DEBUG
    
    net_os_printf("W5100: UDP send: socket:%u dst:%u.%u.%u.%u:%u\r\n", hSocket->Number,
        pDestination->IPAddress.Byte3.Value, pDestination->IPAddress.Byte2.Value, pDestination->IPAddress.Byte1.Value, pDestination->IPAddress.Byte0.Value, pDestination->Port.Value);
    
    #endif
    
    
    if ((error = hSocket->WizHandle->Hal.WIZ_HAL_TakeMutex()) != WIZ_ERR_OK) return error;
    
    if ((error = W5100_WriteByte(hSocket->WizHandle, W5100_S_DIPR0(hSocket->Number), pDestination->IPAddress.Byte3.Value)) == WIZ_ERR_OK)
    {
        if ((error = W5100_WriteByte(hSocket->WizHandle, W5100_S_DIPR1(hSocket->Number), pDestination->IPAddress.Byte2.Value)) == WIZ_ERR_OK)
        {
            if ((error = W5100_WriteByte(hSocket->WizHandle, W5100_S_DIPR2(hSocket->Number), pDestination->IPAddress.Byte1.Value)) == WIZ_ERR_OK)
            {
                if ((error = W5100_WriteByte(hSocket->WizHandle, W5100_S_DIPR3(hSocket->Number), pDestination->IPAddress.Byte0.Value)) == WIZ_ERR_OK)
                {
                    
                    if ((error = W5100_WriteByte(hSocket->WizHandle, W5100_S_DPORT0(hSocket->Number), pDestination->Port.Byte1.Value)) == WIZ_ERR_OK)
                    {
                        error = W5100_WriteByte(hSocket->WizHandle, W5100_S_DPORT1(hSocket->Number), pDestination->Port.Byte0.Value);
                    }
                }
            }
        }
    }
    
    NET_ErrorTypeDef releaseError = hSocket->WizHandle->Hal.WIZ_HAL_ReleaseMutex();
    
    if (releaseError) return releaseError;
    if (error) return error;
    
    
    if ((error = W5100_SendData(hSocket, pBuffer, length)) != WIZ_ERR_OK) return error;
    
    
    return WIZ_ERR_OK;
}

// Get status.
NET_ErrorTypeDef W5100_GetSocketState(WIZ_SocketHandleTypeDef *hSocket, WIZ_SocketStateTypeDef *pState)
{
    return W5100_ReadByte(hSocket->WizHandle, W5100_S_SR(hSocket->Number), (uint8_t *)pState);
}

// Get socket flags for specified mask.
NET_ErrorTypeDef W5100_GetSocketFlags(WIZ_SocketHandleTypeDef *hSocket, WIZ_SocketFlagTypeDef *pFlags, WIZ_SocketFlagTypeDef flagsMask)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    if (flagsMask)
    {
        if ((error = hSocket->WizHandle->Hal.WIZ_HAL_TakeMutex()) != WIZ_ERR_OK) return error;
    
        if ((error = W5100_ReadByte(hSocket->WizHandle, W5100_S_IR(hSocket->Number), (uint8_t *)pFlags)) == WIZ_ERR_OK)
        {
            *pFlags &= flagsMask;
            
            error = W5100_WriteByte(hSocket->WizHandle, W5100_S_IR(hSocket->Number), *((uint8_t *)pFlags));
        }
        
        NET_ErrorTypeDef releaseError = hSocket->WizHandle->Hal.WIZ_HAL_ReleaseMutex();
        
        if (releaseError) return releaseError;
        if (error) return error;
    }
    else
    {
        *pFlags = WIZ_SOCKET_FLAG_NONE;
    }
    
    
    return WIZ_ERR_OK;
}

// Get chip flags for specified mask.
NET_ErrorTypeDef W5100_GetFlags(WIZ_HandleTypeDef *hWiz, WIZ_FlagTypeDef *pFlags, WIZ_FlagTypeDef flagsMask)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    if (flagsMask)
    {    
        uint8_t flagsRaw = 0;
        uint8_t maskRaw = ((flagsMask & 0xF000) >> 8) + (flagsMask & 0x000F);
        
        
        if ((error = hWiz->Hal.WIZ_HAL_TakeMutex()) != WIZ_ERR_OK) return error;
    
        if ((error = W5100_ReadByte(hWiz, W5100_IR, &flagsRaw)) == WIZ_ERR_OK)
        {
            uint8_t flagsForClear = flagsRaw & (maskRaw & 0xF0); // Only upper bits must be cleared.
            
            if (flagsForClear)
            {
                error = W5100_WriteByte(hWiz, W5100_IR, flagsForClear);
            }
        }
        
        NET_ErrorTypeDef releaseError = hWiz->Hal.WIZ_HAL_ReleaseMutex();
        
        if (releaseError) return releaseError;
        if (error) return error;
                
        
        *pFlags = (WIZ_FlagTypeDef)(((flagsRaw & 0xF0) << 8) + (flagsRaw & 0x0F));
    }
    else
    {
        *pFlags = WIZ_FLAG_NONE;
    }
    
    
    return WIZ_ERR_OK;
}

// Gets destination end point.
NET_ErrorTypeDef W5100_GetDestination(WIZ_SocketHandleTypeDef *hSocket, NET_EndPointTypeDef *pDestination)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    if ((error = hSocket->WizHandle->Hal.WIZ_HAL_TakeMutex()) != WIZ_ERR_OK) return error;
    
    if ((error = W5100_ReadByte(hSocket->WizHandle, W5100_S_DIPR0(hSocket->Number), &(pDestination->IPAddress.Byte3.Value))) == WIZ_ERR_OK)
    {
        if ((error = W5100_ReadByte(hSocket->WizHandle, W5100_S_DIPR1(hSocket->Number), &(pDestination->IPAddress.Byte2.Value))) == WIZ_ERR_OK)
        {
            if ((error = W5100_ReadByte(hSocket->WizHandle, W5100_S_DIPR2(hSocket->Number), &(pDestination->IPAddress.Byte1.Value))) == WIZ_ERR_OK)
            {
                if ((error = W5100_ReadByte(hSocket->WizHandle, W5100_S_DIPR3(hSocket->Number), &(pDestination->IPAddress.Byte0.Value))) == WIZ_ERR_OK)
                {
                    
                    if ((error = W5100_ReadByte(hSocket->WizHandle, W5100_S_DPORT0(hSocket->Number), &(pDestination->Port.Byte1.Value))) == WIZ_ERR_OK)
                    {
                        error = W5100_ReadByte(hSocket->WizHandle, W5100_S_DPORT1(hSocket->Number), &(pDestination->Port.Byte0.Value));
                    }
                }
            }
        }
    }
    
    NET_ErrorTypeDef releaseError = hSocket->WizHandle->Hal.WIZ_HAL_ReleaseMutex();
    
    if (releaseError) return releaseError;
    if (error) return error;
    
    
    return WIZ_ERR_OK;
}

WIZ_SocketInfoTypeDef * W5100_GetSocketInfo(WIZ_HandleTypeDef *hWiz)
{
    return ((W5100_ChipInfoTypeDef *)hWiz->ChipInfo)->SocketInfo;
}

// Open socket in current (selected) mode.
static NET_ErrorTypeDef W5100_Open(WIZ_SocketHandleTypeDef *hSocket, NET_PortTypeDef port)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    uint8_t status = 0;
    
    if ((error = W5100_ReadByte(hSocket->WizHandle, W5100_S_SR(hSocket->Number), &status)) != WIZ_ERR_OK) return error;
    
    if (status != W5100_S_SR_CLOSED)
    {
        #ifdef WIZ_DEBUG
        
        net_os_puts("W5100: warn: socket already opened");
        
        #endif
        
        if ((error = W5100_WriteByte(hSocket->WizHandle, W5100_S_CR(hSocket->Number), W5100_S_CR_CLOSE)) != WIZ_ERR_OK) return error;
    }
    
    
    if ((error = hSocket->WizHandle->Hal.WIZ_HAL_TakeMutex()) != WIZ_ERR_OK) return error;
    
    if ((error = W5100_WriteByte(hSocket->WizHandle, W5100_S_PORT0(hSocket->Number), port.Byte1.Value)) == WIZ_ERR_OK)
    {
        error = W5100_WriteByte(hSocket->WizHandle, W5100_S_PORT1(hSocket->Number), port.Byte0.Value);
    }
    
    NET_ErrorTypeDef releaseError = hSocket->WizHandle->Hal.WIZ_HAL_ReleaseMutex();
    
    if (releaseError) return releaseError;
    if (error) return error;
    
    
    if ((error = W5100_WriteByte(hSocket->WizHandle, W5100_S_CR(hSocket->Number), W5100_S_CR_OPEN)) != WIZ_ERR_OK) return error;
    
    
    return WIZ_ERR_OK;
}

// Write one byte to specified address.
static NET_ErrorTypeDef W5100_WriteByte(WIZ_HandleTypeDef *hWiz, uint16_t address, uint8_t data)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    NET_Union16TypeDef union16 = { .Value = address };
    
    // Compile-time-length array, placed on stack.
    uint8_t buffer[] = { W5100_WRITE_OPCODE, union16.Byte1.Value, union16.Byte0.Value, data }; // BigEndian.
    
    if ((error = W5100_TakeControl(hWiz)) != WIZ_ERR_OK) return error;
    
    if ((error = hWiz->Hal.WIZ_HAL_TransmitReceiveSync(buffer, sizeof(buffer))) != WIZ_ERR_OK) return error;
    
    NET_ErrorTypeDef releaseError = W5100_ReleaseControl(hWiz);
    
    return releaseError ? releaseError : error;
}

// Read one byte from specified address.
static NET_ErrorTypeDef W5100_ReadByte(WIZ_HandleTypeDef *hWiz, uint16_t address, uint8_t *pData)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    NET_Union16TypeDef union16 = { .Value = address };
    
    // Compile-time-length array, placed on stack.
    uint8_t buffer[] = { W5100_READ_OPCODE, union16.Byte1.Value, union16.Byte0.Value, 0 }; // BigEndian.
    
    if ((error = W5100_TakeControl(hWiz)) != WIZ_ERR_OK) return error;
    
    if ((error = hWiz->Hal.WIZ_HAL_TransmitReceiveSync(buffer, sizeof(buffer))) != WIZ_ERR_OK) return error;
    
    NET_ErrorTypeDef releaseError = W5100_ReleaseControl(hWiz);
    
    error = releaseError ? releaseError : error;
    
    if (!error)
    {
        *pData = buffer[3];
    }
    
    return error;
}

static NET_ErrorTypeDef W5100_TakeControl(WIZ_HandleTypeDef *hWiz)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    if ((error = hWiz->Hal.WIZ_HAL_TakeMutex()) != WIZ_ERR_OK) return error;
    
    hWiz->Hal.WIZ_HAL_ActivateNss();
    
    return error;
}

static NET_ErrorTypeDef W5100_ReleaseControl(WIZ_HandleTypeDef *hWiz)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    hWiz->Hal.WIZ_HAL_DeactivateNss();
    
    if ((error = hWiz->Hal.WIZ_HAL_ReleaseMutex()) != WIZ_ERR_OK) return error;
    
    return error;
}
