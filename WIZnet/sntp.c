#include <stdlib.h>
#include <string.h>

#include "dns.h"
#include "net.h"
#include "net_os.h"
#include "udp.h"

#include "sntp.h"


#define SNTP_LEAP_INDICATOR_NONE     0
#define SNTP_VERSION_4               4
#define SNTP_MODE_CLIENT             3
#define SNTP_PORT                    123


typedef struct
{
    uint32_t Seconds;
    uint32_t Fraction;
}
SNTP_TimeStampTypeDef; // Big-endian.

typedef struct
{
    unsigned Mode          : 3;
    unsigned Version       : 3;
    unsigned LeapIndicator : 2;

    uint8_t StratumLevel;
    uint8_t PollInterval;
    uint8_t Precision;

    uint32_t Delay;
    uint32_t Dispersion;
    uint32_t ReferenceId;

    SNTP_TimeStampTypeDef RefererenceTimeStamp;
    SNTP_TimeStampTypeDef OriginTimeStamp;
    SNTP_TimeStampTypeDef ReceivedTimeStamp;
    SNTP_TimeStampTypeDef TransmitTimeStamp;
}
SNTP_PacketTypeDef; // Big-endian.


static NET_ErrorTypeDef SNTP_DetermineServer(SNTP_HandleTypeDef * hSntp, NET_IPAddressTypeDef *pServerAddress);
static NET_ErrorTypeDef SNTP_UPDProcess(SNTP_HandleTypeDef * hSntp, SNTP_PacketTypeDef *pRequest, SNTP_PacketTypeDef **ppAnswer, UDP_SocketHandleTypeDef *hUdp, NET_IPAddressTypeDef serverAddress);
static void SNTP_ConvertTime(SNTP_TimeStampTypeDef *pTimeStamp, SNTP_TimeOffsetTypeDef timeOffset, SNTP_DateTimeTypeDef *pDateTime);


// Determine current time with specified NTP server.
NET_ErrorTypeDef SNTP_DetermineDateTime(SNTP_HandleTypeDef * hSntp, SNTP_TimeOffsetTypeDef timeOffset, SNTP_DateTimeTypeDef *pDateTime)
{
    NET_ErrorTypeDef error = SNTP_ERR_OK;
    
    
    // Determine address of SNTP server.
    NET_IPAddressTypeDef serverAddress = { .Value = 0 };
    
    if ((error = SNTP_DetermineServer(hSntp, &serverAddress)) != SNTP_ERR_OK)
    {
        return error;
    } 
    
    
    // Allocate and fill request buffer.
    SNTP_PacketTypeDef *pRequest = (SNTP_PacketTypeDef *)net_os_malloc(sizeof(SNTP_PacketTypeDef));
    
    if (pRequest == NULL)
    {            
        return SNTP_ERR_MEM_ALLOCATION;
    }
    
    memset(pRequest, 0, sizeof(SNTP_PacketTypeDef));
    
    pRequest->LeapIndicator = SNTP_LEAP_INDICATOR_NONE;
    pRequest->Version = SNTP_VERSION_4;
    pRequest->Mode = SNTP_MODE_CLIENT;
    
    
    // Open or wait for UDP socket.
    UDP_SocketHandleTypeDef udpSocket;
    memset(&udpSocket, 0, sizeof(UDP_SocketHandleTypeDef));
    udpSocket.LocalPort = NET_GetFreePort(hSntp->NetHandle, NET_PORTMODE_UDP);
    
    uint16_t timeout = NET_POLL_INFINITE;
    
    if ((error = UDP_PollForOpenSocket(&udpSocket, &timeout, hSntp->NetHandle)) != UDP_ERR_OK)
    {
        net_os_free(pRequest);
        
        return (NET_ErrorTypeDef)(error | SNTP_ERR_LOW_LEVEL);
    }   
    
    #ifdef SNTP_DEBUG
            
    net_os_puts("SNTP: socket opened");
            
    #endif
    
    
    // Process UDP.
    SNTP_PacketTypeDef *pAnswer = NULL;
    
    error = SNTP_UPDProcess(hSntp, pRequest, &pAnswer, &udpSocket, serverAddress);   
    
    
    // Close socket.    
    NET_ErrorTypeDef closeError;
    
    if ((closeError = UDP_CloseSocket(&udpSocket)) != UDP_ERR_OK)
    {
        if (error != SNTP_ERR_OK && error != SNTP_ERR_CANNOT_DETERMINE)
        {
            error = (NET_ErrorTypeDef)(closeError | SNTP_ERR_LOW_LEVEL);
        }
    }
    
    
    // Convert result.
    if (error == SNTP_ERR_OK)
    {
        // Parse answer.
        SNTP_ConvertTime(&(pAnswer->TransmitTimeStamp), timeOffset, pDateTime);
        
        #ifdef SNTP_DEBUG
        
        net_os_printf("SNTP: %04u.%02u.%02uT%02u:%02u:%02u+03:00\r\n", pDateTime->Year, pDateTime->Month, pDateTime->Day, pDateTime->Hours, pDateTime->Minutes, pDateTime->Seconds);
        
        #endif
    } 
    
    
    // net_os_free resources, return.
    net_os_free(pRequest);
    net_os_free(pAnswer);
    
    return error;
}

// Determine SNTP server.
static NET_ErrorTypeDef SNTP_DetermineServer(SNTP_HandleTypeDef * hSntp, NET_IPAddressTypeDef *pServerAddress)
{
    NET_ErrorTypeDef error = SNTP_ERR_OK;
    
    
    // Create copy of SNTP server params.
    uint16_t netsrvFlags = NET_GetFlags(hSntp->NetHandle);
    *pServerAddress = NET_GetSNTPServerAddress(hSntp->NetHandle);
    char *sntpHostName = NET_GetSNTPServerName(hSntp->NetHandle);
    
    
    // Determine SNTP address source.
    bool useHostName = false;
    
    if (netsrvFlags & NET_FLAG_SNTP_NAME_PRIORITY)
    {
        if (sntpHostName != NULL)
        {
            useHostName = true;
        }
        else if (pServerAddress->Value == 0)
        {
            return SNTP_ERR_SOURCE_NOT_DEFINED;
        }
    }
    else
    {
        if (pServerAddress->Value == 0)
        {
            if (sntpHostName != NULL)
            {
                useHostName = true;
            }
            else
            {
                return SNTP_ERR_SOURCE_NOT_DEFINED;
            }            
        }
    }
    
    
    // Resolve SNTP using DNS.
    if (useHostName)
    {        
        if ((error = DNS_Resolve(hSntp->DnsHandle, sntpHostName, pServerAddress)) != DNS_ERR_OK)
        {
            return (NET_ErrorTypeDef)(error | SNTP_ERR_LOW_LEVEL);
        }
    }
    
    
    return error;
}

// Process work with UDP. Created for simple resource releasing.
static NET_ErrorTypeDef SNTP_UPDProcess(SNTP_HandleTypeDef * hSntp, SNTP_PacketTypeDef *pRequest, SNTP_PacketTypeDef **ppAnswer, UDP_SocketHandleTypeDef *hUdp, NET_IPAddressTypeDef serverAddress)
{
    NET_ErrorTypeDef error = SNTP_ERR_OK;
    
    uint8_t tryCounter = SNTP_TRY_COUNT;
    
    
    // Try send packet, receive and parse answer.
    while (tryCounter--)
    {
        uint16_t timeLeft = SNTP_POLL_TIMEOUT;
        
        
        // Wait for send packet to server.
        NET_EndPointTypeDef serverEndPoint = { .IPAddress = serverAddress, .Port.Value = SNTP_PORT };        
            
        if ((error = UDP_PollForSendData(hUdp, (uint8_t *)pRequest, sizeof(SNTP_PacketTypeDef), &serverEndPoint, &timeLeft)) != UDP_ERR_OK)
        {
            if (error == UDP_ERR_POLL_TIMEOUT || error == UDP_ERR_SEND_TIMEOUT)
            {
                #ifdef SNTP_DEBUG
            
                net_os_puts("SNTP: data send timeout");
                    
                #endif
                
                error = UDP_ERR_OK;
                continue;
            }
            else
            {
                return (NET_ErrorTypeDef)(error | SNTP_ERR_LOW_LEVEL);
            }
        }
        
        #ifdef SNTP_DEBUG
            
        net_os_puts("SNTP: data sent ok");
            
        #endif        
        
        
        while (true)
        {
            // Wait for answer datagram.
            UDP_DatagramTypeDef datagram;
            memset(&datagram, 0, sizeof(UDP_DatagramTypeDef));
            
            if ((error = UDP_PollForReceiveDatagram(hUdp, &datagram, &timeLeft)) != UDP_ERR_OK)
            {
                if (error == UDP_ERR_POLL_TIMEOUT)
                {
                    #ifdef SNTP_DEBUG
                
                    net_os_puts("SNTP: data receive timeout");
                        
                    #endif
                    
                    error = UDP_ERR_OK;
                    break;
                }
                else if (error == UDP_ERR_RX_EMPTY)
                {
                    error = UDP_ERR_OK;
                    continue;
                }
                else
                {
                    return (NET_ErrorTypeDef)(error | SNTP_ERR_LOW_LEVEL);
                }
            }


            // Check length and remote point equality.
            if ((datagram.FullLength != sizeof(SNTP_PacketTypeDef)) ||
                (serverEndPoint.IPAddress.Value != datagram.RemotePoint.IPAddress.Value) || (serverEndPoint.Port.Value != datagram.RemotePoint.Port.Value))
            {
                // Flush datagram.
                if ((error = UDP_FlushDatagram(&datagram)) != UDP_ERR_OK)
                {                    
                    return (NET_ErrorTypeDef)(error | SNTP_ERR_LOW_LEVEL);
                }
                    
                continue;
            }
            
            #ifdef SNTP_DEBUG
            
            net_os_puts("SNTP: datagram checked");
            
            #endif
            
            
            // Allocate buffer for answer.
            *ppAnswer = (SNTP_PacketTypeDef *)net_os_malloc(sizeof(SNTP_PacketTypeDef));
            
            if (*ppAnswer == NULL)
            {                
                return SNTP_ERR_MEM_ALLOCATION;
            }
            
            
            // Receive data of datagram.
            uint16_t receivedAnswerLength = 0;
            
            if ((error = UDP_ReceiveData(&datagram, ((uint8_t *)*ppAnswer), sizeof(SNTP_PacketTypeDef), &receivedAnswerLength)) != UDP_ERR_OK)
            {                
                return (NET_ErrorTypeDef)(error | SNTP_ERR_LOW_LEVEL);
            }
            
            #ifdef SNTP_DEBUG
            
            net_os_puts("SNTP: datagram received");
            
            #endif
                        
                
            return error;
        };
    }
    
    
    return SNTP_ERR_CANNOT_DETERMINE;
}

// Convert seconds from NTP timestamp to date and time.
static void SNTP_ConvertTime(SNTP_TimeStampTypeDef *pTimeStamp, SNTP_TimeOffsetTypeDef timeOffset, SNTP_DateTimeTypeDef *pDateTime)
{
    memset(pDateTime, 0, sizeof(SNTP_DateTimeTypeDef));
    
    uint32_t secondsTotal = ntohl(pTimeStamp->Seconds);
    
    secondsTotal += (3600 * timeOffset);
    
    pDateTime->Seconds = secondsTotal % 60;
    secondsTotal /= 60;
    
    pDateTime->Minutes = secondsTotal % 60;
    secondsTotal /= 60;
    
    pDateTime->Hours = secondsTotal % 24;
    
    
    static const uint16_t longMonths[] = { 0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335, 366 };
    static const uint16_t shortMonths[] = { 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365 };
    
    
    uint16_t days = secondsTotal / 24;
    
    uint8_t years = 0;
    
    if (days >= shortMonths[12])
    {
        years = (days / 365) - 1;

        days -= years * 365;

        for (uint8_t i = 1; i < years; i++)
        {
            if (!(i & 0x03)) days--;
        }

        uint16_t currentYearDays = ((!(years & 0x03) && years != 0) ? longMonths[12] : shortMonths[12]);

        if (days + 1 > currentYearDays)
        {
            years++;
            days -= currentYearDays;
        }
    }
    
    const uint16_t *monthArray = ((!(years & 0x03) && years != 0) ? longMonths : shortMonths);

    uint8_t months = 11;

    while (monthArray[months] > days) months--;

    days -= monthArray[months];

    days++;
    months++;
    
    pDateTime->Day = days;
    pDateTime->Month = months;
    pDateTime->Year = years + 1900;
}
