#include <stdbool.h>
#include <stdint.h>

#include "net_def.h"
#include "net_os.h"

#include "wizchip.h"


NET_ErrorTypeDef WIZ_Init(WIZ_HandleTypeDef *hWiz)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    if ((error = hWiz->ChipApi->WIZ_CHIP_API_Init(hWiz)) != WIZ_ERR_OK) return error;
    
    
    return WIZ_ERR_OK;
}

NET_ErrorTypeDef WIZ_DeInit(WIZ_HandleTypeDef *hWiz)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    if ((error = hWiz->ChipApi->WIZ_CHIP_API_DeInit(hWiz)) != WIZ_ERR_OK) return error;
    
    
    return WIZ_ERR_OK;
}

NET_ErrorTypeDef WIZ_SetMemorySizes(WIZ_HandleTypeDef *hWiz, WIZ_MemorySizeTypeDef *pTxSizes, WIZ_MemorySizeTypeDef *pRxSizes)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    if ((error = hWiz->ChipApi->WIZ_CHIP_API_SetMemorySizes(hWiz, pTxSizes, pRxSizes)) != WIZ_ERR_OK) return error;
    
    
    #ifdef WIZ_DEBUG
    
    net_os_printf("WIZ memory: SOCKET:TX:RX");
    
    #endif
    
    
    WIZ_SocketInfoTypeDef * socketInfo = (WIZ_SocketInfoTypeDef *)hWiz->ChipApi->WIZ_CHIP_API_GetSocketInfo(hWiz);    
    
    for (WIZ_SocketNumberTypeDef socket = WIZ_SOCKET_0; socket < hWiz->ChipApi->SocketCount; socket++)
    {
        WIZ_SocketInfoTypeDef info = { .TxSize = pTxSizes[socket], .RxSize = pRxSizes[socket], .IsBusy = 0 };
        
        socketInfo[socket].TxSize = info.TxSize;
        socketInfo[socket].RxSize = info.RxSize;
        socketInfo[socket].IsBusy = 0;
        
        
        #ifdef WIZ_DEBUG
    
        net_os_printf(" %u:%u:%u", socket, info.TxSize, info.RxSize);
        
        #endif 
    }
    
    #ifdef WIZ_DEBUG
    
    net_os_puts("");
    
    #endif
    
    
    return WIZ_ERR_OK;
}

NET_ErrorTypeDef WIZ_SetMacAddress(WIZ_HandleTypeDef *hWiz, uint8_t *pMacAddress)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    if ((error = hWiz->ChipApi->WIZ_CHIP_API_SetMacAddress(hWiz, pMacAddress)) != WIZ_ERR_OK) return error;
    
    
    #ifdef WIZ_DEBUG
    
    net_os_printf("WIZ: MacAddr:%02X-%02X-%02X-%02X-%02X-%02X\r\n", pMacAddress[5], pMacAddress[4], pMacAddress[3], pMacAddress[2], pMacAddress[1], pMacAddress[0]);

    #endif
    
    
    return WIZ_ERR_OK;
}

NET_ErrorTypeDef WIZ_SetIPAddress(WIZ_HandleTypeDef *hWiz, NET_IPAddressTypeDef ipAddress)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    if ((error = hWiz->ChipApi->WIZ_CHIP_API_SetIPAddress(hWiz, ipAddress)) != WIZ_ERR_OK) return error;
    
    
    #ifdef WIZ_DEBUG
    
    net_os_print_ip("WIZ: SrcAddr:", ipAddress, "\0");
        
    #endif
    
    
    return WIZ_ERR_OK;
}

NET_ErrorTypeDef WIZ_SetSubnetMask(WIZ_HandleTypeDef *hWiz, NET_IPAddressTypeDef subnetMask)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    if ((error = hWiz->ChipApi->WIZ_CHIP_API_SetSubnetMask(hWiz, subnetMask)) != WIZ_ERR_OK) return error;
    
    
    #ifdef WIZ_DEBUG
    
    net_os_print_ip("WIZ: SbntMsk:", subnetMask, "\0");
        
    #endif
    
    
    return WIZ_ERR_OK;
}

NET_ErrorTypeDef WIZ_SetDefaultGateway(WIZ_HandleTypeDef *hWiz, NET_IPAddressTypeDef defaultGateway)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    if ((error = hWiz->ChipApi->WIZ_CHIP_API_SetDefaultGateway(hWiz, defaultGateway)) != WIZ_ERR_OK) return error;
    
    
    #ifdef WIZ_DEBUG
    
    net_os_print_ip("WIZ: Gateway:", defaultGateway, "\0");
        
    #endif
    
    
    return WIZ_ERR_OK;
}

NET_ErrorTypeDef WIZ_TakeSocket(WIZ_SocketHandleTypeDef *hSocket)
{
    WIZ_SocketInfoTypeDef *pSocketInfo = (WIZ_SocketInfoTypeDef *)hSocket->WizHandle->ChipApi->WIZ_CHIP_API_GetSocketInfo(hSocket->WizHandle);
    
    NET_OS_LOCK(&(hSocket->WizHandle->__SocketPoolLocker))
    {    
        for (WIZ_SocketNumberTypeDef socket = WIZ_SOCKET_0; socket < hSocket->WizHandle->ChipApi->SocketCount; socket++)
        {
            if (!pSocketInfo[socket].IsBusy && pSocketInfo[socket].TxSize != WIZ_MEMSIZE_0 && pSocketInfo[socket].RxSize != WIZ_MEMSIZE_0) // Not busy, tx and rx not 0.
            {            
                pSocketInfo[socket].IsBusy = true;
                
                NET_OS_Unlock(&(hSocket->WizHandle->__SocketPoolLocker));
                
                
                hSocket->Number = socket;
                hSocket->TxSize = pSocketInfo[socket].TxSize;
                hSocket->RxSize = pSocketInfo[socket].RxSize;
                hSocket->IsOwner = true;
                
                return WIZ_ERR_OK;
            }
        }
    }
    
    
    return WIZ_ERR_NO_SOCK_AVAILABLE;
}

NET_ErrorTypeDef WIZ_ReleaseSocket(WIZ_SocketHandleTypeDef *hSocket)
{
    WIZ_SocketInfoTypeDef * socketInfo = (WIZ_SocketInfoTypeDef *)hSocket->WizHandle->ChipApi->WIZ_CHIP_API_GetSocketInfo(hSocket->WizHandle);
    
    if (hSocket == NULL)
    {
        #ifdef WIZ_DEBUG
        
        net_os_puts("WIZ: release err: hSocket == NULL");
        
        #endif
        
        return WIZ_ERR_NULLPTR;
    }
    
    if (hSocket->Number >= hSocket->WizHandle->ChipApi->SocketCount) // If unknown number.
    {
        #ifdef WIZ_DEBUG
        
        net_os_puts("WIZ: release err: hSocket->Number >= WIZ_SOCKET_COUNT");
        
        #endif
        
        return WIZ_ERR_WRONGSOCKET;
    }
    
    if (!hSocket->IsOwner) // If not owner.
    {
        #ifdef WIZ_DEBUG
        
        net_os_puts("WIZ: release warn: !hSocket->IsOwner");
        
        #endif
    }
    
    if (!socketInfo[hSocket->Number].IsBusy) // If already released.
    {
        #ifdef WIZ_DEBUG
        
        net_os_puts("WIZ: release warn: !_sockets[hSocket->Number].IsBusy");
        
        #endif
    }
    
    
    hSocket->IsOwner = false;
    
    NET_OS_LOCK(&(hSocket->WizHandle->__SocketPoolLocker))
    {    
        socketInfo[hSocket->Number].IsBusy = false;
    }
    
    
    return WIZ_ERR_OK;
}

void WIZ_RememberPort(WIZ_SocketHandleTypeDef *hSocket, NET_PortTypeDef port, NET_PortModeTypeDef portMode)
{
    WIZ_SocketInfoTypeDef *pSocketInfo = (WIZ_SocketInfoTypeDef *)hSocket->WizHandle->ChipApi->WIZ_CHIP_API_GetSocketInfo(hSocket->WizHandle);
    
    NET_OS_LOCK(&(hSocket->WizHandle->__SocketPoolLocker))
    {
        pSocketInfo[hSocket->Number].Port = port;
        pSocketInfo[hSocket->Number].PortMode = portMode;
    }
}

bool WIZ_IsPortBusy(WIZ_HandleTypeDef *hWiz, NET_PortTypeDef port, NET_PortModeTypeDef portMode)
{
    WIZ_SocketInfoTypeDef *pSocketInfo = (WIZ_SocketInfoTypeDef *)hWiz->ChipApi->WIZ_CHIP_API_GetSocketInfo(hWiz);
    
    NET_OS_LOCK(&(hWiz->__SocketPoolLocker))
    {    
        for (WIZ_SocketNumberTypeDef socket = WIZ_SOCKET_0; socket < hWiz->ChipApi->SocketCount; socket++)
        {
            if (pSocketInfo[socket].IsBusy && pSocketInfo[socket].Port.Value == port.Value && pSocketInfo[socket].PortMode == portMode)
            {                
                NET_OS_Unlock(&(hWiz->__SocketPoolLocker));
                
                return true;
            }
        }
    }
    
    return false;
}

NET_ErrorTypeDef WIZ_GetFlags(WIZ_HandleTypeDef *hWiz, WIZ_FlagTypeDef *pFlags, WIZ_FlagTypeDef flagsMask)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    if ((error = hWiz->ChipApi->WIZ_CHIP_API_GetFlags(hWiz, pFlags, flagsMask)) != WIZ_ERR_OK) return error;
    
    
    return WIZ_ERR_OK;
}

NET_ErrorTypeDef WIZ_AddFlagUsingSocket(WIZ_SocketHandleTypeDef *hSocket, WIZ_FlagTypeDef *pFlags)
{
    if (hSocket->Number >= hSocket->WizHandle->ChipApi->SocketCount) return WIZ_ERR_WRONGSOCKET;
    
    
    *pFlags += (1 << hSocket->Number);
    
    
    return WIZ_ERR_OK;
}

NET_ErrorTypeDef WIZ_ApplyMaskForSocket(WIZ_SocketHandleTypeDef *hSocket, WIZ_FlagTypeDef *pFlags)
{
    if (hSocket->Number >= hSocket->WizHandle->ChipApi->SocketCount) return WIZ_ERR_WRONGSOCKET;
    
    
    *pFlags &= (1 << hSocket->Number);
    
    
    return WIZ_ERR_OK;
}
