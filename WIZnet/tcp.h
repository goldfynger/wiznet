#ifndef __TCP_H
#define __TCP_H


#include <stdint.h>

#include "net.h"
#include "net_def.h"
#include "wizchip.h"


#define TCP_POLL_INTERVAL  20


typedef enum
{
    TCP_STATE_UNKNOWN       = 0,
    TCP_STATE_CLOSED        = 1,
    TCP_STATE_OPENED        = 2,
    TCP_STATE_LISTEN        = 3,
    TCP_STATE_CONNECTION    = 4,
    TCP_STATE_CONNECTED     = 5,
    TCP_STATE_CLOSE_WAIT    = 6,
    TCP_STATE_IN_PROCESS    = 7,
    TCP_STATE_CLOSING       = 8,
}
TCP_StateTypeDef;

typedef enum
{
    TCP_FLAG_NONE           = 0x00,
    
    TCP_FLAG_CONNECTED      = 0x01,
    TCP_FLAG_DISCONNECTION  = 0x02,
    TCP_FLAG_RECEIVED       = 0x04,
    TCP_FLAG_TIMEOUT        = 0x08,
    TCP_FLAG_SENT           = 0x10,
    
    TCP_FLAG_ALL            = 0xFF,
}
TCP_FlagTypeDef;


typedef struct
{
    WIZ_SocketHandleTypeDef Socket;
    
    NET_EndPointTypeDef     RemotePoint;
    
    NET_PortTypeDef         LocalPort;
}
TCP_SocketHandleTypeDef;


NET_ErrorTypeDef TCP_ConnectSocket       (TCP_SocketHandleTypeDef *hTcp, NET_HandleTypeDef *hNet);
NET_ErrorTypeDef TCP_ListenSocket        (TCP_SocketHandleTypeDef *hTcp, NET_HandleTypeDef *hNet);

NET_ErrorTypeDef TCP_SendLength          (TCP_SocketHandleTypeDef *hTcp, uint16_t *pLength);
NET_ErrorTypeDef TCP_SendData            (TCP_SocketHandleTypeDef *hTcp, uint8_t *pBuffer, uint16_t bufferLength);

NET_ErrorTypeDef TCP_ReceiveLength       (TCP_SocketHandleTypeDef *hTcp, uint16_t *pLength);
NET_ErrorTypeDef TCP_ReceiveData         (TCP_SocketHandleTypeDef *hTcp, uint8_t *pBuffer, uint16_t bufferLength, uint16_t *pReceivedLength);

NET_ErrorTypeDef TCP_DisconnectSocket    (TCP_SocketHandleTypeDef *hTcp);
NET_ErrorTypeDef TCP_CloseSocket         (TCP_SocketHandleTypeDef *hTcp);

NET_ErrorTypeDef TCP_GetState            (TCP_SocketHandleTypeDef *hTcp, TCP_StateTypeDef *pState);
NET_ErrorTypeDef TCP_GetFlags            (TCP_SocketHandleTypeDef *hTcp, TCP_FlagTypeDef *pFlags, TCP_FlagTypeDef flagsMask);
NET_ErrorTypeDef TCP_PollForFlags        (TCP_SocketHandleTypeDef *hTcp, TCP_FlagTypeDef *pFlags, TCP_FlagTypeDef flagsMask, uint16_t *pTimeout);

NET_ErrorTypeDef TCP_GetDestination      (TCP_SocketHandleTypeDef *hTcp, NET_EndPointTypeDef *pDestination);


#endif /* __TCP_H */
