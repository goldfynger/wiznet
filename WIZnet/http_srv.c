#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "net.h"
#include "net_os.h"

#include "http_srv.h"


#define HTTP_SRV_METHOD_MAX_LENGTH  ((uint8_t)5)
#define HTTP_SRV_URL_MAX_LENGTH     ((uint8_t)20)


static const char _methodGet[] = "GET";


static NET_ErrorTypeDef HTTP_SRV_CheckFlags              (HTTP_SRV_HandleTypeDef *hHttp);
static NET_ErrorTypeDef HTTP_SRV_TryAddListener          (HTTP_SRV_HandleTypeDef *hHttp);

static NET_ErrorTypeDef HTTP_SRV_TryReceiveAndProcess    (HTTP_SRV_HandleTypeDef *hHttp, uint8_t socketIndex);
static NET_ErrorTypeDef HTTP_SRV_CheckSending            (HTTP_SRV_SocketHandleTypeDef *hHttpSocket);

static NET_ErrorTypeDef HTTP_SRV_ReceiveAndParseRequest  (HTTP_SRV_HandleTypeDef *hHttp, uint8_t socketIndex);
static NET_ErrorTypeDef HTTP_SRV_ProcessBadRequest       (TCP_SocketHandleTypeDef *hTcp);
static NET_ErrorTypeDef HTTP_SRV_SendHeader              (HTTP_SRV_SocketHandleTypeDef *hHttpSocket);
static NET_ErrorTypeDef HTTP_SRV_SendContent             (HTTP_SRV_SocketHandleTypeDef *hHttpSocket);
static NET_ErrorTypeDef HTTP_SRV_SendData                (TCP_SocketHandleTypeDef *hTcp, const char *pData, uint16_t length);
static NET_ErrorTypeDef HTTP_SRV_Disconnect              (HTTP_SRV_SocketHandleTypeDef *hHttpSocket);


// Allocates and clears fields with variable length.
NET_ErrorTypeDef HTTP_SRV_AllocHandle(HTTP_SRV_HandleTypeDef *hHttp, NET_PortTypeDef localPort)
{
    if (hHttp == NULL) return HTTP_SRV_ERR_NULLPTR;
    
    
    #ifdef HTTP_SRV_DEBUG
    
    if (hHttp->HttpSockets != NULL)
    {
        net_os_puts("HTTP_SRV: warn: hHttp->HttpSockets != NULL");
    }
    
    if (hHttp->UrlEntries != NULL)
    {
        net_os_puts("HTTP_SRV: warn: hHttp->UrlEntries != NULL");
    }
    
    #endif
    
    
    // Allocates array with HTTP sockets.
    uint16_t socketsSize = sizeof(HTTP_SRV_SocketHandleTypeDef) * hHttp->SocketsCount;
    
    hHttp->HttpSockets = (HTTP_SRV_SocketHandleTypeDef *)net_os_malloc(socketsSize);
    
    if (hHttp->HttpSockets == NULL) return HTTP_SRV_ERR_MEM_ALLOCATION;    
    
    
    // Allocates array with URL entries.
    uint16_t urlEntriesSize = sizeof(HTTP_SRV_UrlEntryTypeDef) * hHttp->UrlEntriesMaxCount;
    
    hHttp->UrlEntries = (HTTP_SRV_UrlEntryTypeDef *)net_os_malloc(urlEntriesSize);
    
    if (hHttp->UrlEntries == NULL)
    {
        net_os_free(hHttp->HttpSockets);
        
        return HTTP_SRV_ERR_MEM_ALLOCATION;
    }
    
    
    // Clears (initializes) allocated arrays.
    memset(hHttp->HttpSockets, 0, socketsSize);
    
    memset(hHttp->UrlEntries, 0, urlEntriesSize);
    
    
    // Sets HTTP server local port of all sockets.
    for (uint8_t i = 0; i < hHttp->SocketsCount; i++)
    {
        hHttp->HttpSockets[i].TcpSocket.LocalPort = localPort;
    }
    
    
    return HTTP_SRV_ERR_OK;
}

// Frees fields with variable length.
NET_ErrorTypeDef HTTP_SRV_FreeHandle(HTTP_SRV_HandleTypeDef *hHttp)
{
    if (hHttp == NULL) return HTTP_SRV_ERR_NULLPTR;
    
    
    #ifdef HTTP_SRV_DEBUG
    
    if (hHttp->HttpSockets == NULL)
    {
        net_os_puts("HTTP_SRV: warn: hHttp->HttpSockets == NULL");
    }
    
    if (hHttp->UrlEntries == NULL)
    {
        net_os_puts("HTTP_SRV: warn: hHttp->UrlEntries == NULL");
    }
    
    #endif
    
    
    net_os_free(hHttp->HttpSockets);
    
    net_os_free(hHttp->UrlEntries);
    
    
    return HTTP_SRV_ERR_OK;
}

// Add new URL entry in entries array.
NET_ErrorTypeDef HTTP_SRV_AddUrlEntry(HTTP_SRV_HandleTypeDef *hHttp, const uint8_t *content, uint16_t contentLength, const char *url, uint8_t urlLength, const char *contentType, uint8_t contentTypeLength)
{
    if (hHttp == NULL || hHttp->HttpSockets == NULL || hHttp->UrlEntries == NULL) return HTTP_SRV_ERR_NULLPTR;
    
    if (url == NULL || content == NULL || contentType == NULL) return HTTP_SRV_ERR_NULLPTR;
    
    if (urlLength == 0 || contentLength == 0 || contentTypeLength == 0) return HTTP_SRV_ERR_ZEROLENGTH;
    
    if (hHttp->UrlEntriesCount >= hHttp->UrlEntriesMaxCount) return HTTP_SRV_ERR_URL_MAX_EXCEEDED;
    
    
    HTTP_SRV_UrlEntryTypeDef *pUrlEntry = hHttp->UrlEntries + hHttp->UrlEntriesCount;
    
    pUrlEntry->Content = content;
    pUrlEntry->ContentLength = contentLength;
    pUrlEntry->Url = url;
    pUrlEntry->UrlLength = urlLength;
    pUrlEntry->ContentType = contentType;
    pUrlEntry->ContentTypeLength = contentTypeLength;
    
    hHttp->UrlEntriesCount++;
    
    
    return HTTP_SRV_ERR_OK;
}

// Process HTTP server.
NET_ErrorTypeDef HTTP_SRV_Run(HTTP_SRV_HandleTypeDef *hHttp)
{
    if (hHttp == NULL || hHttp->HttpSockets == NULL || hHttp->UrlEntries == NULL) return HTTP_SRV_ERR_NULLPTR;
    
    
    NET_ErrorTypeDef error = HTTP_SRV_ERR_OK;
    
    
    // Checks sockets flags, runs actions if need.
    if ((error = HTTP_SRV_CheckFlags(hHttp)) != HTTP_SRV_ERR_OK) return error;
    
    
    // Tries add new listener if need.
    if ((error = HTTP_SRV_TryAddListener(hHttp)) != HTTP_SRV_ERR_OK) return error;
    
    
    return HTTP_SRV_ERR_OK;
}

// Checks WIZ flags of all owned sockets, if flags are set, process flags.
static NET_ErrorTypeDef HTTP_SRV_CheckFlags(HTTP_SRV_HandleTypeDef *hHttp)
{
    NET_ErrorTypeDef error = HTTP_SRV_ERR_OK;
    
    
    WIZ_FlagTypeDef flagsMask = WIZ_FLAG_NONE;
    WIZ_HandleTypeDef *hWiz = NULL;
    
    // Fills mask of flags.
    for (uint8_t i = 0; i < hHttp->SocketsCount; i++)
    {
        TCP_SocketHandleTypeDef *hTcpSocket = &((hHttp->HttpSockets + i)->TcpSocket);
        
        // Checks is server owner of this socket.
        if (hTcpSocket->Socket.IsOwner)
        {
            // Add bit of current socket in common mask.
            if ((error = WIZ_AddFlagUsingSocket(&(hTcpSocket->Socket), &flagsMask)) != TCP_ERR_OK)
            {
                // Unexpected error.
                error = (NET_ErrorTypeDef)(HTTP_SRV_ERR_LOW_LEVEL | error);
                return error;
            }
            
            hWiz = hTcpSocket->Socket.WizHandle;
        }
    }
    
    // If mask is empty - return: no sockets owned.
    if (!flagsMask) return HTTP_SRV_ERR_OK;    
    
    
    WIZ_FlagTypeDef flags = WIZ_FLAG_NONE; 
    
    // Gets WIZ flags.
    if ((error = WIZ_GetFlags(hWiz, &flags, flagsMask)) != TCP_ERR_OK)
    {
        // Unexpected error.
        error = (NET_ErrorTypeDef)(HTTP_SRV_ERR_LOW_LEVEL | error);
        return error;
    }
    
    // If flags is empty - return: no owned sockets with flags.
    if (!flags) return HTTP_SRV_ERR_OK;
    
    
    for (uint8_t i = 0; i < hHttp->SocketsCount; i++)
    {
        TCP_SocketHandleTypeDef *hTcpSocket = &((hHttp->HttpSockets + i)->TcpSocket);
        
        // Checks is server owner of this socket.
        if (hTcpSocket->Socket.IsOwner)
        {
            WIZ_FlagTypeDef socketFlag = flags;
            
            // Applies socket mask on socket.
            if ((error = WIZ_ApplyMaskForSocket(&(hTcpSocket->Socket), &socketFlag)) != TCP_ERR_OK)
            {
                // Unexpected error.
                error = (NET_ErrorTypeDef)(HTTP_SRV_ERR_LOW_LEVEL | error);
                return error;
            }
            
            // No flags for this socket - continue.
            if (!socketFlag) continue;
            
            
            TCP_FlagTypeDef tcpFlags = TCP_FLAG_NONE;
            
            // Gets concrete flags for this socket.
            if ((error = TCP_GetFlags(hTcpSocket, &tcpFlags, TCP_FLAG_ALL)) != TCP_ERR_OK)
            {
                // Unexpected error.
                error = (NET_ErrorTypeDef)(HTTP_SRV_ERR_LOW_LEVEL | error);
                return error;
            }
            
            
            if (tcpFlags & TCP_FLAG_CONNECTED)
            {
                #ifdef HTTP_SRV_DEBUG
                
                NET_EndPointTypeDef destination = { .IPAddress.Value = 0, .Port.Value = 0 };
                
                if ((error = TCP_GetDestination(hTcpSocket, &destination)) != TCP_ERR_OK)
                {
                    // Unexpected error.
                    error = (NET_ErrorTypeDef)(HTTP_SRV_ERR_LOW_LEVEL | error);
                    return error;
                }
                
                net_os_printf("HTTP_SRV: CONNECTED on socket:%u dst:%u.%u.%u.%u:%u\r\n", hTcpSocket->Socket.Number,
                    destination.IPAddress.Byte3.Value, destination.IPAddress.Byte2.Value, destination.IPAddress.Byte1.Value, destination.IPAddress.Byte0.Value, destination.Port.Value);
                
                #endif
                
                
                hHttp->ListeningInProcess = false;
            }
            
            if (tcpFlags & TCP_FLAG_RECEIVED)
            {
                #ifdef HTTP_SRV_DEBUG
                
                net_os_printf("HTTP_SRV: RECEIVED on socket:%u\r\n", hTcpSocket->Socket.Number);
                
                #endif
                
                
                if ((error = HTTP_SRV_TryReceiveAndProcess(hHttp, i)) != HTTP_SRV_ERR_OK) return error;
            }
            
            if (tcpFlags & TCP_FLAG_SENT)
            {
                #ifdef HTTP_SRV_DEBUG
                
                net_os_printf("HTTP_SRV: SENT on socket:%u\r\n", hTcpSocket->Socket.Number);
                
                #endif
                
                
                if ((error = HTTP_SRV_CheckSending((hHttp->HttpSockets + i))) != HTTP_SRV_ERR_OK) return error;
            }
            
            if (tcpFlags & (TCP_FlagTypeDef)(TCP_FLAG_TIMEOUT | TCP_FLAG_DISCONNECTION))
            {
                #ifdef HTTP_SRV_DEBUG
                
                if (tcpFlags & TCP_FLAG_TIMEOUT)
                {
                    net_os_printf("HTTP_SRV: TIMEOUT on socket:%u\r\n", hTcpSocket->Socket.Number);
                }
                
                if (tcpFlags & TCP_FLAG_DISCONNECTION)
                {
                    net_os_printf("HTTP_SRV: DISCONNECTION on socket:%u\r\n", hTcpSocket->Socket.Number);
                }
                
                #endif
                
                
                // Sends disconnect if not sent yet.
                if (tcpFlags & TCP_FLAG_DISCONNECTION && !(hHttp->HttpSockets + i)->SentDisconnect)
                {
                    if ((error == HTTP_SRV_Disconnect(hHttp->HttpSockets + i)) != HTTP_SRV_ERR_OK) return error;
                }
                else 
                {
                    if ((error = TCP_CloseSocket(hTcpSocket)) != TCP_ERR_OK)
                    {
                        // Unexpected error.
                        error = (NET_ErrorTypeDef)(HTTP_SRV_ERR_LOW_LEVEL | error);
                        return error;
                    }
                }
            }
        }
    }
    
    
    return HTTP_SRV_ERR_OK;
}

// Checks for active listeners, if not found, tries open new listener.
static NET_ErrorTypeDef HTTP_SRV_TryAddListener(HTTP_SRV_HandleTypeDef *hHttp)
{
    NET_ErrorTypeDef error = HTTP_SRV_ERR_OK;
    
    
    // Socket in listening state existed.
    if (hHttp->ListeningInProcess) return HTTP_SRV_ERR_OK;    
    
    
    for (uint8_t i = 0; i < hHttp->SocketsCount; i++)
    {        
        // Checks is server not owner of this socket.
        if (!(hHttp->HttpSockets + i)->TcpSocket.Socket.IsOwner)
        {
            // Try listen free socket.
            if ((error = TCP_ListenSocket(&((hHttp->HttpSockets + i)->TcpSocket), hHttp->NetHandle)) != TCP_ERR_OK)
            {
                // All physical sockets already busy.
                if (error == TCP_ERR_NO_SOCK_AVAILABLE)
                {
                    // Can not add new listener, try again later.
                    return HTTP_SRV_ERR_OK;
                }
                else
                {
                    // Unexpected error.
                    error = (NET_ErrorTypeDef)(HTTP_SRV_ERR_LOW_LEVEL | error);
                    return error;
                }
            }
            
            // New socket opened as listener.
            hHttp->ListeningInProcess = true;
            
            // Remove sent disconnect flag.
            (hHttp->HttpSockets + i)->SentDisconnect = false;
            
            
            #ifdef HTTP_SRV_DEBUG
            
            net_os_printf("HTTP_SRV: LISTEN on socket:%u\r\n", (hHttp->HttpSockets + i)->TcpSocket.Socket.Number);
            
            #endif
            
            
            return HTTP_SRV_ERR_OK;
        }
    }
    
    
    return HTTP_SRV_ERR_OK;
}

// Receives data and runs parsing and responding processes if need.
static NET_ErrorTypeDef HTTP_SRV_TryReceiveAndProcess(HTTP_SRV_HandleTypeDef *hHttp, uint8_t socketIndex)
{
    NET_ErrorTypeDef error = HTTP_SRV_ERR_OK;
    
    
    HTTP_SRV_SocketHandleTypeDef *hHttpSocket = hHttp->HttpSockets + socketIndex;
    
               
    // Tries receive and parse request.
    if ((error = HTTP_SRV_ReceiveAndParseRequest(hHttp, socketIndex)) != HTTP_SRV_ERR_OK)
    {
        if (error == HTTP_SRV_ERR_BAD_REQUEST)
        {
            #ifdef HTTP_SRV_DEBUG
        
            net_os_puts("HTTP_SRV: BAD_REQUEST");
            
            #endif
            
            
            error = HTTP_SRV_ERR_OK;
            
            // If request is bad, processes it.
            if ((error = HTTP_SRV_ProcessBadRequest(&(hHttpSocket->TcpSocket))) != HTTP_SRV_ERR_OK)
            {
                // Unexpected error.
                return error;
            }
            
            return HTTP_SRV_ERR_OK;
        }
        // RX buffer was empty.
        else if (error & TCP_ERR_RX_EMPTY)
        {
            return HTTP_SRV_ERR_OK;
        }                        
        else
        {
            // Unexpected error.
            return error;
        }
    }
    
    
    // If we still here, request is successfully parsed, need send response.
    
    
    // Sends HTTP header.
    if ((error = HTTP_SRV_SendHeader(hHttpSocket)) != HTTP_SRV_ERR_OK)
    {
        // Unexpected error.
        return error;
    }
    
    
    // Clears field that indicates length of sent content.
    hHttp->HttpSockets[socketIndex].SentLength = 0;
    
    // Sends HTTP content (part or full).
    if ((error = HTTP_SRV_SendContent(hHttpSocket)) != HTTP_SRV_ERR_OK)
    {
        // Unexpected error.
        return error;
    }
    
    
    return HTTP_SRV_ERR_OK;
}

// Sends unsent parts of content, starts disconnection if all data sent.
static NET_ErrorTypeDef HTTP_SRV_CheckSending(HTTP_SRV_SocketHandleTypeDef *hHttpSocket)
{
    NET_ErrorTypeDef error = HTTP_SRV_ERR_OK;    
    
    
    // If unsent part of content is exist.
    if (hHttpSocket->CurrentUrlEntry->ContentLength > hHttpSocket->SentLength)
    {
        // Send unsent data.
        if ((error = HTTP_SRV_SendContent(hHttpSocket)) != HTTP_SRV_ERR_OK) return error;
    }
    else
    {
        // Disconnect when all content is sent.
        if ((error = HTTP_SRV_Disconnect(hHttpSocket)) != TCP_ERR_OK) return error;
    }
    
    
    return HTTP_SRV_ERR_OK;
}

// Receives and parses request. Return pointer on URL entry if success.
static NET_ErrorTypeDef HTTP_SRV_ReceiveAndParseRequest(HTTP_SRV_HandleTypeDef *hHttp, uint8_t socketIndex)
{
    NET_ErrorTypeDef error = HTTP_SRV_ERR_OK;
    
    
    TCP_SocketHandleTypeDef *hTcpSocket = &(hHttp->HttpSockets[socketIndex].TcpSocket);
    
    uint16_t length = 0;
    
    // Gets length of data in receive buffer.
    if ((error = TCP_ReceiveLength(hTcpSocket, &length)) != TCP_ERR_OK)
    {
        // Unexpected error.
        error = (NET_ErrorTypeDef)(HTTP_SRV_ERR_LOW_LEVEL | error);
        return error;
    }
    
    // If data in buffer exists.
    if (length)
    {
        // Allocate memory buffer for request.
        char *pRequest = (char *)net_os_malloc(length);
        
        if (pRequest == NULL)
        {
            return HTTP_SRV_ERR_MEM_ALLOCATION;
        }
        
        
        uint16_t receivedLength = 0;
        
        // Receives data in allocated buffer.
        if ((error = TCP_ReceiveData(hTcpSocket, (uint8_t *)pRequest, length, &receivedLength)) != TCP_ERR_OK)
        {
            net_os_free(pRequest);
            
            // Unexpected error.
            error = (NET_ErrorTypeDef)(HTTP_SRV_ERR_LOW_LEVEL | error);
            return error;
        }
        
        
        #ifdef HTTP_SRV_DEBUG_DATA
    
        uint16_t debugIdx = 0;
        
        while (debugIdx < receivedLength)
        {
            net_os_printf("%c", pRequest[debugIdx++]);
        }
        
        #endif
        
        
        uint16_t requestCounter = 0;
        
        
        uint8_t methodStart = requestCounter;
        uint8_t methodLength = 0;
        
        // Looking for method string.
        while (pRequest[methodStart + methodLength] != ' ')
        {
            if (++methodLength > HTTP_SRV_METHOD_MAX_LENGTH)
            {
                net_os_free(pRequest);
                
                return HTTP_SRV_ERR_BAD_REQUEST;
            }
        }
        
        
        #ifdef HTTP_SRV_DEBUG
        
        net_os_printf("HTTP_SRV: method:%.*s\r\n", methodLength, pRequest + methodStart);
        
        #endif
        
        
        // Checks length and equality of known HTTP methods.
        if (methodLength != strlen(_methodGet) || memcmp(pRequest + methodStart, _methodGet, methodLength))
        {
            net_os_free(pRequest);
                
            return HTTP_SRV_ERR_BAD_REQUEST;
        }
        
        // Skip splitter space.
        requestCounter += methodLength + 1;
        
        
        uint8_t urlStart = requestCounter;
        uint8_t urlLength = 0;
        
        // Looking for URL string.
        while (pRequest[urlStart + urlLength] != ' ')
        {
            if (++urlLength > HTTP_SRV_URL_MAX_LENGTH)
            {
                net_os_free(pRequest);
                
                return HTTP_SRV_ERR_BAD_REQUEST;
            }
        }
        
        
        #ifdef HTTP_SRV_DEBUG
        
        net_os_printf("HTTP_SRV: URL:%.*s\r\n", urlLength, pRequest + urlStart);
        
        #endif
        
        
        // Checks length and equality of all known URL entries.
        for (uint8_t i = 0; i < hHttp->UrlEntriesCount; i++)
        {
            HTTP_SRV_UrlEntryTypeDef *pUrlEntry = &(hHttp->UrlEntries[i]);
            
            if (urlLength == pUrlEntry->UrlLength && !memcmp(pRequest + urlStart, pUrlEntry->Url, urlLength))
            {
                hHttp->HttpSockets[socketIndex].CurrentUrlEntry = pUrlEntry;
                
                net_os_free(pRequest);
                
                return HTTP_SRV_ERR_OK;
            }
        }
        
        // If we still here, no entries found, return BAD_REQUEST.
        
        net_os_free(pRequest);
                
        return HTTP_SRV_ERR_BAD_REQUEST;
    }
    else
    {
        // If no data in rx buffer, return RX_EMPTY error.
        return (NET_ErrorTypeDef)(TCP_ERR_RX_EMPTY | HTTP_SRV_ERR_LOW_LEVEL);
    }
}

// Sends BAD REQUEST response.
static NET_ErrorTypeDef HTTP_SRV_ProcessBadRequest(TCP_SocketHandleTypeDef *hTcp)
{
    static const char badResponse[] = "HTTP/1.1 404 Not Found\r\nContent-Length: 0\r\nContent-Type: text/html\r\n\r\n";
    
    
    return HTTP_SRV_SendData(hTcp, badResponse, strlen(badResponse));
}

static NET_ErrorTypeDef HTTP_SRV_SendHeader(HTTP_SRV_SocketHandleTypeDef *hHttpSocket)
{
    static const char pResponseOK[] = "HTTP/1.1 200 OK\r\n";
    
    static const char pConnectionClose[] = "Connection: close\r\n";
    
    static const char pContentTypeHeader[] = "Content-Type: ";
    static const char pContentLengthHeader[] = "Content-Length: ";
    
    static const char pNewLine[] = "\r\n";
    
    
    NET_ErrorTypeDef error = HTTP_SRV_ERR_OK;
    
    
    TCP_SocketHandleTypeDef *hTcpSocket = &(hHttpSocket->TcpSocket);
    HTTP_SRV_UrlEntryTypeDef *pUrlEntry = hHttpSocket->CurrentUrlEntry;
    
    // Creates string representation of length of content.
    char pContentLengthStr[6]; // Five symbols max (uint16) + \0.
    uint8_t contentLengthStrLen = sprintf(pContentLengthStr, "%d", pUrlEntry->ContentLength);
    
    // Computes length of HTTP header.
    uint16_t headerLength = strlen(pResponseOK) +
                            strlen(pConnectionClose) +
                            strlen(pContentTypeHeader) + pUrlEntry->ContentTypeLength + strlen(pNewLine) +
                            strlen(pContentLengthHeader) + contentLengthStrLen + strlen(pNewLine) +
                            strlen(pNewLine);
    
    
    // Allocates buffer for HTTP header.
    char *pHeader = (char *)net_os_malloc(headerLength);
    
    if (pHeader == NULL) return HTTP_SRV_ERR_MEM_ALLOCATION;
    
    
    uint16_t strIdx = 0;
    uint8_t substrLen = 0;
    
    // Response OK string.
    substrLen = strlen(pResponseOK);
    memcpy(pHeader + strIdx, pResponseOK, substrLen);
    strIdx += substrLen;
    
    // Connection close string.
    substrLen = strlen(pConnectionClose);
    memcpy(pHeader + strIdx, pConnectionClose, substrLen);
    strIdx += substrLen;
    
    // Content type header.
    substrLen = strlen(pContentTypeHeader);
    memcpy(pHeader + strIdx, pContentTypeHeader, substrLen);
    strIdx += substrLen;
    
    // Content type body.
    substrLen = pUrlEntry->ContentTypeLength;
    memcpy(pHeader + strIdx, pUrlEntry->ContentType, substrLen);
    strIdx += substrLen;
    
    // New line.
    substrLen = strlen(pNewLine);
    memcpy(pHeader + strIdx, pNewLine, substrLen);
    strIdx += substrLen;
    
    // Content length header.
    substrLen = strlen(pContentLengthHeader);
    memcpy(pHeader + strIdx, pContentLengthHeader, substrLen);
    strIdx += substrLen;
    
    // Content length body.
    substrLen = contentLengthStrLen;
    memcpy(pHeader + strIdx, pContentLengthStr, substrLen);
    strIdx += substrLen;
    
    // New line.
    substrLen = strlen(pNewLine);
    memcpy(pHeader + strIdx, pNewLine, substrLen);
    strIdx += substrLen;
    
    // New line.
    substrLen = strlen(pNewLine);
    memcpy(pHeader + strIdx, pNewLine, substrLen);
    strIdx += substrLen;
    
    
    #ifdef HTTP_SRV_DEBUG
    
    if (strIdx != headerLength) net_os_puts("HTTP_SRV: warn: (strIdx != headerLength)");
    
    #endif
    
    
    // Sends HTTP header.
    error = HTTP_SRV_SendData(hTcpSocket, pHeader, headerLength);
    
    net_os_free(pHeader);
    
    return error;
}

// Sends content or part of content.
static NET_ErrorTypeDef HTTP_SRV_SendContent(HTTP_SRV_SocketHandleTypeDef *hHttpSocket)
{
    NET_ErrorTypeDef error = HTTP_SRV_ERR_OK;
    
    
    TCP_SocketHandleTypeDef *hTcpSocket = &(hHttpSocket->TcpSocket);
    HTTP_SRV_UrlEntryTypeDef *pUrlEntry = hHttpSocket->CurrentUrlEntry;
    uint16_t contentLength = pUrlEntry->ContentLength;
    
    
    // If unsent part of content is exist.
    if (contentLength > hHttpSocket->SentLength)
    {
        uint16_t freeSize = 0;
        
        // Gets free size in TX buffer.
        if ((error = TCP_SendLength(hTcpSocket, &freeSize)) != TCP_ERR_OK) return error;
        
        
        // If free size in TX buffer is non-zero.
        if (freeSize)
        {
            // Compute real size for send.
            uint16_t sizeForSend = contentLength - hHttpSocket->SentLength;
            
            if (sizeForSend > freeSize) sizeForSend = freeSize;
            
            
            // Sends part of data.
            if ((error = HTTP_SRV_SendData(hTcpSocket, (char *)(pUrlEntry->Content + hHttpSocket->SentLength), sizeForSend)) != HTTP_SRV_ERR_OK) return error;
            
            // Updates SentLength field.
            hHttpSocket->SentLength += sizeForSend;
        }
    }
    
    
    return HTTP_SRV_ERR_OK;
}

// Sends raw response.
static NET_ErrorTypeDef HTTP_SRV_SendData(TCP_SocketHandleTypeDef *hTcp, const char *pData, uint16_t length)
{
    NET_ErrorTypeDef error = HTTP_SRV_ERR_OK;
    
    
    #ifdef HTTP_SRV_DEBUG_DATA
    
    uint16_t debugIdx = 0;
    
    while (debugIdx < length)
    {
        net_os_printf("%c", pData[debugIdx++]);
    }
    
    #endif
    
    
    if ((error = TCP_SendData(hTcp, (uint8_t *)pData, length)) != TCP_ERR_OK)
    {
        // Unexpected error.
        error = (NET_ErrorTypeDef)(HTTP_SRV_ERR_LOW_LEVEL | error);
        return error;
    }
    
    
    return HTTP_SRV_ERR_OK;
}

// Sends disconnect.
static NET_ErrorTypeDef HTTP_SRV_Disconnect(HTTP_SRV_SocketHandleTypeDef *hHttpSocket)
{
    NET_ErrorTypeDef error = HTTP_SRV_ERR_OK;
    
    
    // Disconnect already sent.
    if (hHttpSocket->SentDisconnect) return HTTP_SRV_ERR_OK;
    
    if ((error = TCP_DisconnectSocket(&(hHttpSocket->TcpSocket))) != TCP_ERR_OK)
    {
        // Unexpected error.
        error = (NET_ErrorTypeDef)(HTTP_SRV_ERR_LOW_LEVEL | error);
        return error;
    }
    
    hHttpSocket->SentDisconnect = true;
    
    
    return HTTP_SRV_ERR_OK;
}
