#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#include "cmsis_os.h"
#include "net_def.h"

#include "net_os.h"


void *net_os_malloc(size_t size)
{
    #ifdef NET_OS_DEBUG
    
    net_os_printf("malloc: free before:%u\r\n", xPortGetFreeHeapSize());
    
    void *ptr = pvPortMalloc(size);
    
    net_os_printf("malloc: free after:%u\r\n", xPortGetFreeHeapSize());
    
    return ptr;
    
    #else
    
    return pvPortMalloc(size);
    
    #endif    
}

void net_os_free(void *ptr)
{
    #ifdef NET_OS_DEBUG
    
    net_os_printf("free: free before:%u\r\n", xPortGetFreeHeapSize());
    
    vPortFree(ptr);
    
    net_os_printf("free: free after:%u\r\n", xPortGetFreeHeapSize());
    
    #else
    
    vPortFree(ptr);
    
    #endif
}

void net_os_print_ip(const char *pre, NET_IPAddressTypeDef ipAddress, const char *post)
{
    net_os_printf("%s%u.%u.%u.%u%s\r\n", pre, ipAddress.Byte3.Value, ipAddress.Byte2.Value, ipAddress.Byte1.Value, ipAddress.Byte0.Value, post);
}

void NET_OS_EnterCritical(void)
{
    taskENTER_CRITICAL();
}

void NET_OS_ExitCritical(void)
{
    taskEXIT_CRITICAL();
}

void NET_OS_Delay(uint32_t millisec)
{
    osDelay(millisec);
}

uint32_t NET_OS_Ticks(void)
{
    return osKernelSysTick();
}

void NET_OS_Yield(void)
{
    //osThreadYield(); -> Trace: SW Buffer Overrun.
    osDelay(1);
}

void NET_OS_Lock(bool *locker)
{
    while (true)
    {
        while (*locker) NET_OS_Yield();        
        
        NET_OS_EnterCritical();
        
        if (*locker)
        {
            NET_OS_ExitCritical();
            
            continue;
        }
        
        *locker = true;
        
        NET_OS_ExitCritical();
        
        return;
    }
}

void NET_OS_Unlock(bool *locker)
{
    NET_OS_CRITICAL()
    {
        *locker = false;
    }
}

NET_OS_MutexHandle * NET_OS_CreateMutex(void)
{
    osMutexDef(NET_OS_Mutex);
    return osMutexCreate(osMutex(NET_OS_Mutex));
}

NET_OS_RecursiveMutexHandle * NET_OS_CreateRecursiveMutex(void)
{
    osMutexDef(NET_OS_RecursiveMutex);
    return osRecursiveMutexCreate(osMutex(NET_OS_RecursiveMutex));
}

NET_OS_OpStatus NET_OS_TakeMutex(NET_OS_MutexHandle *hMutex, uint32_t time)
{
    return osMutexWait(hMutex, time);
}

NET_OS_OpStatus NET_OS_TakeRecursiveMutex(NET_OS_RecursiveMutexHandle *hMutex, uint32_t time)
{
    return osRecursiveMutexWait(hMutex, time);
}

NET_OS_OpStatus NET_OS_ReleaseMutex(NET_OS_MutexHandle *hMutex)
{
    return osMutexRelease(hMutex);
}

NET_OS_OpStatus NET_OS_ReleaseRecursiveMutex(NET_OS_RecursiveMutexHandle *hMutex)
{
    return osRecursiveMutexRelease(hMutex);
}

NET_OS_OpStatus NET_OS_DeleteMutex(NET_OS_MutexHandle *hMutex)
{
    return osMutexDelete(hMutex);
}

NET_OS_OpStatus NET_OS_DeleteRecursiveMutex(NET_OS_RecursiveMutexHandle *hMutex)
{
    return osMutexDelete(hMutex);
}
