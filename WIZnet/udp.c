#include "udp.h"


#define UDP_HEADER_LENGTH     8


// Open UDP socket if free socket is available.
NET_ErrorTypeDef UDP_OpenSocket(UDP_SocketHandleTypeDef *hUdp, NET_HandleTypeDef *hNet)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    hUdp->Socket.WizHandle = &hNet->WizHandle;
    
    
    if ((error = WIZ_TakeSocket(&(hUdp->Socket))) != WIZ_ERR_OK) return error;
    
    
    if ((error = hUdp->Socket.WizHandle->ChipApi->WIZ_CHIP_API_UDPOpen(&(hUdp->Socket), hUdp->LocalPort)) != WIZ_ERR_OK) return error;
    
    
    return WIZ_ERR_OK;
}

// Wait for free socket if all sokets is busy.
NET_ErrorTypeDef UDP_PollForOpenSocket(UDP_SocketHandleTypeDef *hUdp, uint16_t *pTimeout, NET_HandleTypeDef *hNet)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    NET_PollInfoTypeDef pollInfo = { .Interval = UDP_POLL_INTERVAL, .Timeout = *pTimeout, .BeginTime = xTaskGetTickCount() };
    
    while (true)
    {
        if ((error = UDP_OpenSocket(hUdp, hNet)) != WIZ_ERR_NO_SOCK_AVAILABLE)
        {
            if (error == UDP_ERR_OK)
            {
                UDP_StateTypeDef state = UDP_STATE_UNKNOWN;
                
                if ((error = UDP_GetState(hUdp, &state)) != UDP_ERR_OK) return error;
                
                if (state != UDP_STATE_OPENED && state != UDP_STATE_IN_PROCESS) return UDP_ERR_WRONGSTATE;
            }
            
            return error;
        }
        
        if (!(*pTimeout)) return WIZ_ERR_POLL_TIMEOUT;
        
        *pTimeout = NET_WaitPollInterval(&pollInfo);
    }
}

// Get free size for new UDP data for send in wizchip buffer.
NET_ErrorTypeDef UDP_SendLength(UDP_SocketHandleTypeDef *hUdp, uint16_t *pLength)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    if ((error = hUdp->Socket.WizHandle->ChipApi->WIZ_CHIP_API_SendLength(&(hUdp->Socket), pLength)) != WIZ_ERR_OK) return error;
    
    
    return WIZ_ERR_OK;
}

// Send UDP data if free size in wizchip buffer is less or equal to buffer lenfth.
NET_ErrorTypeDef UDP_SendData(UDP_SocketHandleTypeDef *hUdp, uint8_t *pBuffer, uint16_t bufferLength, NET_EndPointTypeDef *pRemotePoint)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    if ((error = hUdp->Socket.WizHandle->ChipApi->WIZ_CHIP_API_UDPSendData(&(hUdp->Socket), pBuffer, bufferLength, pRemotePoint)) != WIZ_ERR_OK) return error;
    
    
    return WIZ_ERR_OK;
}

// Send data and pool for sent_ok flag or sent_timeout flag.
NET_ErrorTypeDef UDP_PollForSendData(UDP_SocketHandleTypeDef *hUdp, uint8_t *pBuffer, uint16_t bufferLength, NET_EndPointTypeDef *pRemotePoint, uint16_t *pTimeout)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    UDP_StateTypeDef state = UDP_STATE_UNKNOWN;
    
    if ((error = UDP_GetState(hUdp, &state)) != UDP_ERR_OK) return error;
    
    if (state != UDP_STATE_OPENED && state != UDP_STATE_IN_PROCESS) return UDP_ERR_WRONGSTATE;
    
    
    UDP_FlagTypeDef flags = UDP_FLAG_NONE;    
    
    if ((error = UDP_GetFlags(hUdp, &flags, (UDP_FlagTypeDef)(UDP_FLAG_SENT | UDP_FLAG_TIMEOUT))) != WIZ_ERR_OK) return error;
    
    
    #ifdef WIZ_DEBUG
    
    if (flags != UDP_FLAG_NONE)
    {
        net_os_printf("UDP: flags before send data:%u\r\n", flags);
        
        flags = UDP_FLAG_NONE;
    }
    
    #endif
    
    
    if ((error = UDP_SendData(hUdp, pBuffer, bufferLength, pRemotePoint)) != WIZ_ERR_OK) return error;    
    
    
    if ((error = UDP_PollForFlags(hUdp, &flags, (UDP_FlagTypeDef)(UDP_FLAG_SENT | UDP_FLAG_TIMEOUT), pTimeout)) != WIZ_ERR_OK) return error;
    
    if (flags & UDP_FLAG_TIMEOUT)
    {
        error = WIZ_ERR_SEND_TIMEOUT;
    }
    
    
    state = UDP_STATE_UNKNOWN;
    
    if ((error = UDP_GetState(hUdp, &state)) != UDP_ERR_OK) return error;
    
    if (state != UDP_STATE_OPENED && state != UDP_STATE_IN_PROCESS) return UDP_ERR_WRONGSTATE;
    
    
    return WIZ_ERR_OK;
}

// Get total length of received UDP data in wizchip buffer.
NET_ErrorTypeDef UDP_ReceiveLength(UDP_SocketHandleTypeDef *hUdp, uint16_t *pLength)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    if ((error = hUdp->Socket.WizHandle->ChipApi->WIZ_CHIP_API_ReceiveLength(&(hUdp->Socket), pLength)) != WIZ_ERR_OK) return error;
    
    
    return WIZ_ERR_OK;
}

// Get next UDP datagram info from wizchip buffer.
NET_ErrorTypeDef UDP_ReceiveDatagram(UDP_SocketHandleTypeDef *hUdp, UDP_DatagramTypeDef *pDatagram)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;    
    
    
    // Compile-time-length array, placed on stack.
    uint8_t udpInfo[UDP_HEADER_LENGTH];
    
    uint16_t udpInfoReceivedLength = 0;
    
    
    if ((error = hUdp->Socket.WizHandle->ChipApi->WIZ_CHIP_API_ReceiveData(&(hUdp->Socket), udpInfo, UDP_HEADER_LENGTH, &udpInfoReceivedLength)) != WIZ_ERR_OK) return error;
    
    
    pDatagram->ReceivedLength = 0;
    pDatagram->UdpSocket = hUdp;
    
    NET_Union16TypeDef *length = (NET_Union16TypeDef *)(&(pDatagram->FullLength));
    
    length->Byte0.Value = udpInfo[7];
    length->Byte1.Value = udpInfo[6];
    
    pDatagram->RemotePoint.IPAddress.Byte3.Value = udpInfo[0];
    pDatagram->RemotePoint.IPAddress.Byte2.Value = udpInfo[1];
    pDatagram->RemotePoint.IPAddress.Byte1.Value = udpInfo[2];
    pDatagram->RemotePoint.IPAddress.Byte0.Value = udpInfo[3];
    
    pDatagram->RemotePoint.Port.Byte1.Value = udpInfo[4];
    pDatagram->RemotePoint.Port.Byte0.Value = udpInfo[5];
    
    
    return WIZ_ERR_OK;
}

// Pool for received_ok flag and receive datagram.
NET_ErrorTypeDef UDP_PollForReceiveDatagram(UDP_SocketHandleTypeDef *hUdp, UDP_DatagramTypeDef *pDatagram, uint16_t *pTimeout)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    UDP_StateTypeDef state = UDP_STATE_UNKNOWN;
    
    if ((error = UDP_GetState(hUdp, &state)) != UDP_ERR_OK) return error;
    
    if (state != UDP_STATE_OPENED && state != UDP_STATE_IN_PROCESS) return UDP_ERR_WRONGSTATE;
    
    
    UDP_FlagTypeDef flags = UDP_FLAG_NONE;
    
    if ((error = UDP_PollForFlags(hUdp, &flags, UDP_FLAG_RECEIVED, pTimeout)) != WIZ_ERR_OK) return error;
    
    
    if ((error = UDP_ReceiveDatagram(hUdp, pDatagram)) != WIZ_ERR_OK) return error;
    
    
    state = UDP_STATE_UNKNOWN;
    
    if ((error = UDP_GetState(hUdp, &state)) != UDP_ERR_OK) return error;
    
    if (state != UDP_STATE_OPENED && state != UDP_STATE_IN_PROCESS) return UDP_ERR_WRONGSTATE;
    
    
    return WIZ_ERR_OK;
}

// Recieve UDP data from specified datagram in wizchip buffer.
NET_ErrorTypeDef UDP_ReceiveData(UDP_DatagramTypeDef *pDatagram, uint8_t *pBuffer, uint16_t bufferLength, uint16_t *pReceivedLength)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    uint16_t availableLength = pDatagram->FullLength - pDatagram->ReceivedLength;
    uint16_t length = bufferLength > availableLength ? availableLength : bufferLength;    
    
    if (length == 0)
    {
        *pReceivedLength = 0;
        
        return WIZ_ERR_OK;
    }
    
    
    if ((error = pDatagram->UdpSocket->Socket.WizHandle->ChipApi->WIZ_CHIP_API_ReceiveData(&(pDatagram->UdpSocket->Socket), pBuffer, length, pReceivedLength)) != WIZ_ERR_OK) return error;
    
    
    pDatagram->ReceivedLength += *pReceivedLength;
    
    
    return WIZ_ERR_OK;
}

// Flush UDP data from specified datagram in wizchip buffer.
NET_ErrorTypeDef UDP_FlushDatagram(UDP_DatagramTypeDef *pDatagram)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    // Compile-time-length array, placed on stack.
    uint8_t buffer[UDP_FLUSH_BUFFER_LEN];
    
    uint16_t lengthForReceive = 0;
    uint16_t udpReceivedLength = 0;
    
    
    while ((lengthForReceive = (pDatagram->FullLength - pDatagram->ReceivedLength)) != 0)
    {
        if (lengthForReceive > UDP_FLUSH_BUFFER_LEN) lengthForReceive = UDP_FLUSH_BUFFER_LEN;        
    
        if ((error = UDP_ReceiveData(pDatagram, buffer, lengthForReceive, &udpReceivedLength)) != WIZ_ERR_OK) return error;        
    }
    
    
    return WIZ_ERR_OK;
}

// Close UDP socket.
NET_ErrorTypeDef UDP_CloseSocket(UDP_SocketHandleTypeDef *hUdp)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    if ((error = hUdp->Socket.WizHandle->ChipApi->WIZ_CHIP_API_CloseSocket(&(hUdp->Socket))) != WIZ_ERR_OK) return error;
    
    
    if ((error = WIZ_ReleaseSocket(&(hUdp->Socket))) != WIZ_ERR_OK) return error;
    
    
    return WIZ_ERR_OK;
}

// Get TCP state of wizchip.
NET_ErrorTypeDef UDP_GetState(UDP_SocketHandleTypeDef *hUdp, UDP_StateTypeDef *pState)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    WIZ_SocketStateTypeDef rawState = WIZ_SOCKET_STATE_NONE;

    if ((error = hUdp->Socket.WizHandle->ChipApi->WIZ_CHIP_API_GetSocketState(&(hUdp->Socket), &rawState)) != WIZ_ERR_OK) return error;
    
    switch (rawState)
    {
        case WIZ_SOCKET_STATE_CLOSED: *pState = UDP_STATE_CLOSED; break;
        
        case WIZ_SOCKET_STATE_UDP: *pState = UDP_STATE_OPENED; break;
        
        case WIZ_SOCKET_STATE_ARP_UDP: *pState = (hUdp->Socket.WizHandle->ChipApi->ChipType == WIZ_CHIP_5100) ? UDP_STATE_IN_PROCESS : UDP_STATE_UNKNOWN; break;
        
        default:  *pState = UDP_STATE_UNKNOWN; break;
    }
    
    
    return WIZ_ERR_OK;
}

// Get UDP flags of wizchip for specified mask.
NET_ErrorTypeDef UDP_GetFlags(UDP_SocketHandleTypeDef *hUdp, UDP_FlagTypeDef *pFlags, UDP_FlagTypeDef flagsMask)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;    
    
    
    if ((error = hUdp->Socket.WizHandle->ChipApi->WIZ_CHIP_API_GetSocketFlags(&(hUdp->Socket), (WIZ_SocketFlagTypeDef *)pFlags, (WIZ_SocketFlagTypeDef)flagsMask)) != WIZ_ERR_OK) return error;
    
    
    return WIZ_ERR_OK;
}

// Poll for one or several flags of wizchip for specified mask.
NET_ErrorTypeDef UDP_PollForFlags(UDP_SocketHandleTypeDef *hUdp, UDP_FlagTypeDef *pFlags, UDP_FlagTypeDef flagsMask, uint16_t *pTimeout)
{
    NET_ErrorTypeDef error = WIZ_ERR_OK;
    
    
    NET_PollInfoTypeDef pollInfo = { .Interval = UDP_POLL_INTERVAL, .Timeout = *pTimeout, .BeginTime = xTaskGetTickCount() };
    
    while (true)
    {
        if ((error = UDP_GetFlags(hUdp, pFlags, flagsMask)) != WIZ_ERR_OK) return error;
        
        if (*pFlags & flagsMask) return WIZ_ERR_OK;
        
        if (!(*pTimeout)) return WIZ_ERR_POLL_TIMEOUT;
        
        *pTimeout = NET_WaitPollInterval(&pollInfo);
    }
}
