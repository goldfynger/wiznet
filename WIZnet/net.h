#ifndef __NET_H
#define __NET_H


#include <stdbool.h>
#include <stdint.h>

#include "net_def.h"
#include "wizchip.h"


// Common network params.
typedef struct
{
    NET_IPAddressTypeDef    IPAddress;
    NET_IPAddressTypeDef    SubnetMask;
    NET_IPAddressTypeDef    DefaultGateway;
    
    NET_IPAddressTypeDef    DHCPServerAddress;
    uint32_t                DHCPLeaseTime;
    uint32_t                DHCPLeftTime;
    
    NET_IPAddressTypeDef    DNSServerAddress;
    
    NET_IPAddressTypeDef    SNTPServerAddress;
    char                    *SNTPServerName;
    
    char                    *HostName;
    
    uint8_t                 MacAddress[6];
    
    NET_FlagTypeDef         Flags;
    
    bool                    __Locker;
}
NET_ParamsTypeDef;

typedef struct NET_OsApiTypeDef NET_OsApiTypeDef;

typedef struct
{
    NET_OsApiTypeDef        *OsApi;
    
    WIZ_HandleTypeDef       WizHandle;
    
    NET_ParamsTypeDef       Params;    
    
    NET_PortTypeDef         ShortLivedPort;
    
    bool                    __ShortLivedPortLocker;
}
NET_HandleTypeDef;

typedef struct
{
    NET_HandleTypeDef       NetHandle;
    
    WIZ_SocketHandleTypeDef WizSocketHandle;
}
NET_SocketHandleTypeDef;

struct NET_OsApiTypeDef
{
    
    
    bool __IsFilled;
};


NET_ErrorTypeDef        NET_Init                (NET_HandleTypeDef *hNet);
NET_PortTypeDef         NET_GetFreePort         (NET_HandleTypeDef *hNet, NET_PortModeTypeDef portMode);
uint16_t                NET_WaitPollInterval    (NET_PollInfoTypeDef *pPollInfo);

NET_IPAddressTypeDef    NET_GetIPAddress        (NET_HandleTypeDef *hNet);
NET_IPAddressTypeDef    NET_GetSubnetMask       (NET_HandleTypeDef *hNet);
NET_IPAddressTypeDef    NET_GetDefaultGateway   (NET_HandleTypeDef *hNet);
NET_IPAddressTypeDef    NET_GetDNSServerAddress (NET_HandleTypeDef *hNet);
NET_IPAddressTypeDef    NET_GetDHCPServerAddress(NET_HandleTypeDef *hNet);
uint32_t                NET_GetDHCPLeaseTime    (NET_HandleTypeDef *hNet);
uint32_t                NET_GetDHCPLeftTime     (NET_HandleTypeDef *hNet);
NET_IPAddressTypeDef    NET_GetSNTPServerAddress(NET_HandleTypeDef *hNet);
char                    * NET_GetSNTPServerName (NET_HandleTypeDef *hNet);
char                    * NET_GetHostName       (NET_HandleTypeDef *hNet);
uint8_t                 * NET_GetMacAddress     (NET_HandleTypeDef *hNet);
NET_FlagTypeDef         NET_GetFlags            (NET_HandleTypeDef *hNet);

NET_ErrorTypeDef        NET_SetIPAddress        (NET_HandleTypeDef *hNet, NET_IPAddressTypeDef ipAddress);
NET_ErrorTypeDef        NET_SetSubnetMask       (NET_HandleTypeDef *hNet, NET_IPAddressTypeDef subnetMask);
NET_ErrorTypeDef        NET_SetDefaultGateway   (NET_HandleTypeDef *hNet, NET_IPAddressTypeDef defaultGateway);
void                    NET_SetDNSServerAddress (NET_HandleTypeDef *hNet, NET_IPAddressTypeDef serverAddress);
void                    NET_SetDHCPServerAddress(NET_HandleTypeDef *hNet, NET_IPAddressTypeDef serverAddress);
void                    NET_SetDHCPLeaseTime    (NET_HandleTypeDef *hNet, uint32_t time);
void                    NET_SetDHCPLeftTime     (NET_HandleTypeDef *hNet, uint32_t time);
void                    NET_SetSNTPServerAddress(NET_HandleTypeDef *hNet, NET_IPAddressTypeDef serverAddress);
void                    NET_SetSNTPServerName   (NET_HandleTypeDef *hNet, char *pServerName);
void                    NET_SetHostName         (NET_HandleTypeDef *hNet, char *pHostName);
NET_ErrorTypeDef        NET_SetMacAddress       (NET_HandleTypeDef *hNet, uint8_t *pMacAddress);
void                    NET_SetFlags            (NET_HandleTypeDef *hNet, NET_FlagTypeDef flags);


#endif /* __NET_H */
