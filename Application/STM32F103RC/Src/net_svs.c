#include <string.h>

#include "dns.h"
#include "narodmon.h"
#include "net.h"
#include "net_def.h"
#include "net_os.h"
#include "sntp.h"
#include "w5100.h"
#include "wizchip.h"
#include "wizchip_hal.h"

#include "net_svs.h"


extern void _Error_Handler(char *, int);

static void NET_SVS_Init(NET_SVS_HandleTypeDef *hNetSvs);
void NET_SVS_NarodmonStartTask(void * argument);
void NET_SVS_NarodmonDataStartTask(void * argument);


void NET_SVS_StartTask(void const * argument)
{
    NET_ErrorTypeDef error = NET_ERR_OK;
    
    
    NET_SVS_HandleTypeDef *hNetSvs = (NET_SVS_HandleTypeDef *)argument;
    
    
    NET_SVS_Init(hNetSvs);
    
    
    hNetSvs->DnsHandle = DNS_AllocateHandle(&hNetSvs->NetHandle, 8);
    
    if (hNetSvs->DnsHandle == NULL)
    {
        _Error_Handler(__FILE__, __LINE__);
    }
    
    
    TaskHandle_t nmTask = NULL;
    
    if (xTaskCreate(NET_SVS_NarodmonStartTask, "NmTask", configMINIMAL_STACK_SIZE * 2, hNetSvs, tskIDLE_PRIORITY + 1, &nmTask) != pdPASS)
    {
        _Error_Handler(__FILE__, __LINE__);
    }
    
    
    while (true)
    {
        vTaskDelay(1000);
    }
    
    
    SNTP_HandleTypeDef sntpHandle;
    memset(&sntpHandle, 0, sizeof(SNTP_HandleTypeDef));
    sntpHandle.NetHandle = &hNetSvs->NetHandle;
    sntpHandle.DnsHandle = hNetSvs->DnsHandle;
    
    SNTP_DateTimeTypeDef sntpDateTime;
    memset(&sntpDateTime, 0, sizeof(SNTP_DateTimeTypeDef));
    
    
    while (true)
    {
        if ((error = SNTP_DetermineDateTime(&(sntpHandle), SNTP_OFFSET_MSK, &sntpDateTime)) != WIZ_ERR_OK)
        {
            net_os_printf("Err %08X\r\n", error);
        
            _Error_Handler(__FILE__, __LINE__);
        }
        
        NET_OS_Delay(10000);
    }
}

static void NET_SVS_Init(NET_SVS_HandleTypeDef *hNetSvs)
{
    NET_ErrorTypeDef error = NET_ERR_OK;
    
    
    NET_HandleTypeDef *hNet = &(hNetSvs->NetHandle);
    WIZ_HandleTypeDef *hWiz = &(hNet->WizHandle);
    
    
    WIZ_HAL_FillCallbacks(&(hWiz->Hal));
    
    W5100_GetChipApi(&(hWiz->ChipApi));
    
    if ((error = WIZ_Init(hWiz)) != WIZ_ERR_OK)
    {
        net_os_printf("Err %u\r\n", error);
        
        _Error_Handler(__FILE__, __LINE__);
    }
    
    WIZ_MemorySizeTypeDef memSizes[W5100_SOCKET_COUNT] = { WIZ_MEMSIZE_2, WIZ_MEMSIZE_2, WIZ_MEMSIZE_2, WIZ_MEMSIZE_2 };
    
    if ((error = WIZ_SetMemorySizes(hWiz, memSizes, memSizes)) != WIZ_ERR_OK)
    {
        net_os_printf("Err %u\r\n", error);
        
        _Error_Handler(__FILE__, __LINE__);
    }
    
    
    NET_Init(hNet);
    
    uint8_t macAddr[6] = { 0xE1, 0x17, 0x65, 0xFA, 0xD6, 0x00 };
    
    if ((error = NET_SetMacAddress(hNet, macAddr)) != WIZ_ERR_OK)
    {
        net_os_printf("Err %u\r\n", error);
        
        _Error_Handler(__FILE__, __LINE__);
    }
    
    if ((error = NET_SetIPAddress(hNet, NET_CreateIPAddress(192, 168, 2, 240))) != WIZ_ERR_OK)
    {
        net_os_printf("Err %u\r\n", error);
        
        _Error_Handler(__FILE__, __LINE__);
    }
    
    if ((error = NET_SetDefaultGateway(hNet, NET_CreateIPAddress(192, 168, 2, 1))) != WIZ_ERR_OK)
    {
        net_os_printf("Err %u\r\n", error);
        
        _Error_Handler(__FILE__, __LINE__);
    }
    
    if ((error = NET_SetSubnetMask(hNet, NET_CreateIPAddress(255, 255, 255, 0))) != WIZ_ERR_OK)
    {
        net_os_printf("Err %u\r\n", error);
        
        _Error_Handler(__FILE__, __LINE__);
    }
    
    NET_SetDNSServerAddress(hNet, NET_CreateIPAddress(192, 168, 2, 1));
    NET_SetSNTPServerName(hNet, "0.ru.pool.ntp.org");
}

void NET_SVS_NarodmonStartTask(void * argument)
{
    NET_ErrorTypeDef error = NET_ERR_OK;
    
    
    NET_SVS_HandleTypeDef *hNetSvs = (NET_SVS_HandleTypeDef *)argument;
    
    
    net_os_printf("NET_SVS: narodmon begin free:%u\r\n", xPortGetFreeHeapSize());
    
    
    hNetSvs->NmHandle = NM_AllocateHandle(&hNetSvs->NetHandle, hNetSvs->DnsHandle, 16);
    
    if (hNetSvs->NmHandle == NULL)
    {
        _Error_Handler(__FILE__, __LINE__);
    }
    
    
    char pDeviceName[] = "DevOne";
    
    if ((error = NM_SetName(hNetSvs->NmHandle, pDeviceName, sizeof(pDeviceName))) != NM_ERR_OK)
    {
        NM_FreeHandle(hNetSvs->NmHandle);
        
        net_os_printf("Err %u\r\n", error);
        
        _Error_Handler(__FILE__, __LINE__);
    }
    
    
    if ((error = NM_SetPosition(hNetSvs->NmHandle, (float)45.044259, (float)41.969752, (float)417.68)) != NM_ERR_OK)
    {
        NM_FreeHandle(hNetSvs->NmHandle);
        
        net_os_printf("Err %u\r\n", error);
        
        _Error_Handler(__FILE__, __LINE__);
    }
    
    
    TaskHandle_t nmDataTask = NULL;
    
    if (xTaskCreate(NET_SVS_NarodmonDataStartTask, "NmDataTask", configMINIMAL_STACK_SIZE, hNetSvs, tskIDLE_PRIORITY + 1, &nmDataTask) != pdPASS)
    {
        _Error_Handler(__FILE__, __LINE__);
    }
    
    
    if ((error = NM_Run(hNetSvs->NmHandle)) != NM_ERR_OK)
    {
        NM_FreeHandle(hNetSvs->NmHandle);
        
        net_os_printf("Err %u\r\n", error);
        
        _Error_Handler(__FILE__, __LINE__);
    }
    
    
    vTaskDelete(nmDataTask);
    
    NM_FreeSensorHandle(hNetSvs->NmSensorOneHandle);
    NM_FreeSensorHandle(hNetSvs->NmSensorTwoHandle);
    
    NM_FreeHandle(hNetSvs->NmHandle);
    
    
    net_os_printf("NET_SVS: narodmon end free:%u\r\n", xPortGetFreeHeapSize());
    
    
    while (true) vTaskDelay(portMAX_DELAY);
}

void NET_SVS_NarodmonDataStartTask(void * argument)
{
    NET_ErrorTypeDef error = NET_ERR_OK;
    
    
    NET_SVS_HandleTypeDef *hNetSvs = (NET_SVS_HandleTypeDef *)argument;
    
    
    vTaskDelay(1000);
    
    
    char pSensorOneMac[] = "SensOne";
    char pSensorTwoMac[] = "SensTwo";
    
    hNetSvs->NmSensorOneHandle = NM_AllocateSensorHandle(hNetSvs->NmHandle, pSensorOneMac, sizeof(pSensorOneMac));
    hNetSvs->NmSensorTwoHandle = NM_AllocateSensorHandle(hNetSvs->NmHandle, pSensorTwoMac, sizeof(pSensorTwoMac));
    
    if (hNetSvs->NmSensorOneHandle == NULL || hNetSvs->NmSensorTwoHandle == NULL)
    {
        _Error_Handler(__FILE__, __LINE__);
    }
    
    
    if ((error = NM_SetSensorName(hNetSvs->NmSensorOneHandle, pSensorOneMac, sizeof(pSensorOneMac))) != NM_ERR_OK)
    {        
        net_os_printf("Err %u\r\n", error);
    
        _Error_Handler(__FILE__, __LINE__);
    }
    
    if ((error = NM_SetSensorName(hNetSvs->NmSensorTwoHandle, pSensorTwoMac, sizeof(pSensorTwoMac))) != NM_ERR_OK)
    {        
        net_os_printf("Err %u\r\n", error);
    
        _Error_Handler(__FILE__, __LINE__);
    }
    
    
    float value = 12345.6789;
    time_t time = 1234567890;
    
    while (true)
    {
        if ((error = NM_AddSensorValue(hNetSvs->NmSensorOneHandle, value, time)) != NM_ERR_OK)
        {
            NM_FreeSensorHandle(hNetSvs->NmSensorOneHandle);
            
            net_os_printf("Err %u\r\n", error);
        
            _Error_Handler(__FILE__, __LINE__);
        }
        
        value += 123.456;
        time += 60;
        
        if ((error = NM_AddSensorValue(hNetSvs->NmSensorTwoHandle, value, time)) != NM_ERR_OK)
        {
            NM_FreeSensorHandle(hNetSvs->NmSensorTwoHandle);
            
            net_os_printf("Err %u\r\n", error);
        
            _Error_Handler(__FILE__, __LINE__);
        }
        
        value += 456.123;
        time += 120;
        
        vTaskDelay(NM_MIN_SENDING_INTERVAL / 5 * configTICK_RATE_HZ); 
    }
}
