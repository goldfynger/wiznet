#ifndef __NET_SVS_H
#define __NET_SVS_H


#include "dns.h"
#include "narodmon.h"
#include "net.h"


typedef struct
{
    NET_HandleTypeDef       NetHandle;
    
    DNS_HandleTypeDef       *DnsHandle;
    
    NM_HandleTypeDef        *NmHandle;
    
    NM_SensorHandleTypeDef  *NmSensorOneHandle;
    
    NM_SensorHandleTypeDef  *NmSensorTwoHandle;
}
NET_SVS_HandleTypeDef;


void NET_SVS_StartTask(void const * argument);


#endif /* __NET_SVS_H */
