#include <stdbool.h>
#include <stddef.h>
#include <string.h>

#include "cmsis_os.h"
#include "main.h"
#include "stm32f1xx_hal.h"

#include "WIZnet/http_srv.h"
#include "WIZnet/net.h"
#include "WIZnet/net_os.h"
#include "WIZnet/sntp.h"
#include "WIZnet/wizchip.h"

#include "net_app.h"


extern SPI_HandleTypeDef hspi1;
extern osMutexId hspi1RecursiveMutexHandle;


const char _contentTypeHtml[]       = "text/html";
const char _contentTypeCss[]        = "text/css";
const char _contentTypeJs[]         = "application/javascript";
const char _contentTypeJson[]       = "application/json";
const char _contentTypePng[]        = "image/png";
const char _contentTypeSvg[]        = "image/svg+xml";

const size_t _contentTypeHtmlSize   = sizeof(_contentTypeHtml) - 1;
const size_t _contentTypeCssSize    = sizeof(_contentTypeCss) - 1;
const size_t _contentTypeJsSize     = sizeof(_contentTypeJs) - 1;
const size_t _contentTypeJsonSize   = sizeof(_contentTypeJson) - 1;
const size_t _contentTypePngSize    = sizeof(_contentTypePng) - 1;
const size_t _contentTypeSvgSize    = sizeof(_contentTypeSvg) - 1;

const char _networkJs[] =
{
    "var networkInfoUrl=\"network.json\";\r\nfunction loadNetwork(){document.title=\"Network\";$(\"#content\").empty();var a=[[\"ipaddr\",\"Device IP address\"],[\"gateway\",\"Default gateway\"],[\""
    "subnetmsk\",\"Subnet mask\"],[\"macaddr\",\"MAC address\"]],b;for(b in a)$(\"#content\").append($(\"<div></div>\").attr(\"class\",\"pair\").append($(\"<label></label>\").attr(\"class\",\"key\""
    ").attr(\"for\",a[b][0]).text(a[b][1]),$(\"<input>\").attr(\"id\",a[b][0]).attr(\"class\",\"val\").attr(\"type\",\"text\")),\"<bv>\");$(\"#content\").append($(\"<div></div>\").attr(\"class\",\""
    "pair\").append($(\"<div></div>\").attr(\"id\",\r\n\"empty\"),$(\"<button></button>\").attr(\"id\",\"submit\").attr(\"type\",\"button\").text(\"Submit\").click(submitNetwork)));$.get(networkInf"
    "oUrl,function(a){$(\"#ipaddr\").val(a.ipaddr);$(\"#gateway\").val(a.gateway);$(\"#subnetmsk\").val(a.subnetmsk);$(\"#macaddr\").val(a.macaddr)},\"json\")}\r\nfunction submitNetwork(){function "
    "a(){rebootNetwork()}if(validateNetwork()){var b={ipaddr:$(\"#ipaddr\").val(),gateway:$(\"#gateway\").val(),subnetmsk:$(\"#subnetmsk\").val(),macaddr:$(\"#macaddr\").val()};$.postJSON(networkIn"
    "foUrl,b,a)}}function rebootNetwork(){$(\"#content\").empty().append($(\"<div></div>\").attr(\"id\",\"reboot\").text(\"Applying settings. Page will be reloaded within 5 seconds.\"));setTimeout("
    "function(){loadNetwork()},5E3)}\r\nfunction validateNetwork(){var a=[[\"#ipaddr\",validateIpAddr,!1],[\"#gateway\",validateIpAddr,!1],[\"#subnetmsk\",validateIpAddr,!1],[\"#macaddr\",validateM"
    "acAddr,!1]],b;for(b in a){var c=$(a[b][0]),d=(0,a[b][1])(c.val());c.css({border:d?\"\":\"2px solid red\"});a[b][2]=d}for(b in a)if(!a[b][2])return!1;return!0}\r\nfunction validateIpAddr(a){ret"
    "urn null!=a.match(/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/)}functio"
    "n validateMacAddr(a){return null!=a.match(/^([0-9A-F]{2}[:]){5}([0-9A-F]{2})$/)};"
};

const size_t _networkJsSize = sizeof(_networkJs) - 1;

const char _networkJsUrl[] = "/network.js";

const size_t _networkJsUrlSize = sizeof(_networkJsUrl) - 1;

const uint8_t _faviconPng[] =
{
    0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A, 0x00, 0x00, 0x00, 0x0D, 0x49, 0x48, 0x44, 0x52, 0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x10, 0x08, 0x06, 0x00, 0x00, 0x00, 0x1F, 0xF3, 0xFF,
    0x61, 0x00, 0x00, 0x00, 0x04, 0x73, 0x42, 0x49, 0x54, 0x08, 0x08, 0x08, 0x08, 0x7C, 0x08, 0x64, 0x88, 0x00, 0x00, 0x00, 0x09, 0x70, 0x48, 0x59, 0x73, 0x00, 0x00, 0x0B, 0x13, 0x00, 0x00, 0x0B,
    0x13, 0x01, 0x00, 0x9A, 0x9C, 0x18, 0x00, 0x00, 0x01, 0xF6, 0x49, 0x44, 0x41, 0x54, 0x38, 0x8D, 0x7D, 0x91, 0xBF, 0x4B, 0x95, 0x71, 0x18, 0xC5, 0x3F, 0xE7, 0x79, 0x5F, 0xBD, 0x9A, 0x15, 0x11,
    0x05, 0x15, 0x85, 0x3F, 0x4A, 0x87, 0x8A, 0x02, 0xB7, 0x26, 0xA1, 0xA5, 0xA5, 0xA9, 0x7F, 0x20, 0x68, 0xB0, 0xA0, 0xB6, 0x96, 0x28, 0x82, 0x68, 0x8D, 0x96, 0x5A, 0x9A, 0x02, 0x77, 0xFF, 0x01,
    0x97, 0x30, 0xAA, 0xB1, 0x30, 0x88, 0x52, 0xBB, 0x61, 0x3F, 0x08, 0x84, 0x50, 0xCC, 0xA2, 0xAE, 0x75, 0xDF, 0xE7, 0x34, 0xDC, 0xAE, 0x69, 0xA2, 0xCF, 0xF2, 0x5D, 0xCE, 0xE7, 0x7C, 0x9F, 0xE7,
    0x1C, 0xD8, 0x62, 0x72, 0xBA, 0xE7, 0x44, 0xD6, 0x7B, 0x8E, 0x6F, 0xA5, 0xD1, 0xA6, 0xF0, 0xDB, 0x6D, 0xC3, 0xB2, 0x9F, 0x03, 0xB8, 0x74, 0x5F, 0x0C, 0x34, 0x3E, 0x6C, 0x69, 0x90, 0xAF, 0x3B,
    0x87, 0x54, 0x16, 0xD7, 0x0D, 0x2F, 0xA9, 0x72, 0x4A, 0x45, 0x3C, 0x5A, 0x2B, 0x34, 0x79, 0xC1, 0xC4, 0x31, 0x60, 0x45, 0xF2, 0x83, 0x18, 0x6C, 0x7C, 0x02, 0x28, 0x57, 0x9D, 0xCA, 0x38, 0x07,
    0x9C, 0x17, 0x90, 0x45, 0xCC, 0x90, 0x5C, 0x71, 0xF8, 0x24, 0x15, 0xA8, 0xB3, 0x9C, 0x50, 0xC5, 0x78, 0xFB, 0x37, 0x27, 0x8B, 0xC0, 0xDD, 0x75, 0x06, 0x46, 0x0D, 0x01, 0x49, 0x4E, 0x41, 0xF1,
    0xCC, 0xF8, 0x1E, 0xA2, 0xA2, 0x94, 0x5C, 0x55, 0xE7, 0x91, 0x6E, 0x18, 0x5F, 0x08, 0x73, 0x18, 0x7B, 0xAA, 0xCD, 0xAD, 0x1A, 0x60, 0xBF, 0x48, 0x79, 0x8A, 0x28, 0x9E, 0x58, 0x1E, 0xCD, 0x83,
    0x0A, 0x77, 0x47, 0x00, 0xC4, 0x77, 0x88, 0xF9, 0xBC, 0x25, 0x7C, 0x2D, 0x93, 0x33, 0xAA, 0x54, 0x6F, 0x63, 0x01, 0x90, 0xB3, 0x5D, 0x23, 0x92, 0x1E, 0x8B, 0x18, 0xA3, 0xF2, 0x65, 0xEF, 0x57,
    0xCD, 0xDD, 0xFF, 0xF2, 0xCD, 0xED, 0x90, 0x7B, 0xA3, 0xC3, 0xE9, 0x9B, 0x0A, 0x9E, 0xAA, 0x43, 0xEF, 0xB3, 0xDE, 0x39, 0x08, 0x10, 0x39, 0xDB, 0x35, 0x22, 0x34, 0xD9, 0x5A, 0xC2, 0x7D, 0x04,
    0x99, 0xDD, 0x1B, 0xCB, 0xF1, 0x76, 0x08, 0xC5, 0x2E, 0x07, 0x87, 0x00, 0x94, 0xC5, 0xAC, 0xDF, 0xD4, 0xFA, 0x02, 0xF9, 0xF3, 0x9A, 0x52, 0x2A, 0x72, 0x93, 0x6A, 0xFD, 0xF7, 0x6D, 0xAE, 0x39,
    0xBB, 0xAB, 0x68, 0x44, 0x0C, 0xAE, 0xD4, 0x5D, 0xBA, 0xAF, 0x85, 0xE7, 0x04, 0xF0, 0x2B, 0xBE, 0x7B, 0x03, 0x1F, 0xCB, 0x26, 0xCD, 0x47, 0x15, 0x4C, 0x66, 0xE6, 0x92, 0x9B, 0xE5, 0x1E, 0xF5,
    0xFF, 0x98, 0x6F, 0x85, 0x34, 0xD0, 0xF8, 0x60, 0xEB, 0xAC, 0xCA, 0x98, 0x90, 0x75, 0x2B, 0xE6, 0xFD, 0x3B, 0xBE, 0x1A, 0x35, 0x41, 0xBF, 0xA1, 0x58, 0x34, 0x5A, 0x70, 0x53, 0xCE, 0xDB, 0xB2,
    0xC6, 0x14, 0xC5, 0xE9, 0x38, 0xFA, 0x6D, 0x61, 0x35, 0xC4, 0xD6, 0xF6, 0x39, 0x0C, 0xE0, 0xF0, 0x25, 0x25, 0x57, 0xF5, 0xA5, 0x5A, 0xE8, 0x98, 0x4B, 0x3A, 0xDE, 0x27, 0x5A, 0xF4, 0x9C, 0xC8,
    0x51, 0x8B, 0x3B, 0xAD, 0x54, 0xD9, 0xB7, 0xA1, 0x46, 0x59, 0x05, 0x82, 0x80, 0xFE, 0xB4, 0x2F, 0x86, 0x75, 0x3F, 0xD3, 0xBD, 0x28, 0x4B, 0xB9, 0x7C, 0x22, 0xC5, 0x43, 0xB5, 0xD3, 0x09, 0xFF,
    0xE3, 0x56, 0xAB, 0x7A, 0xD3, 0x7D, 0x80, 0x82, 0x53, 0x58, 0x3F, 0xC9, 0xE6, 0x3B, 0x15, 0xC5, 0xF4, 0xFA, 0x0C, 0x35, 0x2C, 0xF9, 0x88, 0x9D, 0x3B, 0x60, 0x65, 0x3C, 0x86, 0x58, 0x5E, 0x67,
    0xF0, 0xFF, 0x64, 0xBD, 0x73, 0x50, 0x59, 0xCC, 0x66, 0x7A, 0x39, 0x6A, 0xB5, 0x5E, 0xF5, 0x7F, 0x5D, 0xDA, 0x4C, 0xBB, 0xE9, 0x78, 0xA6, 0x36, 0x90, 0xAF, 0x76, 0xEE, 0xDE, 0x4A, 0xF3, 0x07,
    0x06, 0x3E, 0xE5, 0xFC, 0xED, 0x22, 0xFC, 0xA9, 0x00, 0x00, 0x00, 0x00, 0x49, 0x45, 0x4E, 0x44, 0xAE, 0x42, 0x60, 0x82
};

const size_t _faviconPngSize = sizeof(_faviconPng) - 1;

const char _faviconPngUrl[] = "/favicon.ico";

const size_t _faviconPngUrlSize = sizeof(_faviconPngUrl) - 1;

osThreadId sntpClntTask;
osThreadId httpSrvTask;


extern void _Error_Handler(char * file, int line);


void NET_APP_RunNetwork(void);
void NET_APP_HaltNetwork(void);

void NET_APP_StartHttpServer(void const * argument);
void NET_APP_StartSntpClient(void const * argument);

NET_ErrorTypeDef NET_APP_SpiTransmitReceiveSync(uint8_t *pBuffer, uint16_t length);
NET_ErrorTypeDef NET_APP_SpiTakeMutex(void);
NET_ErrorTypeDef NET_APP_SpiReleaseMutex(void);

void NET_APP_WizReset(void);
void NET_APP_WizRun(void);
void NET_APP_WizActivateNss(void);
void NET_APP_WizDeactivateNss(void);


void NET_APP_StartNetwork(void const * argument)
{
    NET_ErrorTypeDef error = NET_ERR_OK;
    
    
    NET_Init();
    
    
    NET_APP_WizReset();
    
    osDelay(1);
    
    NET_APP_WizRun();
    
    
    // Initialize HAL.
    WIZ_HAL_InitTypeDef halInit =
        { .WIZ_HAL_TransmitReceiveSync = NET_APP_SpiTransmitReceiveSync, .WIZ_HAL_TakeMutex = NET_APP_SpiTakeMutex, .WIZ_HAL_ReleaseMutex = NET_APP_SpiReleaseMutex, 
          .WIZ_HAL_ResetChip = NET_APP_WizReset, .WIZ_HAL_RunChip = NET_APP_WizRun, .WIZ_HAL_ActivateNss = NET_APP_WizActivateNss, .WIZ_HAL_DeactivateNss = NET_APP_WizDeactivateNss };
    
    if ((error = WIZ_HAL_Init(&halInit)) != WIZ_ERR_OK)
    {
        net_os_printf("WIZ HAL init err:%08X\r\n", error);
        
        _Error_Handler(__FILE__, __LINE__);
    }
    
    
    // Initialize Memory.
    WIZ_MemorySizeTypeDef pTxSizes[WIZ_SOCKET_COUNT];
    WIZ_MemorySizeTypeDef pRxSizes[WIZ_SOCKET_COUNT];
    
    for (WIZ_SocketNumberTypeDef socket = WIZ_SOCKET_0; socket < WIZ_SOCKET_COUNT; socket++)
    {
        pTxSizes[socket] = WIZ_MEMSIZE_2;
        pRxSizes[socket] = WIZ_MEMSIZE_2;
    }
    
    if ((error = WIZ_SetMemorySizes(pTxSizes, pRxSizes)) != WIZ_ERR_OK)
    {
        net_os_printf("WIZ MEM init err:%08X\r\n", error);
        
        _Error_Handler(__FILE__, __LINE__);
    }
     
    
    // Initialize MAC.
    uint8_t pMacAddress[] = { 0x5E, 0x4C, 0x3D, 0x2B, 0x1A, 0x00 };
    
    if ((error = NET_SetMacAddress(pMacAddress)) != WIZ_ERR_OK)
    {
        net_os_printf("WIZ set IP error:%08X\r\n", error);
    }
    
    
    // Initialize network main params.
    /* While using DHCP we must set 0.0.0.0 for all. */
    NET_IPAddressTypeDef ipAddress = NET_CreateIPAddress(192, 168, 1, 200);
    NET_IPAddressTypeDef subnetMask = NET_CreateIPAddress(255, 255, 255, 0);
    NET_IPAddressTypeDef defaultGateway = NET_CreateIPAddress(192, 168, 1, 1);      
    
    if ((error = NET_SetIPAddress(ipAddress)) != WIZ_ERR_OK)
    {
        net_os_printf("WIZ set IP error:%08X\r\n", error);
    }    
    
    if ((error = NET_SetSubnetMask(subnetMask)) != WIZ_ERR_OK)
    {
        net_os_printf("WIZ set IP error:%08X\r\n", error);
    }    
    
    if ((error = NET_SetDefaultGateway(defaultGateway)) != WIZ_ERR_OK)
    {
        net_os_printf("WIZ set IP error:%08X\r\n", error);
    }
    
    
    // Initialize network additional params.
    NET_SetDNSServerAddress(NET_CreateIPAddress(192, 168, 1, 1));
    NET_SetSNTPServerName("0.ru.pool.ntp.org");
    
    NET_SetFlags((NET_FlagTypeDef)(NET_FLAG_USE_DHCP_FOR_ALL | NET_FLAG_SNTP_NAME_PRIORITY));
        
    
    /* DHCP_Run must be here */
    
    
    NET_APP_RunNetwork();
    
    while(true)
    {
        NET_OS_Delay(1000);
    }    
}

void NET_APP_RunNetwork(void)
{
    printf("free size: %u\r\n", xPortGetFreeHeapSize());
    printf("min free size: %u\r\n", xPortGetMinimumEverFreeHeapSize());
    
    osThreadDef(SNTP_CLNT, NET_APP_StartSntpClient, osPriorityNormal, 0, 128 * 2);
    sntpClntTask = osThreadCreate(osThread(SNTP_CLNT), NULL);
    
    printf("free size: %u\r\n", xPortGetFreeHeapSize());
    printf("min free size: %u\r\n", xPortGetMinimumEverFreeHeapSize());
    
    osThreadDef(HTTP_SRV, NET_APP_StartHttpServer, osPriorityNormal, 0, 128 * 2);
    httpSrvTask = osThreadCreate(osThread(HTTP_SRV), NULL);
    
    printf("free size: %u\r\n", xPortGetFreeHeapSize());
    printf("min free size: %u\r\n", xPortGetMinimumEverFreeHeapSize());
}

void NET_APP_HaltNetwork(void)
{
    /* not implemented */ _Error_Handler(__FILE__, __LINE__);
}

void NET_APP_StartHttpServer(void const * argument)
{
    NET_ErrorTypeDef error = NET_ERR_OK;
    
    
    HTTP_SRV_HandleTypeDef hHttp;
    memset(&hHttp, 0, sizeof(HTTP_SRV_HandleTypeDef));
    
    hHttp.SocketsCount = 3;
    hHttp.UrlEntriesMaxCount = 8;
    
    NET_PortTypeDef port = { .Value = 80 };
    
    
    if ((error = HTTP_SRV_AllocHandle(&hHttp, port)) != HTTP_SRV_ERR_OK)
    {
        net_os_printf("HTTP srv err:%08X\r\n", error);
        
        _Error_Handler(__FILE__, __LINE__);
    }
    
    
    if ((error = HTTP_SRV_AddUrlEntry(&hHttp, (const uint8_t *)_networkJs, _networkJsSize, _networkJsUrl, _networkJsUrlSize, _contentTypeJs, _contentTypeJsSize)) != HTTP_SRV_ERR_OK)
    {
        net_os_printf("HTTP srv err:%08X\r\n", error);
        
        _Error_Handler(__FILE__, __LINE__);
    }
    
    if ((error = HTTP_SRV_AddUrlEntry(&hHttp, (const uint8_t *)_faviconPng, _faviconPngSize, _faviconPngUrl, _faviconPngUrlSize, _contentTypePng, _contentTypePngSize)) != HTTP_SRV_ERR_OK)
    {
        net_os_printf("HTTP srv err:%08X\r\n", error);
        
        _Error_Handler(__FILE__, __LINE__);
    }
    
    
    while (true)
    {
        if ((error = HTTP_SRV_Run(&hHttp)) != HTTP_SRV_ERR_OK)
        {
            net_os_printf("HTTP srv err:%08X\r\n", error);
            
            _Error_Handler(__FILE__, __LINE__);
        }
        
        NET_OS_Delay(10);
    }
}

void NET_APP_StartSntpClient(void const * argument)
{
    NET_ErrorTypeDef error = NET_ERR_OK;    
    
    
    while (true)
    {    
        puts("");
        
        SNTP_DateTimeTypeDef dateTime;
        
        if ((error = SNTP_DetermineDateTime(NTP_OFFSET_MSK, &dateTime)) != SNTP_ERR_OK)
        {
            printf("Date & time determine err:%08X\r\n", error);
            
            if ((error & SNTP_ERR_CANNOT_DETERMINE) == 0)
            {
                _Error_Handler(__FILE__, __LINE__);
            }
        }
        
        osDelay(15000);
    }
}

// Callback for SPI exchange.
NET_ErrorTypeDef NET_APP_SpiTransmitReceiveSync(uint8_t *pBuffer, uint16_t length)
{
    NET_ErrorTypeDef error = NET_ERR_OK;
    
    
    // Take mutex.
    if ((error = NET_APP_SpiTakeMutex()) != NET_ERR_OK) return error;
    
    // Down NSS pin.
    NET_APP_WizActivateNss();
    
    // Exchange.
    HAL_StatusTypeDef halStatus = HAL_SPI_TransmitReceive(&hspi1, pBuffer, pBuffer, length, 100);
    
    // Up NSS pin.
    NET_APP_WizDeactivateNss();
    
    // Release mutex.
    if ((error = NET_APP_SpiReleaseMutex()) != NET_ERR_OK) return error; 
    
    
    return (halStatus == HAL_OK) ? NET_ERR_OK : WIZ_ERR_HAL_HARDWARE;
}

// Callback for wait and take SPI mutex.
NET_ErrorTypeDef NET_APP_SpiTakeMutex(void)
{
    return (osRecursiveMutexWait(hspi1RecursiveMutexHandle, osWaitForever) == osOK) ? NET_ERR_OK : WIZ_ERR_HAL_SOFTWARE;
}

// Callback for release SPI mutex.
NET_ErrorTypeDef NET_APP_SpiReleaseMutex(void)
{
    return (osRecursiveMutexRelease(hspi1RecursiveMutexHandle) == osOK) ? NET_ERR_OK : WIZ_ERR_HAL_SOFTWARE;
}

// Hard reset WIZnet chip.
void NET_APP_WizReset(void)
{
    HAL_GPIO_WritePin(WIZ_RST_GPIO_Port, WIZ_RST_Pin, GPIO_PIN_RESET);
}

// Run WIZnet chip after reset.
void NET_APP_WizRun(void)
{
    HAL_GPIO_WritePin(WIZ_RST_GPIO_Port, WIZ_RST_Pin, GPIO_PIN_SET);
}

// Activate NSS pin. Must be called AFTER successful taking mutex.
void NET_APP_WizActivateNss(void)
{
    HAL_GPIO_WritePin(WIZ_NSS_GPIO_Port, WIZ_NSS_Pin, GPIO_PIN_RESET);
}

// Deactivate NSS pin. Must be called BEFORE releasing mutex.
void NET_APP_WizDeactivateNss(void)
{
    HAL_GPIO_WritePin(WIZ_NSS_GPIO_Port, WIZ_NSS_Pin, GPIO_PIN_SET);
}
