#include "cmsis_os.h"
#include "main.h"

#include "spi.h"
#include "net.h"
#include "wizchip.h"

#include "wizchip_app.h"


#define WIZ_APP_SPI_TIMEOUT   ((uint32_t)100)


extern osMutexId _SPI2_APP_MutexHandle;


NET_ErrorTypeDef WIZ_HAL_APP_TransmitReceiveSync(uint8_t *pTxRxData, uint16_t length);
NET_ErrorTypeDef WIZ_HAL_APP_TakeControl(void);
NET_ErrorTypeDef WIZ_HAL_APP_ReleaseControl(void);
bool WIZ_HAL_APP_IsInt(void);
void WIZ_HAL_APP_Yield(void);
static void WIZ_HAL_APP_ActivateNss(void);
static void WIZ_HAL_APP_DeactivateNss(void);
static NET_ErrorTypeDef WIZ_HAL_APP_TakeMutex(void);
static NET_ErrorTypeDef WIZ_HAL_APP_ReleaseMutex(void);


void WIZ_HAL_APP_FillCallbacks(WIZ_HAL_InitTypeDef *hHal)
{
    hHal->WIZ_HAL_TransmitReceiveSync = WIZ_HAL_APP_TransmitReceiveSync;
    hHal->WIZ_HAL_TakeMutex = WIZ_HAL_APP_TakeControl;
    hHal->WIZ_HAL_ReleaseMutex = WIZ_HAL_APP_ReleaseControl;
    //hHal->NRF24_HAL_IsIrq = WIZ_HAL_APP_IsIrq;
    //hHal->NRF24_HAL_Yield = WIZ_HAL_APP_Yield;
}

NET_ErrorTypeDef WIZ_HAL_APP_TransmitReceiveSync(uint8_t *pTxRxData, uint16_t length)
{
    return (HAL_SPI_TransmitReceive(&hspi1, pTxRxData, pTxRxData, length, WIZ_APP_SPI_TIMEOUT) != HAL_OK) ? WIZ_ERR_HAL_HARDWARE : NET_ERR_OK;
}

NET_ErrorTypeDef WIZ_HAL_APP_TakeControl(void)
{
    NET_ErrorTypeDef error = NET_ERR_OK;
    
    if ((error = WIZ_HAL_APP_TakeMutex()) != NET_ERR_OK) return error;
    
    WIZ_HAL_APP_ActivateNss();
    
    return error;
}

NET_ErrorTypeDef WIZ_HAL_APP_ReleaseControl(void)
{
    NET_ErrorTypeDef error = NET_ERR_OK;
    
    if ((error = WIZ_HAL_APP_ReleaseMutex()) != NET_ERR_OK) return error;
    
    WIZ_HAL_APP_DeactivateNss();
    
    return error;
}

void WIZ_HAL_APP_ActivateCe(void)
{
    HAL_GPIO_WritePin(NRF_CE_GPIO_Port, NRF_CE_Pin, GPIO_PIN_SET);
}

void WIZ_HAL_APP_DeactivateCe(void)
{
    HAL_GPIO_WritePin(NRF_CE_GPIO_Port, NRF_CE_Pin, GPIO_PIN_RESET);
}

bool WIZ_HAL_APP_IsIrq(void)
{
    return (HAL_GPIO_ReadPin(NRF_IRQ_GPIO_Port, NRF_IRQ_Pin) == GPIO_PIN_RESET);
}

void WIZ_HAL_APP_Yield(void)
{
    taskYIELD();
}

static void WIZ_HAL_APP_ActivateNss(void)
{
    HAL_GPIO_WritePin(NRF_NSS_GPIO_Port, NRF_NSS_Pin, GPIO_PIN_RESET);
}

static void WIZ_HAL_APP_DeactivateNss(void)
{
    HAL_GPIO_WritePin(NRF_NSS_GPIO_Port, NRF_NSS_Pin, GPIO_PIN_SET);
}

static NET_ErrorTypeDef WIZ_HAL_APP_TakeMutex(void)
{
    return (osRecursiveMutexWait(_SPI2_APP_MutexHandle, osWaitForever) == osOK) ? NET_ERR_OK : WIZ_ERR_HAL_SOFTWARE;
}

static NET_ErrorTypeDef WIZ_HAL_APP_ReleaseMutex(void)
{
    return (osRecursiveMutexRelease(_SPI2_APP_MutexHandle) == osOK) ? NET_ERR_OK : WIZ_ERR_HAL_SOFTWARE;
}
