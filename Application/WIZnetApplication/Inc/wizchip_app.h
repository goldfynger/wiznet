#ifndef __NRF24_APP_H
#define __NRF24_APP_H


#include "nrf24.h"


void NRF24_HAL_APP_FillCallbacks(NRF24_HalTypeDef *hHal);


#endif /* __NRF24_H */
